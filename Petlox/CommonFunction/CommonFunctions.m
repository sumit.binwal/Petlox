//
//  CommonFunctions.m
//  Metal Calculator
//
//  Created by Sumit Sharma on 26/11/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "CommonFunctions.h"

@implementation CommonFunctions

+(void) setNavigationBar:(UINavigationController*)navController setNavItem:(UINavigationItem *)navItem
{
    
    
//   [navController setNavigationBarHidden:FALSE];
//   [navController.navigationBar setTranslucent:NO];
//    
//    [[UINavigationBar appearance] setShadowImage:[UIImage new]];
    
  //  [navController.navigationBar setBackgroundImage:[UIImage imageNamed:@"haeder_bar"] forBarMetrics:UIBarMetricsDefault];
   
//  UIImageView *imgViewsearchBarBg = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f,81.0f ,44.0f)];
//  [imgViewsearchBarBg setImage:[UIImage imageNamed:@"haeder_bar"]];
//  
//navItem.titleView = imgViewsearchBarBg;
}

+(void) setNavigationBar:(UINavigationController*)navController
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    UINavigationBar *navBar=navController.navigationBar;
    navController.navigationBar.barTintColor=[UIColor colorWithRed:70.0f/255.0f green:11.0f/255.0f blue:50.0f/255.0f alpha:1];
//    navController.navigationBar.barTintColor=[UIColor colorWithRed:0.0f/255.0f green:172.0f/255.0f blue:238.0f/255.0f alpha:1];
    navBar.tintColor=[UIColor whiteColor];
    
   // UIImage *img=[UIImage imageNamed:@"topNavBar"];

   // [navBar setBackgroundImage:img forBarMetrics:UIBarMetricsDefault];
    [navController setNavigationBarHidden:FALSE];
    

    [navController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:FONT_GULIM size:17],NSFontAttributeName,[UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    [navController.navigationBar setTranslucent:NO];

    
    UIView *statusView=[[UIView alloc]initWithFrame:CGRectMake(0, -[UIApplication sharedApplication].statusBarFrame.size.height,[UIApplication sharedApplication].statusBarFrame.size.width, [UIApplication sharedApplication].statusBarFrame.size.height)];
    [statusView setBackgroundColor:[UIColor colorWithRed:71.0f/255.0f green:13.0f/255.0f blue:52.0f/255.0f alpha:1.0]];
//    [statusView setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f green:172.0f/255.0f blue:238.0f/255.0f alpha:1.0]];
    [navBar addSubview:statusView];
    
    
    
    [[UINavigationBar appearance] setTitleTextAttributes: @{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"OpenSans-Semibold" size:18]}];
}

+(void) setSecondNavigationBar:(UINavigationController *)navController
{

    UINavigationBar *navBar=navController.navigationBar;
    UIImage *img=[UIImage imageNamed:@"topNavBar"];
    [navBar setBackgroundImage:img forBarMetrics:UIBarMetricsDefault];
    [navController.navigationBar setTintColor:[UIColor whiteColor]];
    [navController setNavigationBarHidden:FALSE];
    [navController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:FONT_SEGOE_NORMAL size:20],NSFontAttributeName,[UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    [navController.navigationBar setTranslucent:NO];
    
    UIView *statusView=[[UIView alloc]initWithFrame:CGRectMake(0, -[UIApplication sharedApplication].statusBarFrame.size.height,[UIApplication sharedApplication].statusBarFrame.size.width, [UIApplication sharedApplication].statusBarFrame.size.height+1)];
    [statusView setBackgroundColor:[UIColor colorWithRed:50.0f/255.0f green:169.0f/255.0f blue:238.0f/255.0f alpha:1.0]];
    [navBar addSubview:statusView];
    
    [[UINavigationBar appearance] setTitleTextAttributes: @{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"OpenSans-Semibold" size:18]}];
    
 [[UINavigationBar appearance]setShadowImage:[[UIImage alloc] init]];
}
#pragma mark
#pragma mark ReachabiltyCheck methods
+ (BOOL)isValueNotEmpty:(NSString*)aString{
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    aString = [aString stringByTrimmingCharactersInSet:whitespace];

    if (aString == nil || [aString length] == 0){
        
        return NO;
    }
    return YES;
}

+(NSString *)trimSpaceInString:(NSString *)mainstr
{
    NSCharacterSet *whitespace=[NSCharacterSet whitespaceAndNewlineCharacterSet];
    mainstr=[mainstr stringByTrimmingCharactersInSet:whitespace];
        mainstr=[mainstr stringByReplacingOccurrencesOfString:@"  " withString:@""];
    return mainstr;
}


+(BOOL) reachabiltyCheck
{
    
    BOOL status =YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    Reachability * reach = [Reachability reachabilityForInternetConnection];
    
    // //NSLog(@"reachabiltyCheck status  : %d",[reach currentReachabilityStatus]);
    
    if([reach currentReachabilityStatus]==0)
    {
        status = NO;
        //NSLog(@"network not connected");
    }
    
    reach.reachableBlock = ^(Reachability * reachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            // blockLabel.text = @"Block Says Reachable";
        });
    };
    
    reach.unreachableBlock = ^(Reachability * reachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            //  blockLabel.text = @"Block Says Unreachable";
        });
    };
    
    [reach startNotifier];
    return status;
}
+(BOOL)reachabilityChanged:(NSNotification*)note
{
    BOOL status =YES;
    //NSLog(@"reachabilityChanged");
    
    Reachability * reach = [note object];
    
    if([reach isReachable])
    {
        //notificationLabel.text = @"Notification Says Reachable"
        status = YES;
        //NSLog(@"NetWork is Available");
    }
    else
    {
        status = NO;
        /*
         CustomAlert *alert=[[CustomAlert alloc]initWithTitle:@"There was a small problem" message:@"The network doesn't seem to be responding, please try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alert show];
         */
    }
    return status;
}

+ (void)removeActivityIndicator {
    [MBProgressHUD hideAllHUDsForView:APPDELEGATE.window animated:YES];
    [MBProgressHUD hideHUDForView:APPDELEGATE.window animated:YES];
}

+ (void)showActivityIndicatorWithText:(NSString *)text {
    [self removeActivityIndicator];
    
    MBProgressHUD *hud   = [MBProgressHUD showHUDAddedTo:APPDELEGATE.window animated:YES];
    //hud.labelText        = text;
//    hud.detailsLabelText = NSLocalizedString(@"Please Wait...", @"");
}
/*!
 @function	removeActivityIndicator
 @abstract	removes the MBProgressHUD (if any) from window.
 */

+(BOOL)IsValidEmail:(NSString *)checkString
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg withDelegate:(id)delegate{
    [[[UIAlertView alloc] initWithTitle:@"Petlox"
                                message:aMsg
                               delegate:delegate
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil, nil] show];
}
+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg BtnTitle:(NSString*)btnName withDelegate:(id)delegate{
    [[[UIAlertView alloc] initWithTitle:@"Petlox"
                                message:aMsg
                               delegate:delegate
                      cancelButtonTitle:btnName
                      otherButtonTitles:nil, nil] show];
}
+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg{
    [self alertTitle:@"Petlox" withMessage:aMsg withDelegate:self];
}


+(NSDate *)convertDateFromGMTFormat:(NSString *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSDate* dateA = [dateFormatter dateFromString:date];
    return dateA;
}

+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg withDelegate:(id)delegate withTag:(int)tag{
    UIAlertView *alrtVw=[[UIAlertView alloc] initWithTitle:@"Petlox"
                                                   message:aMsg
                                                  delegate:delegate
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil, nil];
    
     alrtVw.tag=tag;
    [alrtVw show];
   
    
}

+(NSString *)convertTimeStampToDate :(NSDate *)date {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.locale = [NSLocale currentLocale];
    [formatter setDateFormat:@"MMM dd, yyyy"];
    
    NSString *stringFromDate = [formatter stringFromDate:date];
    return stringFromDate;
}

+(UIViewController *)exists:(Class)viewControllerClass in:(UINavigationController *)navigationController{
    
    for (UIViewController *oneVC in navigationController.viewControllers) {
        
        if ([oneVC isKindOfClass: viewControllerClass]) {
            return oneVC;
        }
    }
    return nil;
}

@end
