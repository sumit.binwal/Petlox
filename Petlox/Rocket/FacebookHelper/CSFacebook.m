//
//  CSFacebook.m
//  demoFacebooklogin
//
//  Created by Chagan Singh on 09/02/15.
//  Copyright (c) 2015 Konstant. All rights reserved.
//

#import "CSFacebook.h"

@interface CSFacebook()
@end

@implementation CSFacebook


#pragma mark -
#pragma mark - Singleton

+ (CSFacebook *)sharedInstance
{
    static CSFacebook *csFacebook = nil;
    
    @synchronized (self){
        
        static dispatch_once_t pred;
        dispatch_once(&pred, ^{
            csFacebook = [[CSFacebook alloc] init];
        });
    }
    
    return csFacebook;
}




#pragma mark -
#pragma mark - Private Methods

- (void)initWithPermissions:(NSArray *)permissions
{
    self.permissions = permissions;
}

- (BOOL)isSessionValid
{
    if (!FBSession.activeSession.isOpen){
        
        if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded){
            [FBSession.activeSession openWithCompletionHandler:^(FBSession *session,
                                                                 FBSessionState status,
                                                                 NSError *error) {
                FBSession.activeSession = session;
            }];
        }
    }
    
    return FBSession.activeSession.isOpen;
}

- (void)loginCallBack:(CSFacebookCallback)callBack
{
    [FBSession openActiveSessionWithReadPermissions:self.permissions allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
        
        if (status == FBSessionStateOpen) {
            
            FBRequest *fbRequest = [FBRequest requestForMe];
            [fbRequest setSession:session];
            
            [fbRequest startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error){
                NSMutableDictionary *userInfo = nil;
                if( [result isKindOfClass:[NSDictionary class]] ){
                    userInfo = (NSMutableDictionary *)result;
                    if( [userInfo count] > 0 ){
                        [userInfo setObject:session.accessTokenData.accessToken forKey:@"accessToken"];
                    }
                }
                if(callBack){
                    callBack(!error, userInfo);
                }
            }];
        }else if(status == FBSessionStateClosedLoginFailed){
            callBack(NO, @"Closed session state indicating that a login attempt failed");
        }
    }];
}



- (void)logoutCallBack:(CSFacebookCallback)callBack
{
    if (FBSession.activeSession.isOpen){
        [FBSession.activeSession closeAndClearTokenInformation];
        [FBSession setActiveSession:nil];
    }
    
    NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray* facebookCookies = [cookies cookiesForURL:[NSURL URLWithString:@"https://facebook.com/"]];
    
    for (NSHTTPCookie* cookie in facebookCookies) {
        [cookies deleteCookie:cookie];
    }
    
    callBack(YES, @"Logout successfully");
}

- (void)getUserFields:(NSString *)fields callBack:(CSFacebookCallback)callBack
{
    if (![self isSessionValid]) {
        callBack(NO, @"Not logged in");
        return;
    }
    
    [self graphFacebookForMethodGET:@"me" params:@{@"fields" : fields} callBack:callBack];
}


- (void)getUserFriendsCallBack:(CSFacebookCallback)callBack
{
    if (![self isSessionValid]) {
        callBack(NO, @"Not logged in");
        return;
    }
    
    [FBRequestConnection startForMyFriendsWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        callBack(!error, result[@"data"]);
    }];
}

- (void)feedPostWithLinkPath:(NSString *)url caption:(NSString *)caption message:(NSString *)message photo:(UIImage *)photo video:(NSData *)videoData callBack:(CSFacebookCallback)callBack
{
    if (![self isSessionValid]) {
        callBack(NO, @"Not logged in");
        return;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    //Need to provide POST parameters to the Facebook SDK for the specific post type
    NSString *graphPath = @"me/feed";
    
    switch (self.postType) {
        case FBPostTypeLink:{
            [params setObject:url forKey:@"link"];
            [params setObject:caption forKey:@"description"];
            break;
        }
        case FBPostTypeStatus:{
            [params setObject:message forKey:@"message"];
            break;
        }
        case FBPostTypePhoto:{
            graphPath = @"me/photos";
            [params setObject:UIImagePNGRepresentation(photo) forKey:@"source"];
            [params setObject:caption forKey:@"message"];
            break;
        }
        case FBPostTypeVideo:{
            graphPath = @"me/videos";
            [params setObject:videoData forKey:@"video.mp4"];
            [params setObject:caption forKey:@"title"];
            [params setObject:message forKey:@"description"];
            break;
        }
            
        default:
            break;
    }
    
    [self graphFacebookForMethodPOST:graphPath params:params callBack:callBack];
}

- (void)myFeedCallBack:(CSFacebookCallback)callBack
{
    if (![self isSessionValid]) {
        callBack(NO, @"Not logged in");
        return;
    }
    
    [self graphFacebookForMethodPOST:@"me/feed" params:nil callBack:callBack];
}

- (void)inviteFriendsWithMessage:(NSString *)message callBack:(CSFacebookCallback)callBack
{
    if (![self isSessionValid]) {
        callBack(NO, @"Not logged in");
        return;
    }
    
    [FBWebDialogs presentRequestsDialogModallyWithSession:nil
                                                  message:message
                                                    title:nil
                                               parameters:nil
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // Error launching the dialog or sending the request.
                                                          callBack(NO, @"Error sending request.");
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // User clicked the "x" icon
                                                              callBack(NO, @"User canceled request.");
                                                          } else {
                                                              callBack(YES, @"Send invite");
                                                          }
                                                      }
                                                  }];
}

- (void)getPagesCallBack:(CSFacebookCallback)callBack
{
    if (![self isSessionValid]) {
        callBack(NO, @"Not logged in");
        return;
    }
    
    [self graphFacebookForMethodGET:@"me/accounts" params:nil callBack:callBack];
}

- (void)getPageById:(NSString *)pageId callBack:(CSFacebookCallback)callBack
{
    if (![self isSessionValid]) {
        callBack(NO, @"Not logged in");
        return;
    }
    
    if (!pageId) {
        callBack(NO, @"Page id or name required");
        return;
    }
    
    [CSFacebook graphFacebookForMethodGET:pageId params:nil callBack:callBack];
}

- (void)feedPostForPage:(NSString *)page message:(NSString *)message callBack:(CSFacebookCallback)callBack
{
    if (![self isSessionValid]) {
        callBack(NO, @"Not logged in");
        return;
    }
    
    if (!page) {
        callBack(NO, @"Page id or name required");
        return;
    }
    
    [CSFacebook graphFacebookForMethodPOST:[NSString stringWithFormat:@"%@/feed", page] params:@{@"message": message} callBack:callBack];
}

- (void)feedPostForPage:(NSString *)page message:(NSString *)message photo:(UIImage *)photo callBack:(CSFacebookCallback)callBack
{
    
    if (![self isSessionValid]) {
        callBack(NO, @"Not logged in");
        return;
    }
    
    if (!page) {
        callBack(NO, @"Page id or name required");
        return;
    }
    
    [CSFacebook graphFacebookForMethodPOST:[NSString stringWithFormat:@"%@/photos", page] params:@{@"message": message, @"source" : UIImagePNGRepresentation(photo)} callBack:callBack];
}

- (void)feedPostForPage:(NSString *)page message:(NSString *)message link:(NSString *)url callBack:(CSFacebookCallback)callBack
{
    if (![self isSessionValid]) {
        callBack(NO, @"Not logged in");
        return;
    }
    
    if (!page) {
        callBack(NO, @"Page id or name required");
        return;
    }
    
    [CSFacebook graphFacebookForMethodPOST:[NSString stringWithFormat:@"%@/feed", page] params:@{@"message": message, @"link" : url} callBack:callBack];
}

- (void)feedPostForPage:(NSString *)page video:(NSData *)videoData title:(NSString *)title description:(NSString *)description callBack:(CSFacebookCallback)callBack
{
    if (![self isSessionValid]) {
        callBack(NO, @"Not logged in");
        return;
    }
    
    if (!page) {
        callBack(NO, @"Page id or name required");
        return;
    }
    
    [CSFacebook graphFacebookForMethodPOST:[NSString stringWithFormat:@"%@/videos", page]
                                    params:@{@"title" : title,
                                             @"description" : description,
                                             @"video.mp4" : videoData} callBack:callBack];
}

- (void)feedPostAdminForPageName:(NSString *)page message:(NSString *)message callBack:(CSFacebookCallback)callBack
{
    [CSFacebook getPagesCallBack:^(BOOL success, id result) {
        
        if (success) {
            
            NSDictionary *dicPageAdmin = nil;
            
            for (NSDictionary *dic in result[@"data"]) {
                
                if ([dic[@"name"] isEqualToString:page]) {
                    dicPageAdmin = dic;
                    break;
                }
            }
            
            if (!dicPageAdmin) {
                callBack(NO, @"Page not found!");
                return;
            }
            
            FBRequest *requestToPost = [[FBRequest alloc] initWithSession:nil
                                                                graphPath:[NSString stringWithFormat:@"%@/feed",dicPageAdmin[@"id"]]
                                                               parameters:@{@"message" : message, @"access_token" : dicPageAdmin[@"access_token"]}
                                                               HTTPMethod:@"POST"];
            
            FBRequestConnection *requestToPostConnection = [[FBRequestConnection alloc] init];
            [requestToPostConnection addRequest:requestToPost completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                if (error) {
                    callBack(NO, [error domain]);
                }else{
                    callBack(YES, result);
                }
            }];
            
            [requestToPostConnection start];
        }
    }];
}

- (void)feedPostAdminForPageName:(NSString *)page video:(NSData *)videoData title:(NSString *)title description:(NSString *)description callBack:(CSFacebookCallback)callBack
{
    [CSFacebook getPagesCallBack:^(BOOL success, id result) {
        
        if (success) {
            
            NSDictionary *dicPageAdmin = nil;
            
            for (NSDictionary *dic in result[@"data"]) {
                
                if ([dic[@"name"] isEqualToString:page]) {
                    dicPageAdmin = dic;
                    break;
                }
            }
            
            if (!dicPageAdmin) {
                callBack(NO, @"Page not found!");
                return;
            }
            
            FBRequest *requestToPost = [[FBRequest alloc] initWithSession:nil
                                                                graphPath:[NSString stringWithFormat:@"%@/videos",dicPageAdmin[@"id"]]
                                                               parameters:@{@"title" : title,
                                                                            @"description" : description,
                                                                            @"video.mp4" : videoData,
                                                                            @"access_token" : dicPageAdmin[@"access_token"]}
                                                               HTTPMethod:@"POST"];
            
            FBRequestConnection *requestToPostConnection = [[FBRequestConnection alloc] init];
            [requestToPostConnection addRequest:requestToPost completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                if (error) {
                    callBack(NO, [error domain]);
                }else{
                    callBack(YES, result);
                }
            }];
            
            [requestToPostConnection start];
        }
    }];
}

- (void)feedPostAdminForPageName:(NSString *)page message:(NSString *)message link:(NSString *)url callBack:(CSFacebookCallback)callBack
{
    [CSFacebook getPagesCallBack:^(BOOL success, id result) {
        
        if (success) {
            
            NSDictionary *dicPageAdmin = nil;
            
            for (NSDictionary *dic in result[@"data"]) {
                
                if ([dic[@"name"] isEqualToString:page]) {
                    dicPageAdmin = dic;
                    break;
                }
            }
            
            if (!dicPageAdmin) {
                callBack(NO, @"Page not found!");
                return;
            }
            
            FBRequest *requestToPost = [[FBRequest alloc] initWithSession:nil
                                                                graphPath:[NSString stringWithFormat:@"%@/feed",dicPageAdmin[@"id"]]
                                                               parameters:@{@"message" : message,
                                                                            @"link" : url,
                                                                            @"access_token" : dicPageAdmin[@"access_token"]}
                                                               HTTPMethod:@"POST"];
            
            FBRequestConnection *requestToPostConnection = [[FBRequestConnection alloc] init];
            [requestToPostConnection addRequest:requestToPost completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                if (error) {
                    callBack(NO, [error domain]);
                }else{
                    callBack(YES, result);
                }
            }];
            
            [requestToPostConnection start];
        }
    }];
}

- (void)feedPostAdminForPageName:(NSString *)page message:(NSString *)message photo:(UIImage *)photo callBack:(CSFacebookCallback)callBack
{
    [CSFacebook getPagesCallBack:^(BOOL success, id result) {
        
        if (success) {
            
            NSDictionary *dicPageAdmin = nil;
            
            for (NSDictionary *dic in result[@"data"]) {
                
                if ([dic[@"name"] isEqualToString:page]) {
                    dicPageAdmin = dic;
                    break;
                }
            }
            
            if (!dicPageAdmin) {
                callBack(NO, @"Page not found!");
                return;
            }
            
            FBRequest *requestToPost = [[FBRequest alloc] initWithSession:nil
                                                                graphPath:[NSString stringWithFormat:@"%@/photos",dicPageAdmin[@"id"]]
                                                               parameters:@{@"message" : message,
                                                                            @"source" : UIImagePNGRepresentation(photo),
                                                                            @"access_token" : dicPageAdmin[@"access_token"]}
                                                               HTTPMethod:@"POST"];
            
            FBRequestConnection *requestToPostConnection = [[FBRequestConnection alloc] init];
            [requestToPostConnection addRequest:requestToPost completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                if (error) {
                    callBack(NO, [error domain]);
                }else{
                    callBack(YES, result);
                }
            }];
            
            [requestToPostConnection start];
        }
    }];
}

- (void)getAlbumsCallBack:(CSFacebookCallback)callBack
{
    if (![self isSessionValid]) {
        callBack(NO, @"Not logged in");
        return;
    }
    
    [self graphFacebookForMethodGET:@"me/albums" params:nil callBack:callBack];
}

- (void)getAlbumById:(NSString *)albumId callBack:(CSFacebookCallback)callBack
{
    if (![self isSessionValid]) {
        callBack(NO, @"Not logged in");
        return;
    }
    
    if (!albumId) {
        callBack(NO, @"Album id required");
        return;
    }
    
    [CSFacebook graphFacebookForMethodGET:albumId params:nil callBack:callBack];
}

- (void)getPhotosAlbumById:(NSString *)albumId callBack:(CSFacebookCallback)callBack
{
    
    if (![self isSessionValid]) {
        callBack(NO, @"Not logged in");
        return;
    }
    
    if (!albumId) {
        callBack(NO, @"Album id required");
        return;
    }
    
    [CSFacebook graphFacebookForMethodGET:[NSString stringWithFormat:@"%@/photos", albumId] params:nil callBack:callBack];
}

- (void)createAlbumName:(NSString *)name message:(NSString *)message privacy:(FBAlbumPrivacyType)privacy callBack:(CSFacebookCallback)callBack
{
    if (![self isSessionValid]) {
        callBack(NO, @"Not logged in");
        return;
    }
    
    if (!name && !message) {
        callBack(NO, @"Name and message required");
        return;
    }
    
    NSString *privacyString = @"";
    
    switch (privacy) {
        case FBAlbumPrivacyEveryone:
            privacyString = @"EVERYONE";
            break;
        case FBAlbumPrivacyAllFriends:
            privacyString = @"ALL_FRIENDS";
            break;
        case FBAlbumPrivacyFriendsOfFriends:
            privacyString = @"FRIENDS_OF_FRIENDS";
            break;
        case FBAlbumPrivacySelf:
            privacyString = @"SELF";
            break;
        default:
            break;
    }
    
    [CSFacebook graphFacebookForMethodPOST:@"me/albums" params:@{@"name" : name,
                                                                 @"message" : message,
                                                                 @"value" : privacyString} callBack:callBack];
}

- (void)feedPostForAlbumId:(NSString *)albumId photo:(UIImage *)photo callBack:(CSFacebookCallback)callBack
{
    if (![self isSessionValid]) {
        callBack(NO, @"Not logged in");
        return;
    }
    
    if (!albumId) {
        callBack(NO, @"Album id required");
        return;
    }
    
    [CSFacebook graphFacebookForMethodPOST:[NSString stringWithFormat:@"%@/photos", albumId] params:@{@"source": UIImagePNGRepresentation(photo)} callBack:callBack];
}

- (void)sendForPostOpenGraphPath:(NSString *)path graphObject:(NSMutableDictionary<FBOpenGraphObject> *)openGraphObject objectName:(NSString *)objectName callBack:(CSFacebookCallback)callBack
{
    if (![self isSessionValid]) {
        callBack(NO, @"Not logged in");
        return;
    }
    
    // Post custom object
    [FBRequestConnection startForPostOpenGraphObject:openGraphObject completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if(!error) {
            // get the object ID for the Open Graph object that is now stored in the Object API
            NSString *objectId = [result objectForKey:@"id"];
            
            // create an Open Graph action
            id<FBOpenGraphAction> action = (id<FBOpenGraphAction>)[FBGraphObject graphObject];
            [action setObject:objectId forKey:objectName];
            
            // create action referencing user owned object
            [FBRequestConnection startForPostWithGraphPath:path graphObject:action completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                if(error) {
                    // An error occurred, we need to handle the error
                    // See: https://developers.facebook.com/docs/ios/errors
                    callBack(NO, [NSString stringWithFormat:@"Encountered an error posting to Open Graph: %@", error.description]);
                } else {
                    callBack(YES, [NSString stringWithFormat:@"OG story posted, story id: %@", result[@"id"]]);
                }
            }];
            
        } else {
            // An error occurred, we need to handle the error
            // See: https://developers.facebook.com/docs/ios/errors
            callBack(NO, [NSString stringWithFormat:@"Encountered an error posting to Open Graph: %@", error.description]);
        }
    }];
}

- (void)sendForPostOpenGraphPath:(NSString *)path graphObject:(NSMutableDictionary<FBOpenGraphObject> *)openGraphObject objectName:(NSString *)objectName withImage:(UIImage *)image callBack:(CSFacebookCallback)callBack
{
    if (![self isSessionValid]) {
        callBack(NO, @"Not logged in");
        return;
    }
    
    // stage an image
    [FBRequestConnection startForUploadStagingResourceWithImage:image completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if(!error) {
            DLog(@"Successfuly staged image with staged URI: %@", [result objectForKey:@"uri"]);
            
            // for og:image we assign the uri of the image that we just staged
            //            object[@"image"] = @[@{@"url": [result objectForKey:@"uri"], @"user_generated" : @"false" }];
            
            openGraphObject.image = @[@{@"url": [result objectForKey:@"uri"], @"user_generated" : @"false" }];
            
            [self sendForPostOpenGraphPath:path graphObject:openGraphObject objectName:objectName callBack:callBack];
        }
    }];
}

- (void)graphFacebookForMethodPOST:(NSString *)method params:(id)params callBack:(CSFacebookCallback)callBack
{
    [FBRequestConnection startWithGraphPath:method parameters:params HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result,NSError *error) {
        if (error) {
            DLog(@"%@", error);
            callBack(NO, error);
        } else {
            DLog(@"%@", result);
            callBack(YES, result);
        }
    }];
}

- (void)graphFacebookForMethodGET:(NSString *)method params:(id)params callBack:(CSFacebookCallback)callBack
{
    [FBRequestConnection startWithGraphPath:method parameters:params HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result,NSError *error) {
        if (error) {
            DLog(@"%@", error);
            callBack(NO, error);
        } else {
            DLog(@"%@", result);
            callBack(YES, result);
        }
    }];
}





#pragma mark -
#pragma mark - Public Methods

+ (void)initWithPermissions:(NSArray *)permissions
{
    [[CSFacebook sharedInstance] initWithPermissions:permissions];
}

+(BOOL)isSessionValid
{
    return [[CSFacebook sharedInstance] isSessionValid];
}

+ (void)loginCallBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] loginCallBack:callBack];
}

+ (void)logoutCallBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] logoutCallBack:callBack];
}

+ (void)getUserFields:(NSString *)fields callBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] getUserFields:fields callBack:callBack];
}

+ (void)getUserFriendsCallBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] getUserFriendsCallBack:callBack];
}

+ (void)feedPostWithLinkPath:(NSString *)url caption:(NSString *)caption callBack:(CSFacebookCallback)callBack
{
    [CSFacebook sharedInstance].postType = FBPostTypeLink;
    [[CSFacebook sharedInstance] feedPostWithLinkPath:url caption:caption message:nil photo:nil video:nil callBack:callBack];
}

+ (void)feedPostWithMessage:(NSString *)message callBack:(CSFacebookCallback)callBack
{
    [CSFacebook sharedInstance].postType = FBPostTypeStatus;
    [[CSFacebook sharedInstance] feedPostWithLinkPath:nil caption:nil message:message photo:nil video:nil callBack:callBack];
}

+ (void)feedPostWithPhoto:(UIImage *)photo caption:(NSString *)caption callBack:(CSFacebookCallback)callBack
{
    [CSFacebook sharedInstance].postType = FBPostTypePhoto;
    [[CSFacebook sharedInstance] feedPostWithLinkPath:nil caption:caption message:nil photo:photo video:nil callBack:callBack];
}

+ (void)feedPostWithVideo:(NSData *)videoData title:(NSString *)title description:(NSString *)description callBack:(CSFacebookCallback)callBack
{
    [CSFacebook sharedInstance].postType = FBPostTypeVideo;
    [[CSFacebook sharedInstance] feedPostWithLinkPath:nil caption:title message:description photo:nil video:videoData callBack:callBack];
}

+ (void)myFeedCallBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] myFeedCallBack:callBack];
}

+ (void)inviteFriendsWithMessage:(NSString *)message callBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] inviteFriendsWithMessage:message callBack:callBack];
}

+ (void)getPagesCallBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] getPagesCallBack:callBack];
}

+ (void)getPageById:(NSString *)pageId callBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] getPageById:pageId callBack:callBack];
}

+ (void)feedPostForPage:(NSString *)page message:(NSString *)message callBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] feedPostForPage:page message:message callBack:callBack];
}

+ (void)feedPostForPage:(NSString *)page message:(NSString *)message photo:(UIImage *)photo callBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] feedPostForPage:page message:message photo:photo callBack:callBack];
}

+ (void)feedPostForPage:(NSString *)page message:(NSString *)message link:(NSString *)url callBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] feedPostForPage:page message:message link:url callBack:callBack];
}

+ (void)feedPostForPage:(NSString *)page video:(NSData *)videoData title:(NSString *)title description:(NSString *)description callBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] feedPostForPage:page video:videoData title:title description:description callBack:callBack];
}

+ (void)feedPostAdminForPageName:(NSString *)page message:(NSString *)message callBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] feedPostAdminForPageName:page message:message callBack:callBack];
}

+ (void)feedPostAdminForPageName:(NSString *)page video:(NSData *)videoData title:(NSString *)title description:(NSString *)description callBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] feedPostAdminForPageName:page video:videoData title:title description:description callBack:callBack];
}

+ (void)feedPostAdminForPageName:(NSString *)page message:(NSString *)message link:(NSString *)url callBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] feedPostAdminForPageName:page message:message link:url callBack:callBack];
}

+ (void)feedPostAdminForPageName:(NSString *)page message:(NSString *)message photo:(UIImage *)photo callBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] feedPostAdminForPageName:page message:message photo:photo callBack:callBack];
}

+ (void)getAlbumsCallBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] getAlbumsCallBack:callBack];
}

+ (void)getAlbumById:(NSString *)albumId callBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] getAlbumById:albumId callBack:callBack];
}

+ (void)getPhotosAlbumById:(NSString *)albumId callBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] getPhotosAlbumById:albumId callBack:callBack];
}

+ (void)createAlbumName:(NSString *)name message:(NSString *)message privacy:(FBAlbumPrivacyType)privacy callBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] createAlbumName:name message:message privacy:privacy callBack:callBack];
}

+ (void)feedPostForAlbumId:(NSString *)albumId photo:(UIImage *)photo callBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] feedPostForAlbumId:albumId photo:photo callBack:callBack];
}

+ (void)sendForPostOpenGraphPath:(NSString *)path graphObject:(NSMutableDictionary<FBOpenGraphObject> *)openGraphObject objectName:(NSString *)objectName callBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] sendForPostOpenGraphPath:path graphObject:openGraphObject objectName:objectName callBack:callBack];
}

+ (void)sendForPostOpenGraphPath:(NSString *)path graphObject:(NSMutableDictionary<FBOpenGraphObject> *)openGraphObject objectName:(NSString *)objectName withImage:(UIImage *)image callBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] sendForPostOpenGraphPath:path graphObject:openGraphObject objectName:objectName withImage:image callBack:callBack];
}

+ (void)graphFacebookForMethodGET:(NSString *)method params:(id)params callBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] graphFacebookForMethodGET:method params:params callBack:callBack];
}

+ (void)graphFacebookForMethodPOST:(NSString *)method params:(id)params callBack:(CSFacebookCallback)callBack
{
    [[CSFacebook sharedInstance] graphFacebookForMethodPOST:method params:params callBack:callBack];
}


#pragma mark facebook social framework integration
+(void)facebookAcesstokenIfAlreadylogedinbyFBApporfromSettingswithCompletion:(void (^)(NSArray *data))completion WithFailure:(void (^)(NSString *error))failure
{
    ACAccountStore *accountStore = [[ACAccountStore alloc]init];
    ACAccountType *FBaccountType= [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    // NSString *key = @"882893555063702";
    NSString *key=@"726002114182660";
    NSDictionary *dictFB = [NSDictionary dictionaryWithObjectsAndKeys:key,ACFacebookAppIdKey,@[@"email"],ACFacebookPermissionsKey, nil];
    
    [accountStore requestAccessToAccountsWithType:FBaccountType options:dictFB completion:
     ^(BOOL granted, NSError *e)
     {
         granted=YES;
         
         if (granted)
         {
             NSArray *accounts = [accountStore accountsWithAccountType:FBaccountType];
             //it will always be the last object with single sign on
             ACAccount   *facebookAccount = [accounts lastObject];
             
             
             ACAccountCredential *facebookCredential = [facebookAccount credential];
             NSString *accessToken = [facebookCredential oauthToken];
             DLog(@"Facebook Access Token: %@", accessToken);
             
             if (accessToken.length==0)
             {
                 accessToken = [FBSession activeSession].accessTokenData.accessToken;
             }
             
             
             DLog(@"facebook account =%@",facebookAccount);
             
             NSURL *requestURL = [NSURL URLWithString:@"https://graph.facebook.com/me"];
             
             SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodGET URL:requestURL parameters:nil];
             request.account = facebookAccount;
             
             [request performRequestWithHandler:^(NSData *data, NSHTTPURLResponse *response, NSError *error)
              {
                  
                  if(!error)
                  {
                      
                      NSDictionary *list =[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                      
                      completion((NSArray*)list);
                  }
                  else
                  {
                      [self getFacebookCredentialsWithFaceBookAccount:facebookAccount andAccountStore:accountStore];
                  }
              }];
             
         }
         else
         {
             
             DLog(@"awake from sleep");
         }
         
     }];
}

+(void)getFacebookCredentialsWithFaceBookAccount:(ACAccount*)facebookAccount andAccountStore:(ACAccountStore*)accountStore
{
    
    NSURL *requestURL = [NSURL URLWithString:@"https://graph.facebook.com/me"];
    
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodGET URL:requestURL parameters:nil];
    request.account = facebookAccount;
    
    [request performRequestWithHandler:^(NSData *data, NSHTTPURLResponse *response, NSError *error) {
        
        if(!error)
        {
            
            NSDictionary *list =[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            DLog(@"Dictionary contains: %@", list );
            
            NSString    *fbID = [NSString stringWithFormat:@"%@", [list objectForKey:@"id"]];
            NSString    *globalFBID = fbID;
            
            NSString    *gender = [NSString stringWithFormat:@"%@", [list objectForKey:@"gender"]];
            NSString    *playerGender = [NSString stringWithFormat:@"%@", gender];
            DLog(@"Gender : %@", playerGender);
            
            
            NSString    *globalmailID   = [NSString stringWithFormat:@"%@",[list objectForKey:@"email"]];
            DLog(@"global mail ID : %@",globalmailID);
            
            NSString    * fbname = [NSString stringWithFormat:@"%@",[list objectForKey:@"name"]];
            DLog(@"faceboooookkkk name %@",fbname);
            
            if([list objectForKey:@"error"]!=nil)
            {
                [self attemptRenewCredentialswithAccount:accountStore andFacebookAccount:facebookAccount];
                
            }
            //            dispatch_async(dispatch_get_main_queue(),^{
            //
            //            });
        }
        else
        {
            
            //handle error gracefully
            NSLog(@"error from get%@",error);
            
        }
        
    }];
    
}


+(void)attemptRenewCredentialswithAccount:(ACAccountStore*)accountStore andFacebookAccount:(ACAccount*)fbAccount
{
    [accountStore renewCredentialsForAccount:(ACAccount *)fbAccount completion:^(ACAccountCredentialRenewResult renewResult, NSError *error){
        if(!error)
        {
            switch (renewResult) {
                case ACAccountCredentialRenewResultRenewed:
                    DLog(@"Good to go");
                    [self getFacebookCredentialsWithFaceBookAccount:fbAccount andAccountStore:accountStore];
                    break;
                case ACAccountCredentialRenewResultRejected:
                    DLog(@"User declined permission");
                    break;
                case ACAccountCredentialRenewResultFailed:
                    DLog(@"non-user-initiated cancel, you may attempt to retry");
                    break;
                default:
                    break;
            }
            
        }
        else{
            //handle error gracefully
            DLog(@"error from renew credentials%@",error);
        }
    }];
}


-(void)FbImplementMethodwithCompletetion:(void (^)(NSDictionary *data))completion WithFailure:(void (^)(NSString *error))failure
{
    
    [self isSessionValid]?@"gud":[FBSession.activeSession closeAndClearTokenInformation];
    
    
    if(!_accountStore)
        _accountStore = [[ACAccountStore alloc] init];
    
    ACAccountType *facebookTypeAccount = [_accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    [_accountStore requestAccessToAccountsWithType:facebookTypeAccount
                                           options:@{ACFacebookAppIdKey: @"390898604442588", ACFacebookPermissionsKey: @[@"email"]}
                                        completion:^(BOOL granted, NSError *error)
     {
         // if(granted){
         NSArray *accounts = [_accountStore accountsWithAccountType:facebookTypeAccount];
         _facebookAccount = [accounts lastObject];
         DLog(@"Success");
         
         NSURL *meurl = [NSURL URLWithString:@"https://graph.facebook.com/me"];
         
         SLRequest *merequest = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                                   requestMethod:SLRequestMethodGET
                                                             URL:meurl
                                                      parameters:@{ @"fields": @"email,first_name,id,name,last_name",}];
         
         merequest.account = _facebookAccount;
         
         [merequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
          {
              NSString *meDataString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
              
              DLog(@"meta data string %@", meDataString);
              
              NSDictionary *list =[NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
              
              completion((NSDictionary *)list);
              
          }];
         
     }];
    
}


@end
