/**
 * Copyright (c) 2011 Muh Hon Cheng
 * Created by honcheng on 28/4/11.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 * WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 * SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @author 		Muh Hon Cheng <honcheng@gmail.com>
 * @copyright	2011	Muh Hon Cheng
 * @version
 *
 */

#import "PCLineChartView.h"

@implementation PCLineChartViewComponent

- (id)init {
    self = [super init];
    if (self) {
        _labelFormat = @"%.1f%%";
    }
    return self;
}

@end

@implementation PCLineChartView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        _interval = 10;
        _maxValue = 0;
        _minValue = -0;
        _yLabelFont = [UIFont boldSystemFontOfSize:8];
        _xLabelFont = [UIFont boldSystemFontOfSize:8];
        _valueLabelFont = [UIFont boldSystemFontOfSize:8];
        _legendFont = [UIFont boldSystemFontOfSize:8];
        _numYIntervals = 5;
        _numXIntervals = 1;
        _yLabelAlignment = NSTextAlignmentRight;
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    UIGraphicsPushContext(ctx);
    
    CGContextSetRGBFillColor(ctx, 72.0f/255.0f, 72.0f/255.0f, 72.0f/255.0f, 1);
    int n_div;
    int power;
    float scale_min, scale_max, div_height;
    float top_margin = 10;
    float bottom_margin = 30;
    float x_label_height = 30;
    
    if (self.autoscaleYAxis) {
        scale_min = 0.0;
        power = floor(log10(self.maxValue/5));
        float increment = self.maxValue / (5 * pow(10,power));
        increment = (increment <= 5) ? ceil(increment) : 10;
        increment = increment * pow(10,power);
        scale_max = 5 * increment;
        self.interval = scale_max / self.numYIntervals;
    } else {
        scale_min = self.minValue;
        scale_max = self.maxValue;
    }
    NSLog(@"%f",self.interval);
    n_div = (scale_max-scale_min)/self.interval + 1;
    div_height = (self.frame.size.height-top_margin-bottom_margin-x_label_height)/(n_div-1);
    
    for (int i=0; i<n_div; i++) {
        float y_axis = scale_max - i*self.interval;
        
        int y = top_margin + div_height*i;
        CGRect textFrame = CGRectMake(0,y-4,25,20);
        
        NSString *formatString = [NSString stringWithFormat:@"%%.%if", (power < 0) ? -power : 0];
        NSString *text;
        if (self.mappedYLabels != nil) {
            NSUInteger key = [[NSString stringWithFormat:formatString, y_axis] integerValue];
            text = [self.mappedYLabels objectForKey:[NSNumber numberWithInteger:key]];
        } else {
            NSLog(@"%@",[NSString stringWithFormat:@"%f", y_axis]);
            text = [NSString stringWithFormat:@"%.0f", y_axis];
            //text = [NSString stringWithFormat:@"%f", y_axis];
        }
        
        int yaxis=[text intValue];
        
        NSLog(@"%d",yaxis);
        //        if ([text isEqualToString:@"10"]||[text isEqualToString:@"20"]||[text isEqualToString:@"40"]||[text isEqualToString:@"50"]||[text isEqualToString:@"70"]||[text isEqualToString:@"80"]) {
        //
        //
        //        }
        NSMutableArray *tableArr=[[NSMutableArray alloc]init];
        for (int i=0; i<yaxis; i++) {
            int tableee=i*30;

            [tableArr addObject:[NSNumber numberWithInt:tableee]];

        }
        
        if ([tableArr containsObject:[NSNumber numberWithInt:yaxis]]||yaxis==0) {
            [text drawInRect:textFrame withFont:self.yLabelFont lineBreakMode:NSLineBreakByWordWrapping alignment:self.yLabelAlignment];
        }
        else
        {
            
        }

        
        // These are "grid" lines
        CGContextSetLineWidth(ctx, 1);
        
        //Color For Graph Background Lines
        CGContextSetRGBStrokeColor(ctx, 212.0f/255.0f, 212.0f/255.0f, 212.0f/255.0f, 1);
        CGContextMoveToPoint(ctx, 40, y);
        
        CGContextAddLineToPoint(ctx, self.frame.size.width-40, y);
        
        
        CGContextStrokePath(ctx);
    }
    
    float margin = 45;
    float div_width;
    if ([self.xLabels count] == 1) {
        div_width = 0;
    } else {
        div_width = (self.frame.size.width-2*margin)/([self.xLabels count]-1);
    }
    
    for (NSUInteger i=0; i<[self.xLabels count]; i++) {
        if (i % self.numXIntervals == 0 || self.numXIntervals==0) {
            int x = (int) (margin + div_width * i);
            NSString *x_label = [NSString stringWithFormat:@"%@", [self.xLabels objectAtIndex:i]];
            CGRect textFrame = CGRectMake(x - 100, self.frame.size.height - x_label_height, 200, x_label_height);
            
            [x_label drawInRect:textFrame
                       withFont:self.xLabelFont
                  lineBreakMode:NSLineBreakByWordWrapping
                      alignment:NSTextAlignmentCenter];
            
        };
    }
    
    //Graph DOts Shadow Color
    CGColorRef shadowColor = [[UIColor clearColor] CGColor];
    CGContextSetShadowWithColor(ctx, CGSizeMake(0,-1), 1, shadowColor);
    
    NSMutableArray *legends = [NSMutableArray array];
    
    float circle_diameter = 4;
    float circle_stroke_width = 4;
    //Graph Lines WIdth
    float line_width = 1.5;
    
    NSLog(@"%@",self.components);
    for (PCLineChartViewComponent *component in self.components) {
        int last_x = 0;
        int last_y = 0;
        
        if (!component.colour) {
            component.colour = PCColorLineBlack;
        }
        NSLog(@"%@",component.points);
        for (int x_axis_index=0; x_axis_index<[component.points count]; x_axis_index++) {
            id object = [component.points objectAtIndex:x_axis_index];
            
            if (object!=[NSNull null] && object) {
                
                float value = [object floatValue];
                if(value!=0)
                {
                    CGContextSetStrokeColorWithColor(ctx, [component.colour CGColor]);
                    CGContextSetLineWidth(ctx, circle_stroke_width);
                    
                    int x = margin + div_width*x_axis_index;
                    int y = top_margin + (scale_max-value)/self.interval*div_height;
                    
                    
                    NSString *str=[NSString stringWithFormat:@"%@",[component.points objectAtIndex:x_axis_index]];
                    if ([str isEqualToString:@"-8"]) {
                        CGRect circleRect = CGRectMake(x-circle_diameter/2, y-circle_diameter/2, 6,7);
                        
                        [self.delegate addImageView:circleRect];
                        
                        
                        
                        
                    }
                    else
                    {
                        CGRect circleRect = CGRectMake(x-circle_diameter/2, y-circle_diameter/2, circle_diameter,circle_diameter);
                        CGContextStrokeEllipseInRect(ctx, circleRect);
                        
                        CGContextSetFillColorWithColor(ctx, [component.colour CGColor]);
                    }
                    
                    if (last_x!=0 && last_y!=0) {
                        NSString *str=[NSString stringWithFormat:@"%@",[component.points objectAtIndex:x_axis_index]];
                        if([str isEqualToString:@"-8"])
                        {
                            
                            float distance = 0;
                            float last_x1 = 0;
                            float last_y1 = 0;
                            float x1 = 0;
                            float y1 = 0;
                            
                            CGContextSetLineWidth(ctx, line_width);
                            CGContextMoveToPoint(ctx, last_x1, last_y1);
                            CGContextAddLineToPoint(ctx, x1, y1);
                            CGContextStrokePath(ctx);
                            
                        }
                        else
                        {
                            
                            float distance = sqrt( pow(x-last_x, 2) + pow(y-last_y,2) );
                            float last_x1 = last_x + (circle_diameter/2) / distance * (x-last_x);
                            float last_y1 = last_y + (circle_diameter/2) / distance * (y-last_y);
                            float x1 = x - (circle_diameter/2) / distance * (x-last_x);
                            float y1 = y - (circle_diameter/2) / distance * (y-last_y);
                            CGContextSetLineWidth(ctx, line_width);
                            CGContextMoveToPoint(ctx, last_x1, last_y1);
                            CGContextAddLineToPoint(ctx, x1, y1);
                            CGContextStrokePath(ctx);
                            
                            
                            NSLog(@"Compontnt %@",[component.points objectAtIndex:x_axis_index]);
                            
                        }
                        
                        
                    }
                    
                    if (x_axis_index==[component.points count]-1) {
                        NSMutableDictionary *info = [NSMutableDictionary dictionary];
                        if (component.title) {
                            [info setObject:component.title forKey:@"title"];
                        }
                        [info setObject:[NSNumber numberWithFloat:x+circle_diameter/2+15] forKey:@"x"];
                        [info setObject:[NSNumber numberWithFloat:y-10] forKey:@"y"];
                        [info setObject:component.colour forKey:@"colour"];
                        [legends addObject:info];
                    }
                    
                    if([str isEqualToString:@"-8"])
                    {
                        last_x=0;
                        last_y=0;
                    }
                    else
                    {
                        last_x = x;
                        last_y = y;
                    }
                }
                
            }
        }
    }
    
    for (int i=0; i<[self.xLabels count]; i++) {
        int y_level = top_margin;
        
        for (int j=0; j<[self.components count]; j++) {
            NSArray *items = [[self.components objectAtIndex:j] points];
            id object = [items objectAtIndex:i];
            if (object!=[NSNull null] && object) {
                float value = [object floatValue];
                int x = margin + div_width*i;
                int y = top_margin + (scale_max-value)/self.interval*div_height;
                int y1 = y - circle_diameter/2 - self.valueLabelFont.pointSize;
                int y2 = y + circle_diameter/2;
                
                if ([[self.components objectAtIndex:j] shouldLabelValues]) {
                    if (y1 > y_level) {
                        CGContextSetRGBFillColor(ctx, 0.0f, 0.0f, 0.0f, 1.0f);
                        NSString *perc_label = [NSString stringWithFormat:[[self.components objectAtIndex:j] labelFormat], value];
                        CGRect textFrame = CGRectMake(x-25,y1, 50,20);
                        [perc_label drawInRect:textFrame
                                      withFont:self.valueLabelFont
                                 lineBreakMode:NSLineBreakByWordWrapping
                                     alignment:NSTextAlignmentCenter];
                        y_level = y1 + 20;
                    }
                    else if (y2 < y_level+20 && y2 < self.frame.size.height-top_margin-bottom_margin) {
                        CGContextSetRGBFillColor(ctx, 0.0f, 0.0f, 0.0f, 1.0f);
                        
                        NSString *perc_label = [NSString stringWithFormat:[[self.components objectAtIndex:j] labelFormat], value];
                        CGRect textFrame = CGRectMake(x-25,y2, 50,20);
                        [perc_label drawInRect:textFrame
                                      withFont:self.valueLabelFont
                                 lineBreakMode:NSLineBreakByWordWrapping
                                     alignment:NSTextAlignmentCenter];
                        y_level = y2 + 20;
                    }
                    else {
                        CGContextSetRGBFillColor(ctx, 0.0f, 0.0f, 0.0f, 1.0f);
                        NSString *perc_label = [NSString stringWithFormat:[[self.components objectAtIndex:j] labelFormat], value];
                        CGRect textFrame = CGRectMake(x-50,y-10, 50,20);
                        [perc_label drawInRect:textFrame
                                      withFont:self.valueLabelFont
                                 lineBreakMode:NSLineBreakByWordWrapping
                                     alignment:NSTextAlignmentCenter];
                        y_level = y1 + 20;
                    }
                }
                if (y+circle_diameter/2>y_level) y_level = y+circle_diameter/2;
            }
        }
    }
    
    NSSortDescriptor *sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"y" ascending:YES];
    [legends sortUsingDescriptors:[NSArray arrayWithObject:sortDesc]];
    
    //CGContextSetRGBFillColor(ctx, 0.0f, 0.0f, 0.0f, 1.0f);
    float y_level = 0;
    for (NSMutableDictionary *legend in legends) {
        NSLog(@"Legends : %@",legend);
        UIColor *colour = [legend objectForKey:@"colour"];
        CGContextSetFillColorWithColor(ctx, [colour CGColor]);
        
        NSString *title = [legend objectForKey:@"title"];
        float x = [[legend objectForKey:@"x"] floatValue];
        float y = [[legend objectForKey:@"y"] floatValue];
        if (y<y_level) {
            y = y_level;
        }
        
        CGRect textFrame = CGRectMake(x,y,margin,15);
        [title drawInRect:textFrame withFont:self.legendFont];
        
        y_level = y + 15;
    }
}

@end
