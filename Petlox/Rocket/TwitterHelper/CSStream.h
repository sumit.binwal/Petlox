//
//  CSStream.h
//  CSTwitterEngine
//
//  Created by BSM on 26/11/14.
//  Copyright (c) 2014 BSM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSTwitterEngine.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

#define k_Consumer_Key @"FQOPczt8ZtcSeTi2QzyfM6cUQ"
#define k_Secreat_Key @"HpqWgjDwfEoK4OTua3CQ0z1IWYTvtoHrix6hS7QtPcsbPuRTd4"

typedef void (^DTTwitterCompletionBlock)(NSDictionary *twitterDetail, ACAccount *twitterAccount);
typedef void (^DTTwitterErrorBlock)(NSError *error);

@interface CSStream : NSObject <UIAlertViewDelegate>

@property (strong,nonatomic) DTTwitterCompletionBlock callback;

// available class methods
+ (void) loginWithCompletion:(DTTwitterCompletionBlock) successBlock andError:(DTTwitterErrorBlock) errorBlock;
+ (void) requestLoginWithAccount:(ACAccount *) account completionBlock:(void (^)(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)) callbackHandler;


@end
