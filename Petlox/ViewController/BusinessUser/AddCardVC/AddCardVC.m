//
//  AddCardVC.m
//  Petlox
//
//  Created by Sumit Sharma on 02/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "AddCardVC.h"
#import "CCDetailVC.h"
@interface AddCardVC ()<UITextFieldDelegate,UIGestureRecognizerDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    IBOutlet UIButton *btnAddCrdtCard;
    
    IBOutlet UITextField *txtCardNumber;
    IBOutlet UITextField *txtCVV;
    IBOutlet UITextField *txtCardExpiry;
    IBOutlet UITextField *txtCardName;
    
    IBOutlet UIImageView *imgCardType;
    
    IBOutlet UIButton *btnNext;
    UITextField *activeTf;
    
    IBOutlet UIView *toolbarView;
    IBOutlet UIView *datePickerVw;
    
    IBOutlet UIBarButtonItem *barBtnNext;
    IBOutlet UIScrollView *scrllVw;
    
    NSString *strCardID;
    
    IBOutlet UIPickerView *pickerVw;
    
    NSMutableArray *monthArr;
    NSMutableArray *yearArr;
    NSString *mnth;
    NSString *year;
}
@end

@implementation AddCardVC
@synthesize cardType,cardDataDict;
#pragma mark - Life Cycle Method
- (void)viewDidLoad {
    wbServiceCount=1;
    [super viewDidLoad];
    [self setUpView];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    NSLog(@"%@",cardDataDict);
    if (cardDataDict.count>0) {
        NSString *cardStr=[cardDataDict objectForKey:@"cardNumber"];
        [self.navigationItem setTitle:@"Update Credit Card"];
        [btnAddCrdtCard setTitle:@"Update Credit Card" forState:UIControlStateNormal];
        cardStr=[cardStr stringByReplacingOccurrencesOfString:@"X" withString:@""];
        NSString *strLastName;
        if ([self isNotNull:[cardDataDict objectForKey:@"customerlastname"]]) {
            strLastName=[cardDataDict objectForKey:@"customerlastname"];
        }
        else
        {
           strLastName=@"";        }
        
        
        
        txtCardNumber.text=[NSString stringWithFormat:@"XXXX XXXX XXXX %@",cardStr];
        txtCardName.text=[NSString stringWithFormat:@"%@ %@",[cardDataDict objectForKey:@"customerfirstname"],strLastName];
        
        NSArray* foo = [[cardDataDict objectForKey:@"expirationDate"] componentsSeparatedByString: @"-"];
        year=[foo objectAtIndex: 0];
        
        year = [year substringFromIndex:2];
        
        mnth=[foo objectAtIndex: 1];
        txtCardExpiry.text=[NSString stringWithFormat:@"%@/%@",mnth,year];
        txtCVV.text=@"XXX";
        strCardID=[cardDataDict objectForKey:@"card_id"];
        
        cardType=[cardDataDict objectForKey:@"cardtype"];
        
        if ([cardType isEqualToString:@"visa"]) {
            [imgCardType setImage:[UIImage imageNamed:@"VisaCardSelected"]];
        }
        else if ([cardType isEqualToString:@"master"])
        {
            [imgCardType setImage:[UIImage imageNamed:@"MasterCardSelected"]];
        }
        else if ([cardType isEqualToString:@"american"])
        {
            [imgCardType setImage:[UIImage imageNamed:@"AmericanCardSelected"]];
        }
        else if ([cardType isEqualToString:@"discover"])
        {
            [imgCardType setImage:[UIImage imageNamed:@"DiscoverCardSelected"]];
        }
        else if ([cardType isEqualToString:@"paypal"])
        {
            [imgCardType setImage:[UIImage imageNamed:@"PaypalCardSelected"]];
        }
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Setup View and its Elements
-(void)setUpView
{
//Setup Navigation bar
    [CommonFunctions setNavigationBar:self.navigationController];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];

    [self.navigationItem setTitle:@"Add Payment Method"];
    
    btnNext.layer.cornerRadius=btnNext.bounds.size.width/2*SCREEN_XScale;
    [btnNext.titleLabel setFont:[UIFont fontWithName:btnNext.titleLabel.font.fontName size:btnNext.titleLabel.font.pointSize*SCREEN_XScale]];

 //Change Textfield Placeholder Color
    [txtCardExpiry setValue:[UIColor colorWithRed:129.0f/255.0f green:128.0f/255.0f blue:128.0f/255.0f alpha:1]
              forKeyPath:@"_placeholderLabel.textColor"];
    [txtCardName setValue:[UIColor colorWithRed:129.0f/255.0f green:128.0f/255.0f blue:128.0f/255.0f alpha:1]
                   forKeyPath:@"_placeholderLabel.textColor"];
    [txtCardNumber setValue:[UIColor colorWithRed:129.0f/255.0f green:128.0f/255.0f blue:128.0f/255.0f alpha:1]
                  forKeyPath:@"_placeholderLabel.textColor"];
    [txtCVV setValue:[UIColor colorWithRed:129.0f/255.0f green:128.0f/255.0f blue:128.0f/255.0f alpha:1]
                 forKeyPath:@"_placeholderLabel.textColor"];

    
//Set Card Images
    if ([cardType isEqualToString:@"visa"]) {
        [imgCardType setImage:[UIImage imageNamed:@"VisaCardSelected"]];
    }
    else if ([cardType isEqualToString:@"master"])
    {
        [imgCardType setImage:[UIImage imageNamed:@"MasterCardSelected"]];
    }
    else if ([cardType isEqualToString:@"american"])
    {
        [imgCardType setImage:[UIImage imageNamed:@"AmericanCardSelected"]];
    }
    else if ([cardType isEqualToString:@"discover"])
    {
        [imgCardType setImage:[UIImage imageNamed:@"DiscoverCardSelected"]];
    }
    else if ([cardType isEqualToString:@"paypal"])
    {
        [imgCardType setImage:[UIImage imageNamed:@"PaypalCardSelected"]];
    }
    txtCardNumber.inputAccessoryView=toolbarView;
    txtCVV.inputAccessoryView=toolbarView;
    txtCardExpiry.inputView=datePickerVw;
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTapGestureClicked)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];
    monthArr = [[NSMutableArray alloc]init];
        yearArr = [[NSMutableArray alloc]init];
    for (int i=1; i<=12; i++) {
        NSString *str=[NSString stringWithFormat:@"%d",i];
        [monthArr addObject:str];
    }
    for (int i=15; i<=54; i++) {
        NSString *str=[NSString stringWithFormat:@"%d",i];
        [yearArr addObject:str];
    }

}


#pragma mark - TextFiled Validate Methods
-(BOOL)isTextFiledValidate
{
    if (![CommonFunctions isValueNotEmpty:txtCardNumber.text]) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter card number."];
        return NO;
    }
    else if (txtCardNumber.text.length<15) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter atleast 15 or 16 digit card number."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtCardName.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter card holder name."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtCardExpiry.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter card expiry date."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtCVV.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter card cvv number."];
        return NO;
    }
    else if ([[[UIDevice currentDevice]systemVersion] floatValue] < 8.0)
    {
        
        if ([txtCardNumber.text rangeOfString:@"X"].location != NSNotFound)
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please enter correct card number."];
            return NO;
        }
        else if ([txtCardExpiry.text rangeOfString:@"X"].location != NSNotFound)
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please enter correct card expiry date."];
            return NO;
        }
        else if ([txtCVV.text rangeOfString:@"X"].location != NSNotFound)
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please enter correct card cvv number."];
            return NO;
        }
        
    }
    else
    {
        if ([txtCardNumber.text containsString:@"X"])
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please enter correct card number."];
            return NO;
        }
        else if ([txtCardExpiry.text containsString:@"X"])
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please enter correct card expiry date."];
            return NO;
        }
        else if ([txtCVV.text containsString:@"X"])
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please enter correct card cvv number."];
            return NO;
        }

    }
    
    return YES;
}

#pragma mark - IBAction Methods
-(void)singleTapGestureClicked
{
[self.view endEditing:YES];
}
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)toolbarNextBtnClicked:(id)sender {
    if (txtCardNumber==activeTf) {
        
        [txtCardName becomeFirstResponder];
    }
    else if (txtCardExpiry==activeTf)
    {
        txtCardExpiry.text=[NSString stringWithFormat:@"%@/%@",mnth,year];
        [txtCVV becomeFirstResponder];

    }
    else if (txtCVV==activeTf)
    {
        [self.view endEditing:YES];
    }
}
- (IBAction)toolbarCancelBtnClicked:(id)sender {
    [self.view endEditing:YES];
}
- (IBAction)addCreditCardButtonClicked:(id)sender {
    [self.view endEditing:YES];
    if ([self isTextFiledValidate]) {
        if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self addCreditCardDetailonServer];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
        }
    }
}

#pragma mark - UITextField Delegate Method

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField==txtCardNumber) {
        [barBtnNext setTitle:@"Next"];
    }
    else if (textField==txtCVV)
    {
        [barBtnNext setTitle:@"Done"];
    }
    activeTf=textField;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==txtCardName) {
        [txtCardExpiry becomeFirstResponder];
    }
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField==txtCardNumber) {
        if (range.location<22) {
            
            
            if ([string isEqualToString:@""]) {
                txtCardNumber.text=[txtCardNumber.text stringByReplacingOccurrencesOfString:@"" withString:@""];
                }
            else if (textField.text.length==4) {
                txtCardNumber.text=[NSString stringWithFormat:@"%@  ",textField.text];
            }
            else if (textField.text.length==10)
            {
                txtCardNumber.text=[NSString stringWithFormat:@"%@  ",textField.text];
            }
            else if (textField.text.length==16)
            {
                txtCardNumber.text=[NSString stringWithFormat:@"%@  ",textField.text];
            }
            return YES;
        }
        else
        {
        return NO;
        }
    }
    else if (textField==txtCVV)
    {
        if (range.location==4) {
            return NO;
        }
    }
    return YES;
}

#pragma mark - UIPickerView Delegate Method
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component==0) {
        return monthArr.count;
    }
    else
    {
        return yearArr.count;
    }
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component==0) {
        mnth=[monthArr objectAtIndex:row];
        return [monthArr objectAtIndex:row];
    }
    else
    {
        year=[yearArr objectAtIndex:row];
        return [NSString stringWithFormat:@"20%@",[yearArr objectAtIndex:row]];
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component==0) {
        mnth=[monthArr objectAtIndex:row];
    }
    else
    {
        year=[yearArr objectAtIndex:row];
    }
}

#pragma mark- WebServices API..

-(void)addCreditCardDetailonServer
{
    NSString *cardExp=[NSString stringWithFormat:@"20%@-%@",year,mnth];
    
    NSLog(@"%@",[CommonFunctions trimSpaceInString:txtCardNumber.text]);
    NSMutableDictionary *param;
    
    NSString *url = [NSString stringWithFormat:@"save_card"];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];

    
    if (cardDataDict.count>0) {
             param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:cardType,@"cardtype",txtCardName.text,@"ccname",[CommonFunctions trimSpaceInString:txtCardNumber.text],@"ccnumber",cardExp,@"ccexpdate",txtCVV.text,@"cvccode",strCardID,@"card_id",cardDataDict[@"customerId"] ,@"customer_id",nil];
    }
    else
    {
        param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:cardType,@"cardtype",txtCardName.text,@"ccname",[CommonFunctions trimSpaceInString:txtCardNumber.text],@"ccnumber",cardExp,@"ccexpdate",txtCVV.text,@"cvccode", nil];
    }
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
                     //http://192.168.0.173/petlox_svn/mobile/saveCreditCard

    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            CCDetailVC *ccdvc=[[CCDetailVC alloc]init];
            [self.navigationController pushViewController:ccdvc animated:YES];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self addCreditCardDetailonServer];
                                              }
                                              else
                                              {

                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              

                                          }
                                      }];
}

@end
