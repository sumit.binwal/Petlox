//
//  BillingWeekHistoryVC.m
//  Petlox
//
//  Created by Sumit Sharma on 07/11/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "BillingHistoryVC.h"
#import "BillingHistoryCustomeCell.h"
#import "InvoiceTransactionVC.h"
@interface BillingHistoryVC ()<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *arrInvoiceData;
    IBOutlet UITableView *tblVwBillingHistory;
}
@end

@implementation BillingHistoryVC
@synthesize strStrtDate,strendDate;
- (void)viewDidLoad {
    [super viewDidLoad];
    wbServiceCount=1;
    // Do any additional setup after loading the view from its nib.
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Billing History"];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getWeekBillingData];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }
    
}
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrInvoiceData.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 42.0f;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"BillingHistoryCustomeCell";
    
    BillingHistoryCustomeCell *cell=[[BillingHistoryCustomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    cell.backgroundColor=[UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    cell.lblInvoiceAmount.text=[NSString stringWithFormat:@"$ %@",[[arrInvoiceData objectAtIndex:indexPath.row] objectForKey:@"amount"]];
    cell.lblInvoiceTitle.text=[NSString stringWithFormat:@"Invoice - %@",[[arrInvoiceData objectAtIndex:indexPath.row] objectForKey:@"transaction_id"]];
    if ([[[arrInvoiceData objectAtIndex:indexPath.row] objectForKey:@"status"]isEqualToString:@"1"]) {
        [cell.imgInvoiceStatus setImage:[UIImage imageNamed:@"checkMarkActive"]];
    }
    else
    {
        [cell.imgInvoiceStatus setImage:[UIImage imageNamed:@"checkMarkInactive"]];
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    InvoiceTransactionVC *itvc=[[InvoiceTransactionVC alloc]init];
    itvc.strReservationID=[[arrInvoiceData objectAtIndex:indexPath.row] objectForKey:@"reservation_id"];
    [self.navigationController pushViewController:itvc animated:YES];
}

#pragma mark- WebService Calling Methods
-(void)getWeekBillingData
{
    NSString *url;
    url = [NSString stringWithFormat:@"weeklyBillingHistory"];
     NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"user_id",strendDate,@"weekenddate",strStrtDate,@"weekstartdate", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://mymeetingdesk.com/mobile/petlox/mobile/weeklyBillingHistory
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            arrInvoiceData=[responseDict objectForKey:@"data"];
            
            [tblVwBillingHistory reloadData];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              [CommonFunctions removeActivityIndicator];
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self getWeekBillingData];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}

@end
