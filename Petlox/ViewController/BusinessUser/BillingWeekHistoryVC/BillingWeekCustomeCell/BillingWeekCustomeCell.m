//
//  BillingWeekCustomeCell.m
//  Petlox
//
//  Created by Sumit Sharma on 09/11/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "BillingWeekCustomeCell.h"

@implementation BillingWeekCustomeCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    return self=[[[NSBundle mainBundle]loadNibNamed:@"BillingWeekCustomeCell" owner:self options:nil] objectAtIndex:0];
}
- (void)awakeFromNib {
    // Initialization code
    [_lblTitle setFont:[UIFont fontWithName:_lblTitle.font.fontName size:_lblTitle.font.pointSize*SCREEN_XScale]];
    [_lblAmount setFont:[UIFont fontWithName:_lblAmount.font.fontName size:_lblAmount.font.pointSize*SCREEN_XScale]];
    [_lblWeekTime setFont:[UIFont fontWithName:_lblWeekTime.font.fontName size:_lblWeekTime.font.pointSize*SCREEN_XScale]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
