//
//  BillingWeekHistoryVC.m
//  Petlox
//
//  Created by Sumit Sharma on 07/11/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "BillingWeekHistoryVC.h"
#import "BillingWeekCustomeCell.h"
#import "BillingHistoryVC.h"
@interface BillingWeekHistoryVC ()<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *arrBillingHistory;
    IBOutlet UITableView *tblVwBillingHistrr;
    IBOutlet UILabel *lblErrorMsg;
    
    NSArray *arrBillingSectionTitle;
    NSMutableDictionary *dictBillingData;
}
@end

@implementation BillingWeekHistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    wbServiceCount=1;
    // Do any additional setup after loading the view from its nib.
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Billing History"];
    
    
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getBillingHistoryData];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection first."];
    }
    
    
    NSArray *viewControllers = self.navigationController.viewControllers;
    UIViewController *rootViewController = (UIViewController *)[viewControllers objectAtIndex:0];
    
    NSLog(@"%@",rootViewController);
    
    
    if ([rootViewController isKindOfClass:[BillingWeekHistoryVC class]]) {
        UIBarButtonItem *backButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStylePlain target:self.viewDeckController action:@selector(toggleLeftView)];
        [self.navigationItem setLeftBarButtonItem:backButton];
    }
    else{
        UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
        btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
        [self.navigationItem setLeftBarButtonItem:btn];
            }

}
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrBillingSectionTitle.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *sectionTitle = [arrBillingSectionTitle objectAtIndex:section];
    
    NSMutableArray *sectionDates = [dictBillingData objectForKey:sectionTitle] ;
    return [sectionDates count];
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *myLabel = [[UILabel alloc] init];
    myLabel.frame = CGRectMake(20, 1, 320, 20);
    myLabel.font = [UIFont fontWithName:FONT_GULIM size:14.0f];
    if (arrBillingSectionTitle.count>0) {
        myLabel.text =[arrBillingSectionTitle objectAtIndex:section];
    }
    else
    {
        myLabel.text=@"";
    }
    
    [myLabel setFont:[UIFont fontWithName:FONT_OPENSANS_BOLD size:13.0F*SCREEN_XScale]];
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320.0f, 30.0f)];
    headerView.backgroundColor=[UIColor colorWithRed:239.0f/255.0f green:239.0f/255.0f blue:239.0f/255.0f alpha:1];
    [headerView addSubview:myLabel];
    
    return headerView;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 46.0f*SCREEN_XScale;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"BillingWeekCustomeCell";
    
    BillingWeekCustomeCell *cell=[[BillingWeekCustomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor clearColor];
    
    NSString *sectionTitle = [arrBillingSectionTitle objectAtIndex:indexPath.section];
    NSArray *sectionUser = [dictBillingData objectForKey:sectionTitle];
    NSString *userList = [sectionUser objectAtIndex:indexPath.row];
    NSLog(@"%@",userList);
    
    if (sectionUser.count>0) {
        lblErrorMsg.text=@"";
        cell.lblTitle.text=@"Invoice";
        if ([self isNotNull:[[sectionUser objectAtIndex:indexPath.row] objectForKey:@"weekly_payment_amount"]]) {
            cell.lblAmount.text=[NSString stringWithFormat:@"$ %@",[[sectionUser objectAtIndex:indexPath.row] objectForKey:@"weekly_payment_amount"]];
        }
        else
        {
            cell.lblAmount.text=@"$ 0.00";
        }
        cell.lblWeekTime.text=[[sectionUser objectAtIndex:indexPath.row] objectForKey:@"weekly_date"];
        

    }
    else
    {
        lblErrorMsg.text=@"No Record Found.";
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *sectionTitle = [arrBillingSectionTitle objectAtIndex:indexPath.section];
    NSArray *sectionUser = [dictBillingData objectForKey:sectionTitle];
    NSString *userList = [sectionUser objectAtIndex:indexPath.row];

    
    BillingHistoryVC *bhvc=[[BillingHistoryVC alloc]initWithNibName:@"BillingHistoryVC" bundle:nil];
    bhvc.strendDate=[[sectionUser objectAtIndex:indexPath.row] objectForKey:@"weekenddate"];
    bhvc.strStrtDate=[[sectionUser objectAtIndex:indexPath.row] objectForKey:@"weekstartdate"];
    [self.navigationController pushViewController:bhvc animated:YES];
}

#pragma mark- WebService Calling Methods
-(void)getBillingHistoryData
{
    NSString *url;
   url = [NSString stringWithFormat:@"monthlyBillingHistory"];
     NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"user_id", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://mymeetingdesk.com/mobile/petlox/mobile/monthlyBillingHistory
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            
            NSMutableArray *tempArry=[[NSMutableArray alloc]init];
            tempArry=[responseDict objectForKey:@"data"];
            arrBillingHistory=tempArry;
       
            
            
            dictBillingData=[responseDict objectForKey:@"data"];
            arrBillingSectionTitle=[[[responseDict objectForKey:@"data"] allKeys] sortedArrayUsingSelector:@selector(localizedCompare:)];
            
            NSSortDescriptor *sortOrder = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
            arrBillingSectionTitle = [[[responseDict objectForKey:@"data"] allKeys] sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortOrder]];
            
            
                 [tblVwBillingHistrr reloadData];

        }
        else if([operation.response statusCode]==206)
        {
            [tblVwBillingHistrr reloadData];
            lblErrorMsg.text=@"No Record Found";
            NSLog(@"%ld",(long)[operation.response statusCode]);
            
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
     }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self getBillingHistoryData];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}


@end
