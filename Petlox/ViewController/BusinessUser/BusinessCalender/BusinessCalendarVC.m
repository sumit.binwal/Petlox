//
//  CalendarVC.m
//  Petlox
//
//  Created by Sumit Sharma on 15/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "CalendarVC.h"
#import "VRGCalendarView.h"
#import <EventKit/EventKit.h>
@interface CalendarVC ()<VRGCalendarViewDelegate,UIAlertViewDelegate>
{
    IBOutlet UILabel *lblDate;
    VRGCalendarView *calendar;
    IBOutlet UILabel *lblMonth;
    EKEventStore *store;

}
@end

@implementation CalendarVC
@synthesize lblWeekDay,strbusiessName;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
         store= [EKEventStore new];
       // Do any additional setup after loading the view from its nib.
}

-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
   [self.navigationItem setTitle:@"Add To Calendar"];
    [self.navigationController.navigationBar setHidden:NO];
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"BackButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    [self.navigationItem setLeftBarButtonItem:btn];

    
    
calendar = [[VRGCalendarView alloc] init];
    calendar.delegate=self;
    
    [self.view addSubview:calendar];
    
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd"];
    lblDate.text = [dateFormat stringFromDate:today];
    
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    [dateFormat1 setDateFormat:@"EEEE"];
    lblWeekDay.text = [dateFormat1 stringFromDate:today];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupNewMotheName:) name:@"ChangeDate" object:nil];


}
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnAddtoCalendearClicked:(id)sender {
    UIButton *btn=(UIButton *)sender;
   // btn.tag=0;
    if (btn.tag==0) {
        [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            if (!granted)
            {
                
                return ;
            }
            EKEvent *event = [EKEvent eventWithEventStore:store];
            event.title = [NSString stringWithFormat:@"Appointed - %@",strbusiessName];
            event.startDate = [NSDate date]; //today
            event.endDate = [event.startDate dateByAddingTimeInterval:60*60*3 ];  //set 1 hour meeting
            event.calendar = [store defaultCalendarForNewEvents];
            NSError *err = nil;
            [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];

            //    self.savedEventId = event.eventIdentifier;  //save the event id if you want to access this later
        }];
        [CommonFunctions alertTitle:@"" withMessage:@"Event Added Sucessfully to Calendar." withDelegate:self withTag:100];
        btn.tag=1;
    }
    else if (btn.tag==1)
    {
            [CommonFunctions alertTitle:@"" withMessage:@"Event Allready Added Sucessfully to Calendar."];
    }
}
-(void)ViewWillDisAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)setupNewMotheName:(NSNotification *) notification
{
    NSLog(@"%@",[NSDate date]);
    lblMonth.text=[NSString stringWithFormat:@"%@",notification.object];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)calendarView:(VRGCalendarView *)calendarView switchedToMonth:(int)month targetHeight:(float)targetHeight animated:(BOOL)animated {
    //if (month==[[NSDate date]month]) {
        NSArray *dates = [NSArray arrayWithObjects:[NSNumber numberWithInt:1],[NSNumber numberWithInt:5], nil];
        [calendarView markDates:dates];

    //}
}

-(void)calendarView:(VRGCalendarView *)calendarView dateSelected:(NSDate *)date {
    NSLog(@"Selected date = %@",date);

}

#pragma mark- UIAlertView Delegate Method
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

#pragma mark- IBActionButtons
- (IBAction)rghtbtnClicked:(id)sender {
    calendar.showNextMonth;
}

- (IBAction)leftBtnClicked:(id)sender {
    calendar.showPreviousMonth;
}


@end
