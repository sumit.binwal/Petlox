//
//  BusinessClaimCustomeCell.m
//  Petlox
//
//  Created by Sumit Sharma on 25/08/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "BusinessClaimCustomeCell.h"

@implementation BusinessClaimCustomeCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[[[NSBundle mainBundle]loadNibNamed:@"BusinessClaimCustomeCell" owner:self options:nil] objectAtIndex:0];
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
