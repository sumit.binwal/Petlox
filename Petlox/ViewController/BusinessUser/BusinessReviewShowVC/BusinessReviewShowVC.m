//
//  ReviewShowVC.m
//  Petlox
//
//  Created by Sumit Sharma on 09/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "BusinessReviewShowVC.h"
#import "WriteReviewVC.h"
@interface BusinessReviewShowVC ()
{
    IBOutlet UILabel *lblTitle;
    IBOutlet UILabel *lblMsg;
    IBOutlet UIImageView *star1;
    IBOutlet UIImageView *star3;
    IBOutlet UIImageView *star4;
    IBOutlet UIImageView *star5;
    IBOutlet UIImageView *star2;
}
@end

@implementation BusinessReviewShowVC
@synthesize dictReviewPost,dictBusinessUserDetail;
- (void)viewDidLoad {
    [super viewDidLoad];
    lblTitle.text=[NSString stringWithFormat:@"%@ %@",[dictReviewPost valueForKey:@"first_name"],[dictReviewPost valueForKey:@"last_name"]];
    lblMsg.text=[dictReviewPost valueForKey:@"review"];
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Review"];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    

    
    
    if ([self isNotNull:[dictReviewPost valueForKey:@"rating"]]) {
        int avgRating=[[dictReviewPost valueForKey:@"rating"]intValue];
        switch (avgRating) {
            case 0:
            {
                break;
            }
                
            case 1:
            {
                [star1 setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
            case 2:
            {
                [star1 setImage:[UIImage imageNamed:@"star_foucs"]];
                [star2 setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
            case 3:
            {
                [star1 setImage:[UIImage imageNamed:@"star_foucs"]];
                [star2 setImage:[UIImage imageNamed:@"star_foucs"]];
                [star3 setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
            case 4:
            {
                [star1 setImage:[UIImage imageNamed:@"star_foucs"]];
                [star2 setImage:[UIImage imageNamed:@"star_foucs"]];
                [star3 setImage:[UIImage imageNamed:@"star_foucs"]];
                [star4 setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
            case 5:
            {
                [star1 setImage:[UIImage imageNamed:@"star_foucs"]];
                [star2 setImage:[UIImage imageNamed:@"star_foucs"]];
                [star3 setImage:[UIImage imageNamed:@"star_foucs"]];
                [star4 setImage:[UIImage imageNamed:@"star_foucs"]];
                [star5 setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
                
            default:
                break;
        }
        
    }

    // Do any additional setup after loading the view from its nib.
}
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
