//
//  BusinessReviewCustomeCell.h
//  Petlox
//
//  Created by Sumit Sharma on 01/12/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BusinessReviewCustomeCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgVwProfileImg;
@property (strong, nonatomic) IBOutlet UILabel *lblBusinessName;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UILabel *lblReviewDiscription;
@property (strong, nonatomic) IBOutlet UIImageView *imgStar1;
@property (strong, nonatomic) IBOutlet UIImageView *imgStar2;
@property (strong, nonatomic) IBOutlet UIImageView *imgStar3;
@property (strong, nonatomic) IBOutlet UIImageView *imgStar4;
@property (strong, nonatomic) IBOutlet UIImageView *imgStar5;

@end
