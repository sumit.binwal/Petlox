//
//  BusinessReviewCustomeCell.m
//  Petlox
//
//  Created by Sumit Sharma on 01/12/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "BusinessReviewCustomeCell.h"

@implementation BusinessReviewCustomeCell

- (void)awakeFromNib {
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
 
    return self=[[[NSBundle mainBundle]loadNibNamed:@"BusinessReviewCustomeCell" owner:self options:nil]objectAtIndex:0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
