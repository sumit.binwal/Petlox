//
//  SettingVC.m
//  Petlox
//
//  Created by Sumit Sharma on 21/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//
#import "SignInVC.h"
#import "BusinessSettingVC.h"
#import "PrivacyPolicyVC.h"
#import "ChangePasswordVC.h"
#import "NotificationVC.h"
@interface BusinessSettingVC ()
{
    
    IBOutlet UIButton *leftBtnClicked;
}
@end

@implementation BusinessSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    wbServiceCount=1;
//    [self.navigationController.navigationBar setTranslucent:YES];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStylePlain target:self.viewDeckController action:@selector(toggleLeftView)];
    [self.navigationItem setLeftBarButtonItem:btn];    // Do any additional setup after loading the view from its nib.
    [self.navigationItem setTitle:@"Settings"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - IBAction Methods
- (IBAction)logoutBtnClicked:(id)sender {
    
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self logoutUser];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network"];
    }
}
- (IBAction)leftBtnClicked:(id)sender {
}

- (IBAction)chnagePasswrdBtnClicked:(id)sender {
    ChangePasswordVC *cpvc=[[ChangePasswordVC alloc]init];
    [self.navigationController pushViewController:cpvc animated:YES];
}
- (IBAction)privacySetting:(id)sender {
    
    PrivacyPolicyVC *ppvc=[[PrivacyPolicyVC alloc]initWithNibName:@"PrivacyPolicyVC" bundle:nil];
    ppvc.strUrlString=@"http://mymeetingdesk.com/mobile/petlox/pages/privacy_business_policy";
    [self.navigationController pushViewController:ppvc animated:YES];
    
}
- (IBAction)notificationBtnclicked:(id)sender {
    NotificationVC *cpvc=[[NotificationVC alloc]init];
    [self.navigationController pushViewController:cpvc animated:YES];

}

#pragma mark - WebServiceAPI
-(void)logoutUser
{
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"user_id", nil];

    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    NSString *url = [NSString stringWithFormat:@"logout"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/userReview
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_TOKEN_ID];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_BUSINESS_TYPE];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            SignInVC *sivc=[[SignInVC alloc]init];
            APPDELEGATE.navController = [[UINavigationController alloc] initWithRootViewController:sivc];
            APPDELEGATE.window.rootViewController = APPDELEGATE.navController;
            
            
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              

                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self logoutUser];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          
                                      }];
}
@end
