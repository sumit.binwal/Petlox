//
//  BusinessSignupFirstVC.m
//  Petlox
//
//  Created by Sumit Sharma on 26/08/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "BusinessSignupFirstVC.h"
#import "BusinessSignupSecndVC.h"
#import "CallMeNowVC.h"
@interface BusinessSignupFirstVC ()<UIGestureRecognizerDelegate,UITextFieldDelegate>
{
    
    IBOutlet UILabel *lblPrivacyPolicyLabel;
    IBOutlet UIButton *btnNext;
    IBOutlet UITextField *txtPhoneNumber;
    IBOutlet UITextField *txtAddress;
        IBOutlet UITextField *txtEmailAddress;
    IBOutlet UITextField *txtBusinessName;
    IBOutlet UIView *vwToolbar;
    NSMutableDictionary *claimDataDict;
}
@end

@implementation BusinessSignupFirstVC

#pragma mark - View Controller Life Cycle Method

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    wbServiceCount=1;
    if ([self isNotNull:[[NSUserDefaults standardUserDefaults] objectForKey:UD_CLAIM_ID]])
    {
        if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self getClaimUserDetail];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Method for setupView and its content
-(void)setUpView
{
    
    txtEmailAddress.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    txtAddress.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    txtPhoneNumber.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    txtBusinessName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    
    btnNext.layer.cornerRadius=btnNext.bounds.size.height/2*SCREEN_XScale;
    [btnNext.titleLabel setFont:[UIFont fontWithName:btnNext.titleLabel.font.fontName size:btnNext.titleLabel.font.pointSize*SCREEN_XScale]];
    
    UIView *view1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 13.0*SCREEN_XScale, 0)];
    txtBusinessName.leftViewMode=UITextFieldViewModeAlways;
    txtBusinessName.leftView=view1;
    
    UIView *view2=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 13.0*SCREEN_XScale, 0)];
    txtAddress.leftView=view2;
    txtAddress.leftViewMode=UITextFieldViewModeAlways;
    
    UIView *view3=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 13.0*SCREEN_XScale, 0)];
    txtEmailAddress.leftView=view3;
    txtEmailAddress.leftViewMode=UITextFieldViewModeAlways;
    
    UIView *view4=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 13.0*SCREEN_XScale, 0)];
    txtPhoneNumber.leftView=view4;
    txtPhoneNumber.leftViewMode=UITextFieldViewModeAlways;
    
    
    txtAddress.layer.cornerRadius=5.0f;
    txtEmailAddress.layer.cornerRadius=5.0f;
    txtPhoneNumber.layer.cornerRadius=5.0f;
    txtBusinessName.layer.cornerRadius=5.0f;
    
    //Change Textfield placeholder color
    [txtAddress setValue:[UIColor colorWithRed:133.0f/255.0f green:133.0f/255.0f blue:133.0f/255.0f alpha:1]
                forKeyPath:@"_placeholderLabel.textColor"];
    [txtBusinessName setValue:[UIColor colorWithRed:133.0f/255.0f green:133.0f/255.0f blue:133.0f/255.0f alpha:1]
                forKeyPath:@"_placeholderLabel.textColor"];
    [txtPhoneNumber setValue:[UIColor colorWithRed:133.0f/255.0f green:133.0f/255.0f blue:133.0f/255.0f alpha:1]
                forKeyPath:@"_placeholderLabel.textColor"];
    [txtEmailAddress setValue:[UIColor colorWithRed:133.0f/255.0f green:133.0f/255.0f blue:133.0f/255.0f alpha:1]
                  forKeyPath:@"_placeholderLabel.textColor"];
    
    claimDataDict =[[NSMutableDictionary alloc]init];
    
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    paragraphStyle.lineSpacing = 2.5;
    
    NSDictionary *nameAttributes=@{
                                   NSParagraphStyleAttributeName : paragraphStyle,
                                   NSBaselineOffsetAttributeName:@0.0
                                   };
    
    
    NSAttributedString *string=[[NSAttributedString alloc] initWithString:@"By clicking the button below, you represent that you have authority to claim this account on behalf of this business, and agree to Petlox's Terms of Service and Privacy Policy." attributes:nameAttributes];
    lblPrivacyPolicyLabel.attributedText=string;
    
    //set Navigation and buttons
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Basic Info"];
UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
[self.navigationItem setLeftBarButtonItem:btn];
    
    txtPhoneNumber.inputAccessoryView=vwToolbar;
    
    
    //UITap Gesture
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTapClicked)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];
}
//gesture action
-(void)singleTapClicked
{
    [self.view endEditing:YES];
}
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - TextFiled Validation Method
-(BOOL)isValidateTextField
{
    if (![CommonFunctions isValueNotEmpty:txtBusinessName.text]) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter business name."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtAddress.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter business address."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtPhoneNumber.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter phone number."];
        return NO;
    }
    else if (txtPhoneNumber.text.length<5)
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter valid phone number."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtEmailAddress.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter email address."];
        return NO;
    }
    else if (![CommonFunctions IsValidEmail:txtEmailAddress.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter valid email address."];
        return NO;
    }

    return YES;
}

#pragma mark - IBAction Button Methods
- (IBAction)nextButtonClicked:(id)sender {
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:UD_CLAIM_ID]);
    if ([self isValidateTextField]) {
        if ([[NSUserDefaults standardUserDefaults]valueForKey:UD_CLAIM_ID]) {
            
            BusinessSignupSecndVC *bsvc=[[BusinessSignupSecndVC alloc]init];
            if (claimDataDict.count>1) {
                NSMutableDictionary *dataDict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:txtBusinessName.text,@"bName",txtAddress.text,@"bAddress",txtPhoneNumber.text,@"bPhnNumber",txtEmailAddress.text,@"bEmail",nil];
                
                                bsvc.dataDict=dataDict;
                
                bsvc.claimDataDict=claimDataDict;
            }
            [self.navigationController pushViewController:bsvc animated:YES];
            
        }
        else
        {
            
            if ([CommonFunctions reachabiltyCheck]) {
                [CommonFunctions showActivityIndicatorWithText:@""];
                [self verifyPhoneNumber];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection"];
            }
            
        }
    }
}

- (IBAction)toolbarDoneBtnClicked:(id)sender {
    [self.view endEditing:YES];
}


#pragma mark - UITextField Delegate Methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==txtBusinessName) {
        [txtAddress becomeFirstResponder];
    }
    else if (textField==txtAddress)
    {
        [txtPhoneNumber becomeFirstResponder];
    }
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField==txtEmailAddress) {
        txtEmailAddress.autocorrectionType = UITextAutocorrectionTypeNo;
        
    }
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField==txtPhoneNumber) {
        if (range.location>11) {
            return NO;
        }
        else
            return YES;
    }
    else if (textField==txtAddress)
    {
        if (range.location>100) {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    
        return YES;
    
    
}

#pragma mark- WebServices API..

-(void)getClaimUserDetail
{
   
        NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_CLAIM_ID],@"id", nil];
    
    NSString *url = [NSString stringWithFormat:@"getprofile"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.173/petlox_svn/mobile/getprofile

    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            claimDataDict=[responseDict objectForKey:@"User"];
            
            txtAddress.text=[claimDataDict objectForKey:@"address"];
            txtBusinessName.text=[claimDataDict objectForKey:@"business_name"];
            txtPhoneNumber.text=[claimDataDict objectForKey:@"phone"];
              txtEmailAddress.text=[claimDataDict objectForKey:@"email"];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              

                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self getClaimUserDetail];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          
                                      }];
}


-(void)verifyPhoneNumber
{
    NSMutableDictionary *param;
    if ([self isNotNull:[[NSUserDefaults standardUserDefaults] objectForKey:UD_CLAIM_ID]]) {
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:txtPhoneNumber.text,@"phone",[[NSUserDefaults standardUserDefaults] objectForKey:UD_CLAIM_ID],@"id", nil];
    }
    else
    {
    

        
        param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:txtPhoneNumber.text,@"phone",@"1",@"country_code",txtEmailAddress.text,@"email", nil];
    }
    
    NSString *url = [NSString stringWithFormat:@"checkphoneNumberTwillo"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.173/petlox_svn/mobile/checkphoneNumber
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {

               CallMeNowVC *cmvc=[[CallMeNowVC alloc]init];
                NSMutableDictionary *dataDict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:txtBusinessName.text,@"bName",txtAddress.text,@"bAddress",txtPhoneNumber.text,@"bPhnNumber",txtEmailAddress.text,@"bEmail", nil];
            [[NSUserDefaults standardUserDefaults]setObject:txtEmailAddress.text forKey:@"email"];
            [[NSUserDefaults standardUserDefaults]setObject:txtPhoneNumber.text forKey:@"phone"];
            [[NSUserDefaults standardUserDefaults]synchronize];

                cmvc.dictBusinessData=dataDict;
                cmvc.phnNumber=[dataDict objectForKey:@"bPhnNumber"];
                [self.navigationController pushViewController:cmvc animated:YES];
            }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self verifyPhoneNumber];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          
                                      }];
}
@end
