//
//  CCDetailVC.m
//  Petlox
//
//  Created by Sumit Sharma on 03/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "CCDetailVC.h"
#import "CCDetailCustomeCell.h"
#import "SelectCardTypeVC.h"
#import "UploadPhotoVC.h"
#import "AddCardVC.h"
@interface CCDetailVC ()<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet UIView *vwBackgroudn;
    IBOutlet UIView *bottomVw;
    
    IBOutlet UIButton *btnNext;
    NSMutableArray *creditCardArr;
    
    IBOutlet UITableView *tblVw;
    
    NSInteger selectIndexpath;
    
    IBOutlet NSLayoutConstraint *viewHeightContraint;
    
    NSString *cardID;
}
@end

@implementation CCDetailVC
#pragma mark - LifeCycle Method
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self addCreditCardDetailonServer];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your internet connection ."];
    }
  
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//setup view
-(void)setUpView
{
    wbServiceCount=1;
    [CommonFunctions setNavigationBar:self.navigationController];
    
    btnNext.layer.cornerRadius=btnNext.bounds.size.width/2*SCREEN_XScale;
    [btnNext.titleLabel setFont:[UIFont fontWithName:btnNext.titleLabel.font.fontName size:btnNext.titleLabel.font.pointSize*SCREEN_XScale]];
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
    [self.navigationItem setTitle:@"Payment"];
    vwBackgroudn.layer.cornerRadius=10.0f;
    vwBackgroudn.layer.borderWidth=1.0;
    vwBackgroudn.layer.borderColor=[UIColor colorWithRed:189.0f/255.0f green:189.0f/255.0f blue:189.0f/255.0f alpha:1.0].CGColor;
    vwBackgroudn.clipsToBounds=YES;
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureCliced)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];
    
}

-(void)tapGestureCliced
{
    [self.view endEditing:YES];
}

#pragma mark - IBAction Button
- (IBAction)addNewCardButtonClicked:(id)sender {
    SelectCardTypeVC *scvc=[[SelectCardTypeVC alloc]init];
    [self.navigationController pushViewController:scvc animated:YES];
}
- (IBAction)continueButtonClicked:(id)sender {
    
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self setActiveCardToServer];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Check your internet connection ."];
    }
}

-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)editButtonClicked:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    
    
    UIAlertController *alrtCntroller=[UIAlertController alertControllerWithTitle:@"Edit Payment Card" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    [alrtCntroller addAction:[UIAlertAction actionWithTitle:@"Edit" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        AddCardVC *acvc=[[AddCardVC alloc]init];
        acvc.cardDataDict=[creditCardArr objectAtIndex:indexNumber];
        [self.navigationController pushViewController:acvc animated:YES];
        
    
    }]];
    [alrtCntroller addAction:[UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if (selectIndexpath==indexNumber)
        {
            
        }
        else
        {
            [self deleteSelectedActiveCardFromServerForCardId:[[creditCardArr objectAtIndex:indexNumber] objectForKey:@"card_id"]];
            
            [creditCardArr removeObjectAtIndex:indexNumber];
            selectIndexpath=0;
            [tblVw reloadData];
            
            [tblVw sizeToFit];
            viewHeightContraint.constant=tblVw.frame.size.height;
        }
        
        
    }]];
    [alrtCntroller addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        
    }]];
    
        indexNumber=btn.tag;
    [self presentViewController:alrtCntroller animated:YES completion:nil];
    
}
-(IBAction)checkButtonClicked:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    
    // here customized for your self
    selectIndexpath=(NSInteger)btn.tag;
    [tblVw reloadData];
    cardID=[[creditCardArr objectAtIndex:selectIndexpath] objectForKey:@"card_id"];
    
}



#pragma mark - UIActionSheet Delegate Method


#pragma mark - UITableView Delegate Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return creditCardArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"CCDetailCustomeCell";
    CCDetailCustomeCell *cell=[[CCDetailCustomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    
    
        [cell.imgCardvw setImage:[UIImage imageNamed:@"CardIcon"]];
    
    
    if (indexPath.row==0) {
        [cell.checkButton setImage:[UIImage imageNamed:@"checkActive"] forState:UIControlStateNormal];
        cardID=[[creditCardArr objectAtIndex:indexPath.row] objectForKey:@"card_id"];

    }
    
    if (selectIndexpath==indexPath.row)
    {
        [cell.checkButton setImage:[UIImage imageNamed:@"checkActive"] forState:UIControlStateNormal];
        cardID=[[creditCardArr objectAtIndex:indexPath.row] objectForKey:@"card_id"];
    }
    else
    {
        [cell.checkButton setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
    }
    
    [cell.editButton addTarget:self action:@selector(editButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.checkButton addTarget:self action:@selector(checkButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    cell.editButton.tag=indexPath.row;
    cell.checkButton.tag=indexPath.row;
    NSString *cardStr=[[creditCardArr objectAtIndex:indexPath.row] objectForKey:@"cardNumber"];
    
    cardStr=[cardStr stringByReplacingOccurrencesOfString:@"X" withString:@""];
    
    cell.lblCardNumber.text=[NSString stringWithFormat:@"XXXX XXXX XXXX %@",cardStr];
    
    return cell;
}

#pragma mark- WebServices API..

-(void)addCreditCardDetailonServer
{
    
    NSString *url = [NSString stringWithFormat:@"saveCreditCardInfoBraintree"];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.173/petlox_svn/mobile/saveCreditCardInfo
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            creditCardArr=[responseDict objectForKey:@"data"];
            [tblVw reloadData];
            [tblVw sizeToFit];
            viewHeightContraint.constant=tblVw.frame.size.height;
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              

                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self addCreditCardDetailonServer];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          
                                      }];
}

-(void)setActiveCardToServer
{
    
    NSString *url = [NSString stringWithFormat:@"activeCreditCard"];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:cardID,@"card_id", nil];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.173/petlox_svn/mobile/activeCreditCard
    
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            
            UploadPhotoVC *upvc=[[UploadPhotoVC alloc]init];
            [self.navigationController pushViewController:upvc animated:YES];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              

                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self setActiveCardToServer];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          
                                      }];
}


-(void)deleteSelectedActiveCardFromServerForCardId:(NSString*)selectedCardId
{
    
    NSString *url = [NSString stringWithFormat:@"deleteCreditCardBraintree"];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:selectedCardId,@"card_id", nil];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];

    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.173/petlox_svn/mobile/activeCreditCard
   
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            //[self addCreditCardDetailonServer];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                          
                                      }];
}


@end
