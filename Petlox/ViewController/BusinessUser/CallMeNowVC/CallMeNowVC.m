//
//  CallMeNowVC.m
//  Petlox
//
//  Created by Sumit Sharma on 26/08/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "CallMeNowVC.h"
#import "ConfermationCodeVC.h"
@interface CallMeNowVC ()
{
    
    IBOutlet UILabel *lblPhnNumber;
    IBOutlet UILabel *lbl1;
    IBOutlet UILabel *lbl3;
    IBOutlet UIButton *btnCallMeNow;
}
@end

@implementation CallMeNowVC
@synthesize phnNumber,dictBusinessData;

#pragma mark - Life Cycle Methods
- (void)viewDidLoad {
    wbServiceCount=1;
    [super viewDidLoad];
    [self setUpView];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Setup View And its component
-(void)setUpView
{
    //SetupFor Navigation Bar...
    
    [CommonFunctions setNavigationBar:self.navigationController];

    [lblPhnNumber setFont:[UIFont fontWithName:lblPhnNumber.font.fontName size:lblPhnNumber.font.pointSize*SCREEN_XScale]];
    [lbl1 setFont:[UIFont fontWithName:lbl1.font.fontName size:lbl1.font.pointSize*SCREEN_XScale]];
    [lbl3 setFont:[UIFont fontWithName:lbl3.font.fontName size:lbl3.font.pointSize*SCREEN_XScale]];
    

    btnCallMeNow.titleLabel.numberOfLines=0;
    btnCallMeNow.titleLabel.textAlignment=NSTextAlignmentCenter;
    btnCallMeNow.layer.cornerRadius=btnCallMeNow.bounds.size.width/2*SCREEN_XScale;
    [btnCallMeNow.titleLabel setFont:[UIFont fontWithName:btnCallMeNow.titleLabel.font.fontName size:btnCallMeNow.titleLabel.font.pointSize*SCREEN_XScale]];
    
    
    NSString *completeString=[NSString stringWithFormat:@"Be prepared to receive a OTP\nfrom PETLOX at\n %@",phnNumber];
    NSMutableAttributedString *attriBute=[[NSMutableAttributedString alloc]initWithString:completeString];
    [attriBute addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:181.0f/255.0f green:37.0f/255.0f blue:86.0f/255.0f alpha:1] range:[completeString rangeOfString:@"PETLOX"]];
    [attriBute addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_OPENSANS_BOLD size:15.0f*SCREEN_XScale] range:[completeString rangeOfString:@"PETLOX"]];
    
    [attriBute addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:89.0f/255.0f green:89.0f/255.0f blue:89.0f/255.0f alpha:1] range:[completeString rangeOfString:phnNumber]];
    [attriBute addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_OPENSANS_BOLD size:15.0f*SCREEN_XScale] range:[completeString rangeOfString:phnNumber]];
    
    lblPhnNumber.attributedText=attriBute;
    
    
    
    NSString *strLbl3String=[NSString stringWithFormat:@"You will be given a 4 digit code \nwhich you will need to confirm"];
    NSMutableAttributedString *attribute3=[[NSMutableAttributedString alloc]initWithString:strLbl3String];
    [attribute3 addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:181.0f/255.0f green:37.0f/255.0f blue:86.0f/255.0f alpha:1] range:[strLbl3String rangeOfString:@"4 digit"]];
    lbl3.attributedText=attribute3;
    
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
}


#pragma mark - IBAction Button Methods
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)callMeNowBtnClicked:(id)sender {
    
    if([CommonFunctions reachabiltyCheck])
    {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self callMeNowAPI];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }

}


#pragma mark- WebServices API..

-(void)callMeNowAPI
{
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:phnNumber,@"phone",@"1",@"country_code",[[NSUserDefaults standardUserDefaults] valueForKey:@"email"],@"email", nil];
    NSString *url = [NSString stringWithFormat:@"callapitwillo"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.173/petlox_svn/mobile/callapi
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {

ConfermationCodeVC *ccvc=[[ConfermationCodeVC alloc]initWithNibName:@"ConfermationCodeVC" bundle:nil];
                ccvc.phnNumber=phnNumber;
                ccvc.transactionNumber=[responseDict objectForKey:@"transaction_number"];
            if (dictBusinessData.count >0) {
                ccvc.dictBusinessData=dictBusinessData;
            }
                [self.navigationController pushViewController:ccvc animated:YES];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self callMeNowAPI];
                                              }
                                              else
                                              {
                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}
@end
