//
//  ChooseBusinessCateVC.m
//  Petlox
//
//  Created by Sumit Sharma on 01/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "ChooseBusinessCateVC.h"
#import "ChooseHoursVC.h"
@interface ChooseBusinessCateVC ()
{
    IBOutlet UIButton *trainingBtn;
    IBOutlet UIButton *sittingBtn;
    IBOutlet UIButton *groomingBtn;
    
    IBOutlet UILabel *lblGrooming;
    IBOutlet UILabel *lblSitting;
    IBOutlet UILabel *lblTraining;
    IBOutlet UILabel *lblWalking;
    IBOutlet UIButton *btnNext;
    NSString *BusinessCategory;
    
    NSMutableArray *categoryArr;
    
    IBOutlet UIButton *walkingBtn;
}
@end

@implementation ChooseBusinessCateVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    BusinessCategory=@"";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//setup View
-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
      categoryArr=[[NSMutableArray alloc]init];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
    
    
    [lblSitting setFont:[UIFont fontWithName:lblSitting.font.fontName size:lblSitting.font.pointSize*SCREEN_XScale]];
    [lblGrooming setFont:[UIFont fontWithName:lblGrooming.font.fontName size:lblGrooming.font.pointSize*SCREEN_XScale]];
    [lblWalking setFont:[UIFont fontWithName:lblWalking.font.fontName size:lblWalking.font.pointSize*SCREEN_XScale]];
    [lblTraining setFont:[UIFont fontWithName:lblTraining.font.fontName size:lblTraining.font.pointSize*SCREEN_XScale]];
    btnNext.layer.cornerRadius=btnNext.bounds.size.width/2*SCREEN_XScale;
    [btnNext.titleLabel setFont:[UIFont fontWithName:btnNext.titleLabel.font.fontName size:btnNext.titleLabel.font.pointSize*SCREEN_XScale]];
    [btnNext setBackgroundColor:[UIColor colorWithRed:192.0f/255.0f green:192.0f/255.0f blue:192.0f/255.0f alpha:1]];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, -4, 100, 54)];
    label.font = [UIFont fontWithName:FONT_GULIM size:17.0f];
    
    label.textColor = [UIColor whiteColor];
    label.text = @"Choose Your \n Business Category";
    
    
    
    label.numberOfLines = 0;
    
    NSMutableParagraphStyle *style  = [[NSMutableParagraphStyle alloc] init];
    style.minimumLineHeight = 17.f;
    style.maximumLineHeight = 17.f;
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style,};
    label.attributedText = [[NSAttributedString alloc] initWithString:label.text
                     
                                                           attributes:attributtes];
    label.textAlignment = NSTextAlignmentCenter;
    [label sizeToFit];
    
    
    self.navigationItem.titleView = label;
    
    
}



#pragma mark - IBAction Button
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)groomingBtnClicked:(id)sender {
    if (groomingBtn.tag==0) {
        BusinessCategory=@"Grooming";
        [groomingBtn setImage:[UIImage imageNamed:@"groomingIconbgActive"] forState:UIControlStateNormal];


        BOOL isFoundValue=[categoryArr containsObject:BusinessCategory];
        
        if (isFoundValue) {
            [categoryArr removeObject:BusinessCategory];
        }
        else
        {
            [categoryArr addObject:BusinessCategory];
        }
        groomingBtn.tag=1;
        
    }
    else
    {
        groomingBtn.tag=0;
        BusinessCategory=@"Grooming";
        [groomingBtn setImage:[UIImage imageNamed:@"groomingIconbg"] forState:UIControlStateNormal];
        BOOL isFoundValue=[categoryArr containsObject:BusinessCategory];
        
        if (isFoundValue) {
            [categoryArr removeObject:BusinessCategory];
        }
    }
    if (categoryArr.count>0) {
        
        [btnNext setBackgroundColor:[UIColor colorWithRed:149.0f/255.0f green:41.0f/255.0f blue:87.0f/255.0f alpha:1]];
    }
    else
    {
        [btnNext setBackgroundColor:[UIColor colorWithRed:192.0f/255.0f green:192.0f/255.0f blue:192.0f/255.0f alpha:1]];
    }
}
- (IBAction)walkingBtnClicked:(id)sender {
    if (walkingBtn.tag==0) {
        BusinessCategory=@"Walking";
        [walkingBtn setImage:[UIImage imageNamed:@"walkingIconActive"] forState:UIControlStateNormal];
        walkingBtn.tag=1;
        BOOL isFoundValue=[categoryArr containsObject:BusinessCategory];
        
        if (isFoundValue) {
            [categoryArr removeObject:BusinessCategory];
        }
        else
        {
            [categoryArr addObject:BusinessCategory];
        }
    }
    else
    {
        BusinessCategory=@"Walking";
        [walkingBtn setImage:[UIImage imageNamed:@"walkingIcon"] forState:UIControlStateNormal];
        walkingBtn.tag=0;
        BOOL isFoundValue=[categoryArr containsObject:BusinessCategory];
        
        if (isFoundValue) {
            [categoryArr removeObject:BusinessCategory];
        }
        
    }
    if (categoryArr.count>0) {
        
        [btnNext setBackgroundColor:[UIColor colorWithRed:149.0f/255.0f green:41.0f/255.0f blue:87.0f/255.0f alpha:1]];
    }
    else
    {
        [btnNext setBackgroundColor:[UIColor colorWithRed:192.0f/255.0f green:192.0f/255.0f blue:192.0f/255.0f alpha:1]];
    }
    }
- (IBAction)sittingBtnClicked:(id)sender {
    
    if (sittingBtn.tag==0) {
        BusinessCategory=@"Sitting";
        [sittingBtn setImage:[UIImage imageNamed:@"sittingIconActive"] forState:UIControlStateNormal];
        sittingBtn.tag=1;
        BOOL isFoundValue=[categoryArr containsObject:BusinessCategory];
        
        if (isFoundValue) {
            [categoryArr removeObject:BusinessCategory];
        }
        else
        {
            [categoryArr addObject:BusinessCategory];
        }
    }
    else
    {
        BusinessCategory=@"Sitting";
        [sittingBtn setImage:[UIImage imageNamed:@"sittingIcon"] forState:UIControlStateNormal];
        sittingBtn.tag=0;
        BOOL isFoundValue=[categoryArr containsObject:BusinessCategory];
        
        if (isFoundValue) {
            [categoryArr removeObject:BusinessCategory];
        }
        
    }
    if (categoryArr.count>0) {
        
        [btnNext setBackgroundColor:[UIColor colorWithRed:149.0f/255.0f green:41.0f/255.0f blue:87.0f/255.0f alpha:1]];
    }
    else
    {
        [btnNext setBackgroundColor:[UIColor colorWithRed:192.0f/255.0f green:192.0f/255.0f blue:192.0f/255.0f alpha:1]];
    }
}
- (IBAction)trainingBtnClicked:(id)sender {
    
    if (trainingBtn.tag==0) {
        BusinessCategory=@"Training";
        [trainingBtn setImage:[UIImage imageNamed:@"trainingIconActive"] forState:UIControlStateNormal];
        trainingBtn.tag=1;
        BOOL isFoundValue=[categoryArr containsObject:BusinessCategory];
        
        if (isFoundValue) {
            [categoryArr removeObject:BusinessCategory];
        }
        else
        {
            [categoryArr addObject:BusinessCategory];
        }
    }
    else
    {
        BusinessCategory=@"Training";
        [trainingBtn setImage:[UIImage imageNamed:@"trainingIcon"] forState:UIControlStateNormal];
        trainingBtn.tag=0;
        BOOL isFoundValue=[categoryArr containsObject:BusinessCategory];
        
        if (isFoundValue) {
            [categoryArr removeObject:BusinessCategory];
        }
    }
    if (categoryArr.count>0) {
        
        [btnNext setBackgroundColor:[UIColor colorWithRed:149.0f/255.0f green:41.0f/255.0f blue:87.0f/255.0f alpha:1]];
    }
    else
    {
        [btnNext setBackgroundColor:[UIColor colorWithRed:192.0f/255.0f green:192.0f/255.0f blue:192.0f/255.0f alpha:1]];
    }
}
- (IBAction)nextButtonClicked:(id)sender {
   
    NSString *categoryStr;
    categoryStr=@"";

    if (categoryArr.count>0) {
        for (int i=0; i<categoryArr.count; i++) {
            if (categoryStr.length<1) {
                categoryStr=[categoryStr stringByAppendingString:[NSString stringWithFormat:@"%@",[categoryArr objectAtIndex:i]]];
            }
            else
            {
                categoryStr=[categoryStr stringByAppendingString:[NSString stringWithFormat:@",%@",[categoryArr objectAtIndex:i]]];
            }
            
        }
        ChooseHoursVC *chvc=[[ChooseHoursVC alloc]init];
        chvc.businessCategory=categoryStr;
        [self.navigationController pushViewController:chvc animated:YES];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please select atleast one category."];
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
