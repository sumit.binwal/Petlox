//
//  ChooseHoursCustomeCell.h
//  Petlox
//
//  Created by Sumit Sharma on 02/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseHoursCustomeCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgDayIcon;
@property (strong, nonatomic) IBOutlet UILabel *lblDayName;
@property (strong, nonatomic) IBOutlet UIButton *btnOpenClose;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@end
