//
//  ConfermationCodeVC.h
//  Petlox
//
//  Created by Sumit Sharma on 26/08/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfermationCodeVC : UIViewController
{
    
}
@property (nonatomic,strong)NSString *phnNumber;
@property (nonatomic,strong)NSString *transactionNumber;
@property(nonatomic,strong)NSMutableDictionary *dictBusinessData;
@end
