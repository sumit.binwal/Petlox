//
//  ConfermationCodeVC.m
//  Petlox
//
//  Created by Sumit Sharma on 26/08/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "ConfermationCodeVC.h"
#import "ThankYouVC.h"
#import "BusinessSignupSecndVC.h"
@interface ConfermationCodeVC ()<UITextFieldDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet UILabel *lblFirst;
    IBOutlet UILabel *lblScnd;
    IBOutlet UILabel *lblThrd;
    IBOutlet UIButton *btnCallMeAgain;
    
    IBOutlet NSLayoutConstraint *vSpace1;
    IBOutlet NSLayoutConstraint *vSpace;
    IBOutlet NSLayoutConstraint *varticleSpace;
    IBOutlet UITextField *txtVerficationCode;
}
@end
@implementation ConfermationCodeVC
@synthesize phnNumber,transactionNumber,dictBusinessData;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [lblFirst setFont:[UIFont fontWithName:lblFirst.font.fontName size:lblFirst.font.pointSize*SCREEN_XScale]];
    [lblScnd setFont:[UIFont fontWithName:lblScnd.font.fontName size:lblScnd.font.pointSize*SCREEN_XScale]];
    [lblThrd setFont:[UIFont fontWithName:lblThrd.font.fontName size:lblThrd.font.pointSize*SCREEN_XScale]];
    
    
    btnCallMeAgain.titleLabel.numberOfLines=0;
    btnCallMeAgain.titleLabel.textAlignment=NSTextAlignmentCenter;
    btnCallMeAgain.layer.cornerRadius=btnCallMeAgain.bounds.size.width/2*SCREEN_XScale;
    [btnCallMeAgain.titleLabel setFont:[UIFont fontWithName:btnCallMeAgain.titleLabel.font.fontName size:btnCallMeAgain.titleLabel.font.pointSize*SCREEN_XScale]];

    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureClicked)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];
    wbServiceCount=1;
    // Do any additional setup after loading the view from its nib.
}
-(void)tapGestureClicked
{
    [self.view endEditing:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    txtVerficationCode.text=@"";
    [txtVerficationCode becomeFirstResponder];
    
    
    [CommonFunctions setNavigationBar:self.navigationController];

    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
    
    if ([UIScreen mainScreen].bounds.size.height<568) {
        vSpace.constant=15.0f;
        vSpace1.constant=15.0f;
        varticleSpace.constant=15.0f;
    }
}
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)callMeAgainBtn:(id)sender
{
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self callMeNowAPI];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Check your internet connection."];
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{

    if (range.location>3) {
        return NO;
    }

        if([textField.text length] == 3 && ![string isEqualToString:@""]) // Verify that replacementString is a digit and not a backspace
        {
            textField.text = [textField.text stringByAppendingString:string];
            if ([CommonFunctions reachabiltyCheck]) {
                
                if (dictBusinessData.count>0) {
                    [CommonFunctions showActivityIndicatorWithText:@""];
                    [self verifyCodeAPI:textField.text];

                }
                else
                {
                    ThankYouVC *tvc=[[ThankYouVC alloc]init];
                    tvc.businessName=@"Grag's Grooming";
                    [self.navigationController pushViewController:tvc animated:YES];
                }
                
                                                            }
                            else
                            {
                                [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection ."];
                            }
            return NO;
        }
    return YES;
        
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark- WebServices API..

-(void)verifyCodeAPI:(NSString *)str
{
    
    
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:str,@"code",@"1",@"country_code",[[NSUserDefaults standardUserDefaults]objectForKey:@"phone"],@"phone",[[NSUserDefaults standardUserDefaults]objectForKey:@"email"],@"email", nil];
    NSString *url = [NSString stringWithFormat:@"codeScreentwillo"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.173/petlox_svn/mobile/codeScreen


    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            if (dictBusinessData.count>0) {
                BusinessSignupSecndVC *bsvc=[[BusinessSignupSecndVC alloc]init];
                bsvc.dataDict=dictBusinessData;
                [self.navigationController pushViewController:bsvc animated:YES];
            }
            else
            {
            ThankYouVC *tvc=[[ThankYouVC alloc]init];
            tvc.businessName=[responseDict objectForKey:@"business_name"];
            [self.navigationController pushViewController:tvc animated:YES];
            }

        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                          
                                      }];
}

-(void)callMeNowAPI
{
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:phnNumber,@"phone",@"1",@"country_code",[[NSUserDefaults standardUserDefaults] valueForKey:@"email"],@"email", nil];
    NSString *url = [NSString stringWithFormat:@"callapitwillo"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.173/petlox_svn/mobile/callapi
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            [self.view endEditing:YES];
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self callMeNowAPI];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                      }];
}


@end
