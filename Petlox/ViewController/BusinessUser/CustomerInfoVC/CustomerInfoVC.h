//
//  CustomerInfoVC.h
//  Petlox
//
//  Created by Sumit Sharma on 15/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerInfoVC : UIViewController
@property(nonatomic,strong)NSMutableDictionary *dictCustomerInfo;
@property(nonatomic,strong)NSString *strFromTap;
@end
