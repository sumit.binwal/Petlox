//
//  DeclineConfermationVC.m
//  Petlox
//
//  Created by Sumit Sharma on 02/11/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "DeclineConfermationVC.h"

@interface DeclineConfermationVC ()
{
    
    IBOutlet UILabel *lbl1;
    IBOutlet UILabel *lbl2;
    IBOutlet UIButton *btnBack;
}
@end

@implementation DeclineConfermationVC
@synthesize lblfromCancel;
- (void)viewDidLoad {
    [super viewDidLoad];
    [CommonFunctions setNavigationBar:self.navigationController];
    btnBack.layer.cornerRadius=btnBack.bounds.size.width/2*SCREEN_XScale;
    [btnBack.titleLabel setFont:[UIFont fontWithName:btnBack.titleLabel.font.fontName size:btnBack.titleLabel.font.pointSize*SCREEN_XScale]];

    [lbl1 setFont:[UIFont fontWithName:lbl1.font.fontName size:lbl1.font.pointSize*SCREEN_XScale]];
    [lbl2 setFont:[UIFont fontWithName:lbl2.font.fontName size:lbl2.font.pointSize*SCREEN_XScale]];
    
    if (lblfromCancel.length>0) {
        [self.navigationItem setTitle:@"Reservation Canceled"];
        lbl1.text=@"Reservation canceled !";
    }
    else
    {
        [self.navigationItem setTitle:@"Reservation Declined"];
        lbl1.text=@"Reservation declined !";
    }
    
self.navigationItem.hidesBackButton = YES;
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - IBAction Button Methods

- (IBAction)btnBcktoReservationClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
