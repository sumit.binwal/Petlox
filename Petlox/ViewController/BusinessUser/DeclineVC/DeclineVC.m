//
//  DeclineVC.m
//  Petlox
//
//  Created by Sumit Sharma on 31/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "DeclineVC.h"
#import "SettingsCell.h"
#import "DeclineConfermationVC.h"
@interface DeclineVC ()
{
    
    IBOutlet UITableView *tblVw;
    IBOutlet UIScrollView *scrllVw;
    IBOutlet UISwitch *switchOnOff;
    IBOutlet UITextView *txtAreaMsg;
    IBOutlet UIImageView *btnRightArrow;
    IBOutlet UILabel *lblTextAreaMsg;
    IBOutlet UIButton *btnDeclineReservation;
    NSString *strReasonForCancle;
    IBOutlet UIView *vwTxtAreaMsg;
    IBOutlet UIImageView *btnRightArrow1;
    NSMutableArray *arrData;
}
@end

@implementation DeclineVC
@synthesize reservationID,fromCancel;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    arrData =[[NSMutableArray alloc]initWithObjects:@"I am Booked",@"I am Closed", nil];
    wbServiceCount=1;
    // Do any additional setup after loading the view from its nib.
}

-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    if (fromCancel.length>0) {
          [self.navigationItem setTitle:@"Reason For Cancellation"];
        [btnDeclineReservation setTitle:@"Cancel\nReservation" forState:UIControlStateNormal];
        btnDeclineReservation.titleLabel.numberOfLines=0;
        btnDeclineReservation.titleLabel.textAlignment=NSTextAlignmentCenter;
        
    }
    else
    {
        [btnDeclineReservation setTitle:@"Decline\nReservation" forState:UIControlStateNormal];
        btnDeclineReservation.titleLabel.numberOfLines=0;
        btnDeclineReservation.titleLabel.textAlignment=NSTextAlignmentCenter;
    [self.navigationItem setTitle:@"Reason For Decline"];
    }
    
    btnDeclineReservation.layer.cornerRadius=btnDeclineReservation.bounds.size.width/2*SCREEN_XScale;
    [btnDeclineReservation.titleLabel setFont:[UIFont fontWithName:btnDeclineReservation.titleLabel.font.fontName size:btnDeclineReservation.titleLabel.font.pointSize*SCREEN_XScale]];
    [self.navigationController.navigationBar setHidden:NO];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];


    vwTxtAreaMsg.layer.borderColor=[UIColor colorWithRed:213.0f/255.0f green:213.0f/255.0f blue:213.0f/255.0f alpha:1].CGColor;
    lblTextAreaMsg.textColor=[UIColor colorWithRed:213.0f/255.0f green:213.0f/255.0f blue:213.0f/255.0f alpha:1];
    vwTxtAreaMsg.layer.borderWidth=1.0f;
    txtAreaMsg.userInteractionEnabled=NO;
    
    
//    vwTxtAreaMsg.layer.borderColor=[UIColor colorWithRed:103.0f/255.0f green:103.0f/255.0f blue:103.0f/255.0f alpha:1].CGColor;
//    vwTxtAreaMsg.layer.borderWidth=1.0f;
    
//    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTapClicked)];
//    tapGesture.delegate=self;
//    tapGesture.numberOfTapsRequired=1;
//    [self.view addGestureRecognizer:tapGesture];
    if ([UIScreen mainScreen].bounds.size.height<568) {
        [scrllVw setContentSize:CGSizeMake(320.0f, 568.0f)];
        [scrllVw setScrollEnabled:YES];
    }
    
}
-(void)viewDidLayoutSubviews
{
    NSLog(@"%f",self.view.frame.size.height);
    if ([UIScreen mainScreen].bounds.size.height<568) {
        [scrllVw setContentSize:CGSizeMake(320.0f, 568.0f)];
        [scrllVw setScrollEnabled:YES];
    }
}
-(void)singleTapClicked
{
    [scrllVw setContentOffset:CGPointZero];
    [txtAreaMsg resignFirstResponder];
    [scrllVw setScrollEnabled:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnClickedReason:(id)sender {
    UIButton *btn=(UIButton *)sender;
    switch (btn.tag) {
        case 0:
        {
            [btnRightArrow setHidden:NO];
            [btnRightArrow1 setHidden:YES];
            strReasonForCancle=@"I am Booked";
            break;
        }
        case 1:
        {
            [btnRightArrow setHidden:YES];
            [btnRightArrow1 setHidden:NO];
            strReasonForCancle=@"I am closed";
            break;
        }
    }
}

#pragma mark-Scroll View Method
-(void)scrollViewToCenterOfScreen:(UITextView *)textField
{
    [scrllVw setScrollEnabled:YES];
    float difference;
    if (scrllVw.contentSize.height == 600)
        difference = 50.0f;
    else
        difference = 50.0f;
    CGFloat viewCenterY = textField.center.y+150;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat avaliableHeight = applicationFrame.size.height - 10.0f;
    CGFloat y = viewCenterY - avaliableHeight / 10.0f;
    
    NSLog(@"%f",y);
    if (y < 0)
        y = 0;
    
    [scrllVw setContentOffset:CGPointMake(0, y) animated:YES];
    [scrllVw setContentSize:CGSizeMake(320.0f, 670.0f)];
}


-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - UITextView Delegate Methods
-(void)textViewDidChange:(UITextView *)textView
{
    if (textView.text.length>0) {
        [lblTextAreaMsg setHidden:YES];
    }
    else
    {
        [lblTextAreaMsg setHidden:NO];
    }
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self scrollViewToCenterOfScreen:textView];
}
- (IBAction)switchButtonClicked:(id)sender {
    
    if ([switchOnOff isOn]) {

        vwTxtAreaMsg.layer.borderColor=[UIColor colorWithRed:103.0f/255.0f green:103.0f/255.0f blue:103.0f/255.0f alpha:1].CGColor;
       lblTextAreaMsg.textColor=[UIColor colorWithRed:98.0f/255.0f green:98.0f/255.0f blue:98.0f/255.0f alpha:1];
        vwTxtAreaMsg.layer.borderWidth=1.0f;
        txtAreaMsg.userInteractionEnabled=YES;
        
    }
    else
    {
    vwTxtAreaMsg.layer.borderColor=[UIColor colorWithRed:213.0f/255.0f green:213.0f/255.0f blue:213.0f/255.0f alpha:1].CGColor;
        lblTextAreaMsg.textColor=[UIColor colorWithRed:213.0f/255.0f green:213.0f/255.0f blue:213.0f/255.0f alpha:1];
        vwTxtAreaMsg.layer.borderWidth=1.0f;
        txtAreaMsg.userInteractionEnabled=NO;
        txtAreaMsg.text=@"";
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (textView==txtAreaMsg) {
   //     lblMsgCount.text=[NSString stringWithFormat:@"%u Characters",200-range.location];

        if (range.location>=200) {
            //lblMsgCount.text=@"0 Character";
            return NO;
        }
        else
        {
            if ([text isEqualToString:@"\n"]) {
                [self singleTapClicked];
                return YES;
            }
            return YES;
        }
    }
    
    else
    {
        return YES;
    }
    return YES;
}
#pragma mark - UITableView Delegate Method
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrData.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f*SCREEN_XScale;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *settingsCell   = @"SettingsCell";
    
    SettingsCell *cell = (SettingsCell *)[tableView dequeueReusableCellWithIdentifier:settingsCell];
    
    if (cell == nil)
    {
        cell = (SettingsCell*)[[[NSBundle mainBundle] loadNibNamed:@"SettingsCell" owner:self options:nil] objectAtIndex:0];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    
    cell.lblTxt.text = arrData[indexPath.row];
    
    if ([strReasonForCancle isEqualToString:[arrData objectAtIndex:indexPath.row]]) {
        
        [cell.imgViewArrow setHidden:NO];
    }
    else
    {
        [cell.imgViewArrow setHidden:YES];
    }
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
        {
            
            strReasonForCancle=@"I am Booked";
            break;
        }
        case 1:
        {
            
            strReasonForCancle=@"I am Closed";
            break;
        }
            
    }
    [tblVw reloadData];
}




- (IBAction)declineBtnClicked:(id)sender {
    [self.view endEditing:YES];
    if (strReasonForCancle.length>1 || switchOnOff.isOn) {
        if ((switchOnOff.isOn && [CommonFunctions isValueNotEmpty:txtAreaMsg.text]) ||!switchOnOff.isOn) {
            if ([CommonFunctions reachabiltyCheck]) {
                [CommonFunctions showActivityIndicatorWithText:@""];
                [self declineReaservation];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
            }
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please enter message for the customer."];
        }
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please select reason for decline reservation."];
    }
}


#pragma mark- WebServices API..

-(void)declineReaservation
{
    NSMutableDictionary *param;
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];

    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:reservationID,@"id",strReasonForCancle,@"reason_for_cancel",txtAreaMsg.text,@"massage",@"business",@"type", nil];
    
    NSString *url = [NSString stringWithFormat:@"cancelReservation"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/cancelReservation
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            DeclineConfermationVC *dcvc=[[DeclineConfermationVC alloc]init];
            dcvc.lblfromCancel=fromCancel;
            [self.navigationController pushViewController:dcvc animated:YES];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                 wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self declineReaservation];
                                              }
                                              else
                                              {
                                                    [CommonFunctions removeActivityIndicator];
                                                    [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
