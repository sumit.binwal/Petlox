//
//  ChooseHoursVC.m
//  Petlox
//
//  Created by Sumit Sharma on 01/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "EditBusinessHoursVC.h"
#import "AddPaymentVC.h"
#import "ChooseHoursCustomeCell.h"
@interface EditBusinessHoursVC ()<UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    
    UIBarButtonItem *doneButton;
    IBOutlet NSLayoutConstraint *heightConstraint;
    IBOutlet UIButton *btnNext;
    IBOutlet UIView *VwNextBtn;
    IBOutlet UIDatePicker *datePickerToTiming;
    IBOutlet UIDatePicker *datePickerFromTime;
    IBOutlet UIView *timePickerVw;
    UITextField *txtLbl;
    IBOutlet UIPickerView *pickerVw;
    IBOutlet UITableView *tblVw;
    BOOL isTimeEditable;
    NSMutableArray *dataHoursArr;
        NSMutableArray *dataHoursArr1;
    NSInteger btnTagIndex;
    IBOutlet UIButton *changeOpenCloseBtnStatus;
    IBOutlet UIView *viewForPicker;
    NSMutableArray *dataArr;
    IBOutlet UIPickerView *pickerVwOpenClose;
    NSString *OperationHrsStatus;
    NSString *startTime;
    NSString *endTime;
    UIBarButtonItem *rgtBtn;
    //Bool for back button redirection if true then back button go to working hours screen else business category screen.
    BOOL forBackButtonRedirection;

}
@end

@implementation EditBusinessHoursVC
@synthesize businessCategory;
- (void)viewDidLoad {
    [super viewDidLoad];
    wbServiceCount=1;
    [self setUpView];
    // Do any additional setup after loading the view from its nib.
}
-(void)setUpView
{
    
    if ([UIScreen mainScreen].bounds.size.height<568) {
        heightConstraint.constant=0;
    }
    
    NSLog(@"%@",businessCategory);
    [CommonFunctions setNavigationBar:self.navigationController];

    dataArr=[[NSMutableArray alloc]initWithObjects:@"Open",@"Close", nil];
    txtLbl.inputView = viewForPicker;
    
    btnNext.layer.cornerRadius=btnNext.bounds.size.width/2*SCREEN_XScale;
    [btnNext.titleLabel setFont:[UIFont fontWithName:btnNext.titleLabel.font.fontName size:btnNext.titleLabel.font.pointSize*SCREEN_XScale]];
    

    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStylePlain target:self.viewDeckController action:@selector(toggleLeftViewAnimated:)];
    [self.navigationItem setLeftBarButtonItem:btn];
    rgtBtn=[[UIBarButtonItem alloc]initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(editBtnClicked:)];
    [self.navigationItem setRightBarButtonItem:rgtBtn];
    
    [tblVw setScrollEnabled:NO];
    
    [self.navigationItem setTitle:@"Hours of Operation"];
    
    
    dataHoursArr1=[[NSMutableArray alloc]init];
    if (dataHoursArr1.count<1) {
        for (int day=0; day<7; day++) {
            NSMutableDictionary *hoursDict=[[NSMutableDictionary alloc]init];
            switch (day) {
                case 0:
                    [hoursDict setObject:@"Monday" forKey:@"day"];
                    [hoursDict setObject:@"MonIcon" forKey:@"imgName"];
                    [hoursDict setObject:@"1" forKey:@"mode"];
                    break;
                case 1:
                    [hoursDict setObject:@"Tuesday" forKey:@"day"];
                    [hoursDict setObject:@"TueIcon" forKey:@"imgName"];
                    [hoursDict setObject:@"1" forKey:@"mode"];
                    break;
                case 2:
                    [hoursDict setObject:@"Wednesday" forKey:@"day"];
                    [hoursDict setObject:@"WedIcon" forKey:@"imgName"];
                    [hoursDict setObject:@"1" forKey:@"mode"];
                    break;
                case 3:
                    [hoursDict setObject:@"Thursday" forKey:@"day"];
                    [hoursDict setObject:@"ThuIcon" forKey:@"imgName"];
                    [hoursDict setObject:@"1" forKey:@"mode"];
                    break;
                case 4:
                    [hoursDict setObject:@"Friday" forKey:@"day"];
                    [hoursDict setObject:@"FriIcon" forKey:@"imgName"];
                    [hoursDict setObject:@"1" forKey:@"mode"];
                    break;
                case 5:
                    [hoursDict setObject:@"Saturday" forKey:@"day"];
                    [hoursDict setObject:@"SatIcon" forKey:@"imgName"];
                    [hoursDict setObject:@"1" forKey:@"mode"];
                    break;
                case 6:
                    [hoursDict setObject:@"Sunday" forKey:@"day"];
                    [hoursDict setObject:@"SunIcon" forKey:@"imgName"];
                    [hoursDict setObject:@"1" forKey:@"mode"];
                    break;
            }
            [hoursDict setObject:@"11:00am" forKey:@"start"];
            [hoursDict setObject:@"10:30pm" forKey:@"end"];

            [dataHoursArr1 addObject:hoursDict];
        }
        
    }
    [CommonFunctions showActivityIndicatorWithText:@""];
    [self getOperationHours];
    

    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction Methds
- (IBAction)toolbarCancelBtnClicked:(id)sender {
    [self.view endEditing:YES];
    [timePickerVw removeFromSuperview];
    [self.navigationController.navigationBar setUserInteractionEnabled:YES];
}
- (IBAction)toolbarDoneBtnClicked:(id)sender {
    [self.view endEditing:YES];
    UIBarButtonItem *btn=(UIBarButtonItem *)sender;
    NSLog(@"%ld",(long)btn.tag);
    if (btn.tag==1) {
        
        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        [outputFormatter setDateFormat:@"h:mma"];
        NSString *strtime,*strtime1;
        strtime=[[outputFormatter stringFromDate:datePickerFromTime.date] lowercaseString];
        strtime1=[[outputFormatter stringFromDate:datePickerToTiming.date] lowercaseString];
        
        double start24tim,end24time;
        start24tim=[[self convert12to24time:strtime] doubleValue];
        end24time=[[self convert12to24time:strtime1] doubleValue];

        if (start24tim>end24time) {
            [CommonFunctions alertTitle:@"" withMessage:@"Start time can not be greater than end time."];
        }
        else if (start24tim==end24time)
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Start and end time not be same."];
        }
        else
        {
            startTime=[[outputFormatter stringFromDate:datePickerFromTime.date] lowercaseString];
            endTime=[[outputFormatter stringFromDate:datePickerToTiming.date] lowercaseString];
            [timePickerVw removeFromSuperview];
            [self.navigationController.navigationBar setUserInteractionEnabled:YES];
        }
        
    }
    
    else
    {
    if ([OperationHrsStatus isEqualToString:@"1"]) {

        [timePickerVw setFrame:CGRectMake(0.0f, self.view.frame.size.height, self.view.frame.size.width, 205.0f)];
        [timePickerVw setFrame:CGRectMake(0.0f, [UIScreen mainScreen].bounds.size.height-timePickerVw.bounds.size.height-50, [UIScreen mainScreen].bounds.size.width, timePickerVw.bounds.size.height+50)];
        
        [self.view addSubview:timePickerVw];
       [self.view endEditing:YES];
[self.navigationController.navigationBar setUserInteractionEnabled:NO];
        
    }
    else if([OperationHrsStatus isEqualToString:@"0"])
    {
        [self.view endEditing:YES];

        startTime=@"0";
        endTime=@"0";
        [tblVw reloadData];
        
    }
        else
        {
            OperationHrsStatus=@"1";
            [changeOpenCloseBtnStatus setImage:[UIImage imageNamed:@"OpenBtn"] forState:UIControlStateNormal];
            [timePickerVw setFrame:CGRectMake(0.0f, self.view.frame.size.height, self.view.frame.size.width, 255.0f)];
            [timePickerVw setFrame:CGRectMake(0.0f, [UIScreen mainScreen].bounds.size.height-timePickerVw.bounds.size.height-50, [UIScreen mainScreen].bounds.size.width, timePickerVw.bounds.size.height+50)];
            [self.view addSubview:timePickerVw];
            [self.view endEditing:YES];
            [self.navigationController.navigationBar setUserInteractionEnabled:NO];
        }
    }
}
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    if (rgtBtn.tag==0 && forBackButtonRedirection) {
        [tblVw setHidden:NO];
        [tblVw reloadData];
        [changeOpenCloseBtnStatus setHidden:YES];
        [btnNext setHidden:NO];
        forBackButtonRedirection=false;

        doneButton.tag=0;

    }
    else
    {
    [self.navigationController popViewControllerAnimated:YES];
    }
}
- (IBAction)nextButtonClicked:(id)sender {
    
    if (isTimeEditable) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please save edited information."];
    }
    else
    {
        if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self updateOperationHours];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Check your internet connection ."];
        }

    }
}
- (IBAction)changeOpenCloseBtnStatus:(id)sender {
//    [viewForPicker setFrame:CGRectMake(0.0f, [UIScreen mainScreen].bounds.size.height-viewForPicker.frame.size.height, self.view.frame.size.width, 200.0f)];
//    [self.view addSubview:viewForPicker];

    
    [txtLbl becomeFirstResponder];
}
-(IBAction)doneBtnClicked:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    NSLog(@"%ld",(long)btn.tag);
    NSLog(@"%@",startTime);
        NSLog(@"%@",endTime);
    
    if (![self isNotNull:startTime]||![self isNotNull:endTime]) {
        startTime=@"11:00am";
        endTime=@"10:30pm";
        OperationHrsStatus=@"1";
       
    }
    
    if (doneButton.tag==1) {
        if ([self isNotNull:startTime] ||[self isNotNull:endTime] || [OperationHrsStatus isEqualToString:@"0"]) {

                NSMutableDictionary *hoursDict=[[NSMutableDictionary alloc]init];
                switch (btnTagIndex) {
                    case 0:
                        [hoursDict setObject:@"Monday" forKey:@"day"];
                        [hoursDict setObject:@"MonIcon" forKey:@"imgName"];
                        break;
                    case 1:
                        [hoursDict setObject:@"Tuesday" forKey:@"day"];
                        [hoursDict setObject:@"TueIcon" forKey:@"imgName"];
                        break;
                    case 2:
                        [hoursDict setObject:@"Wednesday" forKey:@"day"];
                        [hoursDict setObject:@"WedIcon" forKey:@"imgName"];
                        break;
                    case 3:
                        [hoursDict setObject:@"Thursday" forKey:@"day"];
                        [hoursDict setObject:@"ThuIcon" forKey:@"imgName"];
                        break;
                    case 4:
                        [hoursDict setObject:@"Friday" forKey:@"day"];
                        [hoursDict setObject:@"FriIcon" forKey:@"imgName"];
                        break;
                    case 5:
                        [hoursDict setObject:@"Saturday" forKey:@"day"];
                        [hoursDict setObject:@"SatIcon" forKey:@"imgName"];
                        break;
                    case 6:
                        [hoursDict setObject:@"Sunday" forKey:@"day"];
                        [hoursDict setObject:@"SunIcon" forKey:@"imgName"];
                        
                        break;
                }
                [hoursDict setObject:OperationHrsStatus forKey:@"openStatus"];
                if ([OperationHrsStatus isEqualToString:@"0"]) {
                    [hoursDict setObject:@"0" forKey:@"start_time"];
                    [hoursDict setObject:@"0" forKey:@"end_time"];
                    
                }
                else
                {
                    if ([startTime isEqualToString:@"0"]||[endTime isEqualToString:@"0"]) {
                        [hoursDict setObject:@"11:00am" forKey:@"start_time"];
                        [hoursDict setObject:@"10:30pm" forKey:@"end_time"];
                    }
                    
                    else
                    {
                    [hoursDict setObject:startTime forKey:@"start_time"];
                    [hoursDict setObject:endTime forKey:@"end_time"];
                    }
                    
                }
                [dataHoursArr replaceObjectAtIndex:btnTagIndex withObject:hoursDict];
                [self.view endEditing:YES];
                [tblVw setHidden:NO];
                [tblVw reloadData];
                [changeOpenCloseBtnStatus setHidden:YES];
                [btnNext setHidden:NO];
                doneButton.tag=0;
                forBackButtonRedirection=false;
    }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please select working time ."];
        }
    }
    else if (doneButton.tag==0)
    {
                UIBarButtonItem *uibarBtn=[[UIBarButtonItem alloc]initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(editBtnClicked:)];
                [self.navigationItem setRightBarButtonItem:uibarBtn];
        isTimeEditable=false;

    }
}
-(IBAction)editBtnClicked:(id)sender
{
    isTimeEditable=true;
    doneButton=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneBtnClicked:)];
    [self.navigationItem setRightBarButtonItem:doneButton];
    doneButton.tag=0;
    [tblVw reloadData];
}
-(IBAction)openCloseBtnClicked:(id)sender
{
    if (isTimeEditable) {
        forBackButtonRedirection=true;
        UIButton *btn=(UIButton *)sender;
        btnTagIndex=btn.tag;
        [tblVw setHidden:YES];
        [btnNext setHidden:YES];
        [changeOpenCloseBtnStatus setHidden:NO];
        doneButton.tag=1;
        OperationHrsStatus=@"-1";
        [changeOpenCloseBtnStatus setImage:[UIImage imageNamed:@"OpenCloseBtn"] forState:UIControlStateNormal];
        
        [pickerVwOpenClose selectRow:0 inComponent:0 animated:YES];
    }
    
}


#pragma mark - Custome Method
-(NSString *)convert12to24time:(NSString *)string1
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"h:mma";
    NSDate *date = [dateFormatter dateFromString:string1];
    
    dateFormatter.dateFormat = @"HH.mm";

    return [dateFormatter stringFromDate:date];
}
#pragma mark - UITableView Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 52.0f*SCREEN_XScale;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataHoursArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"ChooseHoursCustomeCell";
    ChooseHoursCustomeCell *cell=[[ChooseHoursCustomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    if (isTimeEditable) {
        cell.btnOpenClose.userInteractionEnabled=YES;
    }
    else
    {
        cell.btnOpenClose.userInteractionEnabled=NO;
    }
    [cell.btnOpenClose addTarget:self action:@selector(openCloseBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnOpenClose.tag=indexPath.row;
    if ([[[dataHoursArr objectAtIndex:indexPath.row]objectForKey:@"openStatus"]isEqualToString:@"0"]) {
    [cell.btnOpenClose setBackgroundImage:[UIImage imageNamed:@"CloseImg"] forState:UIControlStateNormal];
    //[cell.btnOpenClose setTitleColor:[UIColor colorWithRed:92.0f/255.0f green:92.0f/255.0f blue:92.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [cell.btnOpenClose setTitle:@"Close" forState:UIControlStateNormal];
    [cell.lblTime setText:@""];
    }
    else
    {
    [cell.btnOpenClose setBackgroundImage:[UIImage imageNamed:@"OpenImg"] forState:UIControlStateNormal];
    //[cell.btnOpenClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cell.lblTime setText:[NSString stringWithFormat:@"%@-%@",[[dataHoursArr objectAtIndex:indexPath.row]  objectForKey:@"start_time"],[[dataHoursArr objectAtIndex:indexPath.row] objectForKey:@"end_time"]]];
    }
    [cell.imgDayIcon setImage:[UIImage imageNamed:[[dataHoursArr1 objectAtIndex:indexPath.row]objectForKey:@"imgName"]]];
    [cell.lblDayName setText:[[dataHoursArr objectAtIndex:indexPath.row] objectForKey:@"day"]];
    cell.backgroundColor=[UIColor clearColor];
    return cell;
    
}

#pragma mark - UIPickerView Delegate Methods
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return dataArr.count;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [dataArr objectAtIndex:row];
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSString *str=[dataArr objectAtIndex:row];
    if ([str isEqualToString:@"Open"]) {
        OperationHrsStatus=@"1";
        [changeOpenCloseBtnStatus setImage:[UIImage imageNamed:@"OpenBtn"] forState:UIControlStateNormal];
    }
    else
    {
        OperationHrsStatus=@"0";
        [changeOpenCloseBtnStatus setImage:[UIImage imageNamed:@"CloseBtn"] forState:UIControlStateNormal];
    }
}

#pragma mark- WebServices API..

-(void)updateOperationHours
{
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:dataHoursArr,@"userinfo",[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"sid",businessCategory,@"business_category", nil];
    NSString *url = [NSString stringWithFormat:@"updateUserInfoNew"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.173/petlox_svn/mobile/updateUserInfo
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self updateOperationHours];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                      }];
}

-(void)getOperationHours
{
    NSMutableDictionary *param;
    
    
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"user_id",@"business",@"type", nil];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    NSString *url = [NSString stringWithFormat:@"operationHours"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.173/petlox_svn/mobile/operationHours
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            dataHoursArr=[responseDict objectForKey:@"data"];
            businessCategory=[[[responseDict objectForKey:@"data"] objectAtIndex:0]objectForKey:@"business_category"];
            [tblVw reloadData];
            
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);

                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self getOperationHours];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                      }];
}


@end
