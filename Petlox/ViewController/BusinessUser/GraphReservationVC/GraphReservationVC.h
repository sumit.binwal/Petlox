//
//  GraphReservationVC.h
//  Petlox
//
//  Created by Sumit Sharma on 09/11/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCLineChartView.h"
@interface GraphReservationVC : UIViewController
@property(nonatomic,strong)PCLineChartView *lineChartView;
@end
