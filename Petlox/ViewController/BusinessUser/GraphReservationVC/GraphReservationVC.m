//
//  GraphReservationVC.m
//  Petlox
//
//  Created by Sumit Sharma on 09/11/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "GraphReservationVC.h"
#import "CustomerInfoVC.h"
#import "ReservationCustomeCell.h"
@interface GraphReservationVC ()<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UIImageView *graphBottomLine;
    IBOutlet UIScrollView *scrllVw;
    IBOutlet UIScrollView *scrllVwGraph;
    IBOutlet UIView *cntainerVw;
    IBOutlet UIButton *btnMnth;
    IBOutlet UIButton *btnWeek;
    IBOutlet UIImageView *graphTrallingLine;
    IBOutlet UIButton *btnDay;
    IBOutlet UILabel *lblViews;
    IBOutlet UILabel *lblReservation;
    IBOutlet UIView *objectView;
    IBOutlet UILabel *lblReservationTitle;
    NSArray *graphDataArr;
    NSMutableArray *filteredArray;
    IBOutlet UITableView *tblVwProgress;
    NSString *selectedBtnTag;
    IBOutlet UILabel *lblErrorMsg;
    NSMutableArray *arrProgressReservations;
}
@end

@implementation GraphReservationVC
@synthesize lineChartView;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    wbServiceCount=1;
    [CommonFunctions showActivityIndicatorWithText:@""];
    [self getLineGraphData];
    


    // Do any additional setup after loading the view from its nib.
}

-(void)setUpView
{



    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStylePlain target:self.viewDeckController action:@selector(toggleLeftViewAnimated:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];

    [self.navigationItem setTitle:@"Reservations"];
    
//    [btnDay setBackgroundColor:[UIColor colorWithRed:110.0f/255.0f green:200.0f/255.0f blue:253.0f/255.0f alpha:1]];
//    [btnDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    
//    [btnMnth setBackgroundColor:[UIColor colorWithRed:152.0f/255.0f green:217.0f/255.0f blue:255.0f/255.0f alpha:1]];
//    [btnMnth setTitleColor:[UIColor colorWithRed:0.0f/255.0f green:172.0f/255.0f blue:238.0f/255.0f alpha:1] forState:UIControlStateNormal];
//    [btnWeek setBackgroundColor:[UIColor colorWithRed:152.0f/255.0f green:217.0f/255.0f blue:255.0f/255.0f alpha:1]];
//    [btnWeek setTitleColor:[UIColor colorWithRed:0.0f/255.0f green:172.0f/255.0f blue:238.0f/255.0f alpha:1] forState:UIControlStateNormal];
    [btnDay setImage:[UIImage imageNamed:@"G_dayFocus"] forState:UIControlStateNormal];
    [btnWeek setImage:[UIImage imageNamed:@"G_week"] forState:UIControlStateNormal];
    [btnMnth setImage:[UIImage imageNamed:@"G_mnth"] forState:UIControlStateNormal];
    selectedBtnTag=@"0";
    
    if ([UIScreen mainScreen] .bounds.size.height<568) {
        [scrllVw setContentSize:CGSizeMake(320.0f, 568.0f)];
        [scrllVw setScrollEnabled:YES];
    }
    
}
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewDidLayoutSubviews
{
    if ([UIScreen mainScreen].bounds.size.height<568) {
        [scrllVw setContentSize:CGSizeMake(320.0f, 568.0f)];
        [scrllVw setScrollEnabled:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrProgressReservations.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 63.0f*SCREEN_XScale;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"ReservationCustomeCell";
    ReservationCustomeCell *cell=[[ReservationCustomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    cell.selectionStyle=UITableViewCellStyleDefault;
    
    [cell.imgTopDevider setHidden:YES];
    cell.imgVwProfileImg.clipsToBounds=YES;
    
    cell.lblReservedTime.text=[[arrProgressReservations objectAtIndex:indexPath.row] objectForKey:@"reservation_date"];
    cell.lblUserName.text=[NSString stringWithFormat:@"%@ %@",[[arrProgressReservations objectAtIndex:indexPath.row] objectForKey:@"first_name"],[[arrProgressReservations objectAtIndex:indexPath.row] objectForKey:@"last_name"]];
    [cell.imgVwProfileImg setImageWithURL:[NSURL URLWithString:[[arrProgressReservations objectAtIndex:indexPath.row] objectForKey:@"image"]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomerInfoVC *civc=[[CustomerInfoVC alloc]initWithNibName:@"CustomerInfoVC" bundle:nil];
    civc.dictCustomerInfo=[arrProgressReservations objectAtIndex:indexPath.row];
    civc.strFromTap=@"1";
    [self.navigationController pushViewController:civc animated:YES];

}


#pragma mark - IBAction Button Methods
- (IBAction)segmentButtonClicked:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    int btnTag=btn.tag;
    [btnDay setImage:[UIImage imageNamed:@"G_day"] forState:UIControlStateNormal];
    [btnWeek setImage:[UIImage imageNamed:@"G_week"] forState:UIControlStateNormal];
    [btnMnth setImage:[UIImage imageNamed:@"G_mnth"] forState:UIControlStateNormal];

//    [btnDay setBackgroundColor:[UIColor colorWithRed:152.0f/255.0f green:217.0f/255.0f blue:255.0f/255.0f alpha:1]];
//    [btnDay setTitleColor:[UIColor colorWithRed:0.0f/255.0f green:172.0f/255.0f blue:238.0f/255.0f alpha:1] forState:UIControlStateNormal];
//    [btnMnth setBackgroundColor:[UIColor colorWithRed:152.0f/255.0f green:217.0f/255.0f blue:255.0f/255.0f alpha:1]];
//    [btnMnth setTitleColor:[UIColor colorWithRed:0.0f/255.0f green:172.0f/255.0f blue:238.0f/255.0f alpha:1] forState:UIControlStateNormal];
//    [btnWeek setBackgroundColor:[UIColor colorWithRed:152.0f/255.0f green:217.0f/255.0f blue:255.0f/255.0f alpha:1]];
//    [btnWeek setTitleColor:[UIColor colorWithRed:0.0f/255.0f green:172.0f/255.0f blue:238.0f/255.0f alpha:1] forState:UIControlStateNormal];
    CGRect thisRect = [UIScreen mainScreen].bounds;
    
    //modify required frame parameter (.origin.x/y, .size.width/height)
    thisRect.origin.y = 33;
    switch (btnTag) {
        case 0:
        {
            [btn setImage:[UIImage imageNamed:@"G_dayFocus"] forState:UIControlStateNormal];
            selectedBtnTag=@"0";
            lblReservationTitle.text=@"Reservations in Progress";
            if ([CommonFunctions reachabiltyCheck]) {
                [CommonFunctions showActivityIndicatorWithText:@""];
                [self getLineGraphData];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
            }
            break;
        }
        case 1:
        {
            
            [btn setImage:[UIImage imageNamed:@"G_weekFocus"] forState:UIControlStateNormal];
                        selectedBtnTag=@"1";
                        lblReservationTitle.text=@"Reservations This Week";
            if ([CommonFunctions reachabiltyCheck]) {
                [CommonFunctions showActivityIndicatorWithText:@""];
                [self getLineGraphData];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
            }
            break;
        }
        case 2:
        {
            
            [btn setImage:[UIImage imageNamed:@"G_mnthFocus"] forState:UIControlStateNormal];
                        selectedBtnTag=@"2";
                                    lblReservationTitle.text=@"Reservations This Month";
            if ([CommonFunctions reachabiltyCheck]) {
                [CommonFunctions showActivityIndicatorWithText:@""];
                [self getLineGraphData];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
            }
            break;
        }
        default:
            break;
    }
}



#pragma mark- WebService Calling Methods
-(void)getLineGraphData
{
    NSString *url;
    if ([selectedBtnTag isEqualToString:@"0"]) {
        url = [NSString stringWithFormat:@"dailyReservationlist"];
    }
    else if ([selectedBtnTag isEqualToString:@"1"])
    {
        url = [NSString stringWithFormat:@"weeklyReservationlist"];
    }
    else
    {
        url = [NSString stringWithFormat:@"monthlyReservationlist"];
    }

    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"user_id",@"0",@"reservation_type", nil];
    
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://mymeetingdesk.com/mobile/petlox/mobile/dailyReservationlist
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            
            [self performSelectorOnMainThread:@selector(fetchGraphData:) withObject:responseDict waitUntilDone:YES];
            
        }
        else if ([operation.response statusCode]==206)
        {
            [self performSelectorOnMainThread:@selector(fetchGraphData:) withObject:responseDict waitUntilDone:YES];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{

                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self getLineGraphData];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                      }];
}
-(void)fetchGraphData:(NSDictionary *)dict
{
    [graphTrallingLine setHidden:YES];
    [graphBottomLine setHighlighted:YES];
   // lblReservation.text=[dict objectForKey:@"total_reservation"];
    if ([self isNotNull:[dict objectForKey:@"total_view"]]||[self isNotNull:[dict objectForKey:@"total_reservation"]]) {
    lblViews.text=[NSString stringWithFormat:@"%@",[dict objectForKey:@"total_view"]];
            lblReservation.text=[NSString stringWithFormat:@"%@",[dict objectForKey:@"total_reservation"]];
    }
    else
    {
        lblViews.text=@"0";
        lblReservation.text=@"0";
    }


    
    arrProgressReservations=[[NSMutableArray alloc]init];
    arrProgressReservations=[dict objectForKey:@"progressReservation"];
    [tblVwProgress reloadData];
    
    
    
    //Show Alert Message if No Reservation in progress
    if (!arrProgressReservations.count>0) {
            lblErrorMsg.text=@"No reservations in progress.";
    }
    else
    {
        lblErrorMsg.text=@"";
    }

    [lineChartView removeFromSuperview];
    filteredArray=[[NSMutableArray alloc]init];
    graphDataArr=[dict objectForKey:@"data"];
    
    
    //in Weekly Filtered Array Remove Those Reservation that total reservation =0
    NSMutableArray *arrFilteredWeekly=[[NSMutableArray alloc]init];
    if ([selectedBtnTag isEqualToString:@"1"]) {
        
        for (int i=0; i<graphDataArr.count; i++) {
            NSString *str=[NSString stringWithFormat:@"%@",[[graphDataArr objectAtIndex:i] objectForKey:@"total_reservation"]];
            
            if ([str isEqualToString:@"0"]) {
                
            }
            else
            {
                [arrFilteredWeekly addObject:[graphDataArr objectAtIndex:i]];
            }
        }
        
    }
    
    //Draw X Axis with Their Value
    
    int yMaxValue=0;
    
    if ([selectedBtnTag isEqualToString:@"1"]) {
        for (int i=0; i<arrFilteredWeekly.count; i++) {
            
            NSString *str=[NSString stringWithFormat:@"%@",[[arrFilteredWeekly objectAtIndex:i] objectForKey:@"total_reservation"]];
            
            
            [filteredArray addObject:str];
            
        }

    }
    else
    {
        for (int i=0; i<graphDataArr.count; i++) {
            
            NSString *str=[NSString stringWithFormat:@"%@",[[graphDataArr objectAtIndex:i] objectForKey:@"total_reservation"]];
            
            
            [filteredArray addObject:str];
            
        }
    }
    
    
    //Find Out Maximum Value in FilterArray
    NSNumber *max = [filteredArray valueForKeyPath:@"@max.self"];
    
    if ([max intValue]>90) {
        yMaxValue=[max intValue];
    }
    else
    {
        yMaxValue=90;
    }
    
    
    //Increase XAxis frame size according to graph expand
    float xAxisSize=10;
    float fixedSizeToIncrease=35.0f;
    if (filteredArray.count>8) {
        for (int i=8; i<filteredArray.count; i++) {
            xAxisSize=xAxisSize+fixedSizeToIncrease;
        }
    }
    

    
//Increase YAxis Frame Size if Parameter are going to Greater than 90;
    float yAxisSize=5;
    float yAxisFixedSizeToIncrease=10.0f;
    int yMaxValueChunk=yMaxValue/10;
    if (yMaxValue>90) {
        for (int i=10; i<yMaxValueChunk; i++) {
            yAxisSize=yAxisSize+yAxisFixedSizeToIncrease;
        }
    }
    
    
    if([UIScreen mainScreen].bounds.size.width==414)
    {
        lineChartView=[[PCLineChartView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x, objectView.frame.origin.y+5, objectView.frame.size.width+xAxisSize, objectView.frame.size.height+yAxisSize)];
    }
    else if([UIScreen mainScreen].bounds.size.width==375)
    {
        lineChartView=[[PCLineChartView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x, objectView.frame.origin.y+5, objectView.frame.size.width+xAxisSize, objectView.frame.size.height+yAxisSize)];
    }
    else
    {
        lineChartView=[[PCLineChartView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x, objectView.frame.origin.y+5, objectView.frame.size.width+xAxisSize, objectView.frame.size.height+yAxisSize)];
                NSLog(@"------- ==%f",lineChartView.frame.origin.y);
        NSLog(@"------- ==%f",lineChartView.frame.size.height);
    }

    lineChartView.maxValue=yMaxValue;
    lineChartView.minValue=0;
    lineChartView.delegate=self;
    [scrllVwGraph setScrollEnabled:YES];
    
    
    
    CGPoint bottomOffset = CGPointMake(0, scrllVwGraph.contentSize.height - scrllVwGraph.bounds.size.height);
    [scrllVwGraph setContentOffset:bottomOffset animated:NO];
    
    
    //Where We Start Scrolling Horizontal or Verticall
    NSLog(@"###### %f",lineChartView.frame.size.height);
    if (yMaxValue>90 || filteredArray.count>8) {
        if (yMaxValue>90 && filteredArray.count<=8) {
            //Verticale Scrolling
          [scrllVwGraph setContentSize:CGSizeMake(290, lineChartView.frame.size.height)];
        }
        else if(yMaxValue<=90 && filteredArray.count>8)
        {
                        //Horizontal  Scrolling
            [scrllVwGraph setContentSize:CGSizeMake(lineChartView.frame.size.width-20, 200)];
        }
        else
        {
            //Fully Vertical And Horizontal Scrolling if X and Y Axis going threw limit
            [scrllVwGraph setContentSize:CGSizeMake(lineChartView.frame.size.width, lineChartView.frame.size.height)];
        }
        

    }
    else
    {
        [scrllVwGraph setScrollEnabled:NO];
    }

    
    
    [objectView addSubview:lineChartView];
    [graphTrallingLine setHidden:NO];
    [graphBottomLine setHidden:NO];
    
    NSMutableArray *xValue=[[NSMutableArray alloc]init];
    
    NSMutableArray *components=[[NSMutableArray alloc]init];
    
    if ([selectedBtnTag isEqualToString:@"1"]) {
        for (int i=0; i<arrFilteredWeekly.count; i++) {
        [xValue addObject:[NSString stringWithFormat:@"%@\n%@",[[arrFilteredWeekly objectAtIndex:i] objectForKey:@"weekstartdate"],[[arrFilteredWeekly objectAtIndex:i] objectForKey:@"weekenddate"]]];
        }
    }
    else
    {
        for (int i=0; i<graphDataArr.count; i++) {
            if ([selectedBtnTag isEqualToString:@"0"]) {
                [xValue addObject:[NSString stringWithFormat:@"%@\n%@",[[graphDataArr objectAtIndex:i] objectForKey:@"day"],[[graphDataArr objectAtIndex:i] objectForKey:@"month"]]];
            }
            else if([selectedBtnTag isEqualToString:@"1"])
            {
                [xValue addObject:[NSString stringWithFormat:@"%@\n%@",[[graphDataArr objectAtIndex:i] objectForKey:@"weekstartdate"],[[graphDataArr objectAtIndex:i] objectForKey:@"weekenddate"]]];
            }
            else
            {
                [xValue addObject:[NSString stringWithFormat:@"%@\n%@",[[graphDataArr objectAtIndex:i] objectForKey:@"month"],[[graphDataArr objectAtIndex:i] objectForKey:@"year"]]];
                
            }
        }
    }
    
    PCLineChartViewComponent *component = [[PCLineChartViewComponent alloc] init];

    [component setPoints:filteredArray];
  

    [component setShouldLabelValues:NO];
    [component setColour:PCColorLineBlack];

    [components addObject:component];
    
    [lineChartView setComponents:components];
    [lineChartView setXLabels:xValue];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
