//
//  ReservationAcceptedVC.m
//  Petlox
//
//  Created by Sumit Sharma on 16/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "ReservationAcceptedVC.h"
#import "BusinessCalendar.h"
#import "DeclineVC.h"
@interface ReservationAcceptedVC ()
{
    
    IBOutlet UIImageView *imgProfilePic;
    IBOutlet UILabel *lblReservationTime;
    IBOutlet UILabel *lblUserName;
    IBOutlet UILabel *lblBackToReservation;
    IBOutlet UILabel *lblAddtoCalender;
}
@end

@implementation ReservationAcceptedVC
@synthesize dictUserData;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    NSLog(@"%@",dictUserData);
    // Do any additional setup after loading the view from its nib.
}

-(void)setUpView
{
    self.navigationItem.hidesBackButton = YES;
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Reservation Accepted"];
//    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
//    [self.navigationItem setLeftBarButtonItem:btn];
    
    [lblAddtoCalender setFont:[UIFont fontWithName:lblAddtoCalender.font.fontName size:lblAddtoCalender.font.pointSize*SCREEN_XScale]];
    [lblBackToReservation setFont:[UIFont fontWithName:lblBackToReservation.font.fontName size:lblBackToReservation.font.pointSize*SCREEN_XScale]];
    
    imgProfilePic.clipsToBounds=YES;
    lblReservationTime.text=[dictUserData objectForKey:@"reservation_date"];
    lblUserName.text=[NSString stringWithFormat:@"%@ %@",[dictUserData objectForKey:@"first_name"],[dictUserData objectForKey:@"last_name"]];
    NSString *imgStr=[dictUserData objectForKey:@"image"];
    if (imgStr.length>1) {
        [imgProfilePic setImageWithURL:[NSURL URLWithString:imgStr] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
}
-(IBAction)backBarButtonClicked:(id)sender
{
    //[self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnBacktoReservationClicked:(id)sender {

    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (IBAction)btnAddtoCalender:(id)sender {
    BusinessCalendar *bcvc=[[BusinessCalendar alloc]init];
    bcvc.dictUserData=dictUserData;
    [self.navigationController pushViewController:bcvc animated:YES];
}
- (IBAction)btnCancleReservation:(id)sender {
    DeclineVC *dvc=[[DeclineVC alloc]init];
    dvc.reservationID=[dictUserData valueForKey:@"id"];
    dvc.fromCancel=@"yes";
    [self.navigationController pushViewController:dvc animated:YES];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
