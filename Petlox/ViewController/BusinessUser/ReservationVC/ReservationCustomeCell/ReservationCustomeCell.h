//
//  ReservationCustomeCell.h
//  Petlox
//
//  Created by Sumit Sharma on 12/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReservationCustomeCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgVwProfileImg;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UILabel *lblReservedTime;
@property (strong, nonatomic) IBOutlet UIImageView *imgTopDevider;

@end
