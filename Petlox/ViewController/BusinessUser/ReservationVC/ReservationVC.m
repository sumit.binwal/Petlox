//
//  ReservationVC.m
//  Petlox
//
//  Created by Sumit Sharma on 07/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "ReservationVC.h"
#import "SignInVC.h"
#import "ReservationCustomeCell.h"
#import "IIViewDeckController.h"
#import "EditProfileVC.h"
#import "CustomerInfoVC.h"
#import "BillingWeekHistoryVC.h"
#import "GraphReservationVC.h"
@interface ReservationVC ()<UITableViewDataSource,UITableViewDelegate>
{
    
    IBOutlet UIView *tabBarView;
    IBOutlet UIButton *btnCompleted;
    IBOutlet UIButton *btnAccepted;
    IBOutlet UIButton *btnNew;
    IBOutlet UITableView *tblVw;
    NSMutableArray *arrReserveUser;
    IBOutlet UILabel *lblMsgError;
    NSString *selectedTabIndex;
    
    
    NSArray *arrUserSectionTitle;
    NSMutableDictionary *users;
}
@end

@implementation ReservationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    wbServiceCount=1;
    // Do any additional setup after loading the view from its nib.
}
-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    
    NSLog(@"%f",[[UIScreen mainScreen] bounds].size.height-tabBarView.frame.size.height);
    NSLog(@"%f",tabBarView.frame.size.height);
    
    
    [tabBarView setFrame:CGRectMake(0,([[UIScreen mainScreen] bounds].size.height-(tabBarView.frame.size.height+65)), [[UIScreen mainScreen] bounds].size.width, 50)];
    [self.view addSubview:tabBarView];
    selectedTabIndex=@"0";
    

    UIBarButtonItem *backButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStylePlain target:self.viewDeckController action:@selector(toggleLeftView)];
    [self.navigationItem setLeftBarButtonItem:backButton];

    [btnAccepted setImage:[UIImage imageNamed:@"accepted"] forState:UIControlStateNormal];
    [btnCompleted setImage:[UIImage imageNamed:@"archived"] forState:UIControlStateNormal];
    [btnNew setImage:[UIImage imageNamed:@"newAddFocus"] forState:UIControlStateNormal];

    
    [self.navigationItem setTitle:@"New Reservations"];
    arrReserveUser=[[NSMutableArray alloc]init];
    

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getReservationList];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)reservationGraphBtnClicked:(id)sender
{
    GraphReservationVC *grvc=[[GraphReservationVC alloc]init];
    [self.navigationController pushViewController:grvc animated:YES];
}
- (IBAction)editProfileBtnClicked:(id)sender {
    EditProfileVC *grvc=[[EditProfileVC alloc]init];
    [self.navigationController pushViewController:grvc animated:YES];
    
}
- (IBAction)billingHistoryBtnClicked:(id)sender {
    BillingWeekHistoryVC *bwhvc=[[BillingWeekHistoryVC alloc]initWithNibName:@"BillingWeekHistoryVC" bundle:nil];
    [self.navigationController pushViewController:bwhvc animated:YES];
}
- (IBAction)newRsrvationBtnClicked:(id)sender {
   
    UIButton *btn=(UIButton *)sender;
    int btnTag=btn.tag;
    [btnAccepted setImage:[UIImage imageNamed:@"accepted"] forState:UIControlStateNormal];
    [btnCompleted setImage:[UIImage imageNamed:@"archived"] forState:UIControlStateNormal];
    [btnNew setImage:[UIImage imageNamed:@"newAdd"] forState:UIControlStateNormal];
        switch (btnTag) {
        case 0:
        {
            [self.navigationItem setTitle:@"New Reservations"];

            selectedTabIndex=@"0";
    [btnNew setImage:[UIImage imageNamed:@"newAddFocus"] forState:UIControlStateNormal];
            if ([CommonFunctions reachabiltyCheck]) {
                [CommonFunctions showActivityIndicatorWithText:@""];
                [self getReservationList];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
            }
            
            break;
        }
        case 1:
        {
            [self.navigationItem setTitle:@"Accepted Reservations"];
                        selectedTabIndex=@"1";
            [btnAccepted setImage:[UIImage imageNamed:@"acceptedFocus"] forState:UIControlStateNormal];
            if ([CommonFunctions reachabiltyCheck]) {
                [CommonFunctions showActivityIndicatorWithText:@""];
                [self getReservationList];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
            }
            break;
        }
        case 2:
        {
            selectedTabIndex=@"2";
            [self.navigationItem setTitle:@"Completed Reservations"];
            [btnCompleted setImage:[UIImage imageNamed:@"archivedFocus"] forState:UIControlStateNormal];
            if ([CommonFunctions reachabiltyCheck]) {
                [CommonFunctions showActivityIndicatorWithText:@""];
                [self getReservationList];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
            }
            break;
        }
        default:
            break;
    }
    
    }
- (IBAction)accptdReservationBtnClicked:(id)sender {
    }
- (IBAction)cmpltedRservatinBtnClicked:(id)sender {
    
}
- (IBAction)logoutBtnClicked:(id)sender {
    SignInVC *sivc=[[SignInVC alloc]init];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_TOKEN_ID];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_BUSINESS_TYPE];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_BUSINESS_TYPE];
    [[NSUserDefaults standardUserDefaults]synchronize];
    APPDELEGATE.navController = [[UINavigationController alloc] initWithRootViewController:sivc];
    APPDELEGATE.window.rootViewController = APPDELEGATE.navController;
    [APPDELEGATE.window makeKeyAndVisible];
}


#pragma mark - WebService API
-(void)logoutUser
{
        NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    NSString *url = [NSString stringWithFormat:@"logout"];

    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.173/petlox_svn/mobile/logout
    
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            SignInVC *sivc=[[SignInVC alloc]init];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_TOKEN_ID];
            [[NSUserDefaults standardUserDefaults]synchronize];
            APPDELEGATE.navController = [[UINavigationController alloc] initWithRootViewController:sivc];
            APPDELEGATE.window.rootViewController = APPDELEGATE.navController;
            [APPDELEGATE.window makeKeyAndVisible];
    }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self logoutUser];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}


-(void)getReservationList
{
            NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    
    NSString *url = [NSString stringWithFormat:@"businessUserReservationList"];

    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"user_id",selectedTabIndex,@"reservation_type", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/businessUserReservaionList
    

    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            lblMsgError.text=@"";
            arrReserveUser=[responseDict objectForKey:@"data"];
//            NSLog(@"Data Aee -- %@",[[arrReserveUser valueForKey:@"21 Oct,2015"] objectAtIndex:0]);
//            NSMutableDictionary *dictTemp=[responseDict objectForKey:@"data"];
//            
//            // get all keys into array
//          //  NSArray * keys = [dictTemp allKeys];
//            
//            // sort it
//      //      NSArray * sorted_keys = [keys sortedArrayUsingSelector:@selector(compare:)];
//            
//            NSArray *aUnsorted = [dictTemp allKeys];
//            NSArray *arrKeys = [aUnsorted sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
//                NSDateFormatter *df = [[NSDateFormatter alloc] init];
//                [df setDateFormat:@"dd MMM,yyyy"];
//                NSDate *d1 = [df dateFromString:(NSString*) obj1];
//                NSDate *d2 = [df dateFromString:(NSString*) obj2];
//                return [d1 compare: d2];
//            }];
//            
//
//            
//            NSMutableDictionary *sortedValues = [[NSMutableDictionary alloc] init];
//          
//            sortedValues = [[NSMutableDictionary alloc] init];
//            for (int i=0;i<arrKeys.count;i++) {
//                NSString *key = [arrKeys objectAtIndex:i];
//                id your_value = [dictTemp objectForKey:key];
//                [sortedValues setObject:your_value forKey:key];
//            }
            users=[responseDict objectForKey:@"data"];
           // arrUserSectionTitle=[[[responseDict objectForKey:@"data"] allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
            
            
            
            NSSortDescriptor *sortOrder = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
            arrUserSectionTitle = [[[responseDict objectForKey:@"data"] allKeys] sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortOrder]];
            [tblVw reloadData];
            
        }
        else
        {
            lblMsgError.text=@"No Reservation Found.";
            [users removeAllObjects];
            arrUserSectionTitle = nil;
            [tblVw reloadData];
            //[CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self getReservationList];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}


#pragma mark- UITableView Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrUserSectionTitle.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *sectionTitle = [arrUserSectionTitle objectAtIndex:section];
    
    NSLog(@"%@",arrReserveUser );
    NSMutableArray *sectionDates = [users objectForKey:sectionTitle] ;
    return [sectionDates count];

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f*SCREEN_YScale;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *myLabel = [[UILabel alloc] init];
    myLabel.frame = CGRectMake(9*SCREEN_XScale, 1, 320, 23*SCREEN_XScale);
    myLabel.font = [UIFont fontWithName:FONT_OPENSANS_BOLD size:14.0f*SCREEN_XScale];
    if (arrUserSectionTitle.count>0) {
        myLabel.text =[arrUserSectionTitle objectAtIndex:section];
    }
    else
    {
        myLabel.text=@"";
    }
    [myLabel setTextColor:[UIColor colorWithRed:181.0f/255.0f green:37.0f/255.0f blue:86.0f/255.0f alpha:1]];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320.0f, 30.0f*SCREEN_XScale)];
    headerView.backgroundColor=[UIColor colorWithRed:239.0f/255.0f green:239.0f/255.0f blue:239.0f/255.0f alpha:1];
    [headerView addSubview:myLabel];
    
    
    return headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"ReservationCustomeCell";
    ReservationCustomeCell *cell=[[ReservationCustomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
  
    cell.imgVwProfileImg.clipsToBounds=YES;
    
    NSString *sectionTitle = [arrUserSectionTitle objectAtIndex:indexPath.section];
    NSArray *sectionUser = [users objectForKey:sectionTitle];
   // NSString *userList = [sectionUser objectAtIndex:indexPath.row];

    cell.lblUserName.text=[NSString stringWithFormat:@"%@ %@",[[sectionUser objectAtIndex:indexPath.row]objectForKey:@"first_name"],[[sectionUser objectAtIndex:indexPath.row]objectForKey:@"last_name"]];

    cell.lblReservedTime.text=[[sectionUser objectAtIndex:indexPath.row] objectForKey:@"reservation_date"];
    
    [cell.imgVwProfileImg setImageWithURL:[NSURL URLWithString:[[sectionUser objectAtIndex:indexPath.row]objectForKey:@"image" ]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    cell.backgroundColor=[UIColor clearColor];
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *sectionTitle = [arrUserSectionTitle objectAtIndex:indexPath.section];
    NSArray *sectionUser = [users objectForKey:sectionTitle];
    
    CustomerInfoVC *civc=[[CustomerInfoVC alloc]initWithNibName:@"CustomerInfoVC" bundle:nil];
    civc.dictCustomerInfo=[sectionUser objectAtIndex:indexPath.row];
    civc.strFromTap=selectedTabIndex;
    [self.navigationController pushViewController:civc animated:YES];
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 27.0f*SCREEN_XScale;
}
//-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    return @"Hello";
//}
/*
#pragma mark - Navigation

 
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
