//
//  BusinessRightMenu.m
//  Petlox
//
//  Created by Sumit Sharma on 21/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "BusinessRightMenu.h"
#import "BusinessSettingVC.h"
#import "ViewFullImageVC.h"
#import "IIViewDeckController.h"
#import "EditProfileVC.h"
#import "ReservationVC.h"
#import "EditBusinessHoursVC.h"
#import "EditPhotoVC.h"
#import "MessageInboxVC.h"
#import "BusinessReviewVC.h"
#import "BillingWeekHistoryVC.h"
#import "BusinessRightMenuCell.h"
#import "GraphReservationVC.h"
#import "SettingVC.h"

@interface BusinessRightMenu ()<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UIImageView *imgProfileImage;
    IBOutlet UILabel *lblBusinessName;
    IBOutlet UIView *vwForImg;
    IBOutlet UIScrollView *scrllVw;
    UIViewController *recentVwController;
    IBOutlet UILabel *lblPhoneNumber;
    IBOutlet UILabel *lblEmailAddress;
    IBOutlet UILabel *lblBusinessOwnerName;
    NSMutableArray *arrData;
}
@end

@implementation BusinessRightMenu

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUpView];
    [self.view endEditing:YES];
}
-(void)viewDidLayoutSubviews
{
  
    [self.navigationController setHidesBarsOnSwipe:NO];
    if ([UIScreen mainScreen].bounds.size.height<568) {
        [scrllVw setScrollEnabled:YES];
        [scrllVw setContentSize:CGSizeMake(320.0f, 600.0f)];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.view endEditing:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.view endEditing:YES];
    NSString *urlStr=[[NSUserDefaults standardUserDefaults]objectForKey:UD_USER_IMG];
    if (urlStr.length>0) {
        [imgProfileImage setImageWithURL:[NSURL URLWithString:urlStr] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    else
    {
        [imgProfileImage setImage:[UIImage imageNamed:@"DefaultProfileImg"]];
    }
    
    [lblBusinessName setFont:[UIFont fontWithName:lblBusinessName.font.fontName size:15*SCREEN_XScale]];
    [lblBusinessOwnerName setFont:[UIFont fontWithName:lblBusinessOwnerName.font.fontName size:13*SCREEN_XScale]];
    [lblEmailAddress setFont:[UIFont fontWithName:lblEmailAddress.font.fontName size:15*SCREEN_XScale]];
    [lblPhoneNumber setFont:[UIFont fontWithName:lblPhoneNumber.font.fontName size:15*SCREEN_XScale]];
    
    
    lblBusinessOwnerName.text=[NSString stringWithFormat:@"%@ %@",[[NSUserDefaults standardUserDefaults] objectForKey:UD_FIRST_NAME],[[NSUserDefaults standardUserDefaults] objectForKey:UD_LAST_NAME]];
    lblBusinessName.text=[[NSUserDefaults standardUserDefaults] objectForKey:UD_BUSINESS_NAME];
    lblEmailAddress.text=[[NSUserDefaults standardUserDefaults] objectForKey:UD_BUSINESS_EMAIL];
    lblPhoneNumber.text=[[NSUserDefaults standardUserDefaults] objectForKey:UD_BUSINESS_PHONE];
}


-(void)setUpView
{
    
    imgProfileImage.clipsToBounds=YES;

    
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [self.view addGestureRecognizer:recognizer];
    
    arrData=[[NSMutableArray alloc]initWithObjects:@"Dashboard",@"Business Photos",@"Billing Information",@"User Reviews",@"Hours of Operation",@"Messages",@"Settings", nil];
    
    [CommonFunctions setNavigationBar:self.navigationController];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"TestNotification"
                                               object:nil];
}

- (void) receiveTestNotification:(NSNotification *) notification
{
    
    recentVwController=(UIViewController *)notification.userInfo;
    NSLog(@"%@",recentVwController);
    
}

- (void) didSwipe:(UISwipeGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    if([recognizer direction] == UISwipeGestureRecognizerDirectionLeft)
    {
        //Swipe from right to left
        if ([recentVwController isKindOfClass:[ReservationVC class]]) {
            recentVwController=[[ReservationVC alloc]init];
        }
        else if ([recentVwController isKindOfClass:[GraphReservationVC class]]) {
            recentVwController=[[GraphReservationVC alloc]init];
        }
        else if ([recentVwController isKindOfClass:[EditPhotoVC class]]) {
            recentVwController=[[EditPhotoVC alloc]init];
        }
        else if ([recentVwController isKindOfClass:[BillingWeekHistoryVC class]]) {
            recentVwController=[[BillingWeekHistoryVC alloc]init];
        }
        else if ([recentVwController isKindOfClass:[BusinessReviewVC class]]) {
            recentVwController=[[BusinessReviewVC alloc]init];
        }
        else if ([recentVwController isKindOfClass:[EditBusinessHoursVC class]]) {
            recentVwController=[[EditBusinessHoursVC alloc]init];
        }
        else if ([recentVwController isKindOfClass:[MessageInboxVC class]]) {
            recentVwController=[[MessageInboxVC alloc]init];
        }
        else if ([recentVwController isKindOfClass:[BusinessSettingVC class]]) {
            recentVwController=[[BusinessSettingVC alloc]init];
        }
        else if ([recentVwController isKindOfClass:[EditProfileVC class]]) {
            recentVwController=[[EditProfileVC alloc]init];
        }
        [(UINavigationController *)self.viewDeckController.centerController pushViewController:recentVwController animated:NO];
        [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:recentVwController]];
        [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
        }];
    }
    else
    {
        //Swipe from left to Right
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)msgButtonCLicked:(id)sender
{
   
}
- (IBAction)bullingInformationBtnClicked:(id)sender {
    
}

#pragma mark - IBActionMethods
- (IBAction)editHoursOperationBtnClicked:(id)sender
{
    
}

- (IBAction)uploadBusinessPhotoBtnClicked:(id)sender {
    }

- (IBAction)userReviewBtnClicked:(id)sender
{
    
}

- (IBAction)settingBtnClicked:(id)sender {
    
}
- (IBAction)leftBarButtonClicked:(id)sender {
    ReservationVC *home  = [[ReservationVC alloc]init];
    [(UINavigationController *)self.viewDeckController.centerController pushViewController:home animated:NO];
    [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:home]];
    [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
    }];

}
- (IBAction)imageBtnClicked:(id)sender {
    NSString *urlStr=[[NSUserDefaults standardUserDefaults]objectForKey:UD_USER_IMG];
    ViewFullImageVC *fullImgVC=[[ViewFullImageVC alloc]initWithNibName:@"ViewFullImageVC" bundle:nil];
    fullImgVC.strImgURL=urlStr;
    [self presentViewController:fullImgVC animated:YES completion:nil];

}
- (IBAction)editButtonClicked:(id)sender
{
    EditProfileVC *epvc  = [[EditProfileVC alloc]init];
    [(UINavigationController *)self.viewDeckController.centerController pushViewController:epvc animated:NO];
    [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:epvc]];
    [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
    }];
}


#pragma mark - UITableView Delegate Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrData.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 43.0f*SCREEN_YScale;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"BusinessRightMenuCell";
    
    BusinessRightMenuCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell==nil) {
        cell=[[BusinessRightMenuCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(BusinessRightMenuCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.lblTxt.text=arrData[indexPath.row];
    cell.backgroundColor=[UIColor clearColor];
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
        {
      
            GraphReservationVC *home  = [[GraphReservationVC alloc]init];
            [(UINavigationController *)self.viewDeckController.centerController pushViewController:home animated:NO];
            [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:home]];
            [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
            }];
            break;
        }
        case 1:
        {
            EditPhotoVC *home  = [[EditPhotoVC alloc]init];
            [(UINavigationController *)self.viewDeckController.centerController pushViewController:home animated:NO];
            [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:home]];
            [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
            }];
            break;
        }
        case 2:
        {
            BillingWeekHistoryVC *home  = [[BillingWeekHistoryVC alloc]init];
            [(UINavigationController *)self.viewDeckController.centerController pushViewController:home animated:NO];
            [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:home]];
            [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
            }];
            break;
        }
        case 3:
        {
            BusinessReviewVC *home  = [[BusinessReviewVC alloc]init];
            [(UINavigationController *)self.viewDeckController.centerController pushViewController:home animated:NO];
            [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:home]];
            [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
            }];
            break;
        }
        case 4:
        {
            EditBusinessHoursVC *home  = [[EditBusinessHoursVC alloc]init];
            [(UINavigationController *)self.viewDeckController.centerController pushViewController:home animated:NO];
            [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:home]];
            [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
            }];
            break;
        }
        case 5:
        {
            MessageInboxVC *home  = [[MessageInboxVC alloc]init];
            [(UINavigationController *)self.viewDeckController.centerController pushViewController:home animated:NO];
            [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:home]];
            [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
            }];
            break;
        }
        case 6:
        {
            SettingVC *home  = [[SettingVC alloc]init];
            [(UINavigationController *)self.viewDeckController.centerController pushViewController:home animated:NO];
            [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:home]];
            [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
            }];
            break;
        }
            
            
        default:
            break;
    }
    
    
}


@end
