//
//  RightMenuMessageCell.m
//  Petlox
//
//  Created by Suchita Bohra on 22/07/16.
//  Copyright © 2016 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "BusinessRightMenuCell.h"

@implementation BusinessRightMenuCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        self = (BusinessRightMenuCell *)[[[NSBundle mainBundle] loadNibNamed:@"BusinessRightMenuCell" owner:self options:nil] firstObject];
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [_lblTxt setFont:[UIFont fontWithName:_lblTxt.font.fontName size:_lblTxt.font.pointSize*SCREEN_XScale]];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
