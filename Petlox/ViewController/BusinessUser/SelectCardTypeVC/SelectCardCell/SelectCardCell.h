//
//  SelectCardCell.h
//  Petlox
//
//  Created by Sumit Sharma on 22/07/16.
//  Copyright © 2016 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectCardCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *btnCheckUncheck;
@property (strong, nonatomic) IBOutlet UIImageView *imgCardDetail;

@end
