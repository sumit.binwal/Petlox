//
//  SelectCardCell.m
//  Petlox
//
//  Created by Sumit Sharma on 22/07/16.
//  Copyright © 2016 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "SelectCardCell.h"

@implementation SelectCardCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    return  self=[[[NSBundle mainBundle]loadNibNamed:@"SelectCardCell" owner:self options:nil]objectAtIndex:0];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
