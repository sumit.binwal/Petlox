//
//  UploadPhotoVC.m
//  Petlox
//
//  Created by Sumit Sharma on 03/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "UploadPhotoVC.h"
#import "UploadPhotoCustomeCell.h"
#import "GoToMyAccountVC.h"
#import "ELCImagePickerHeader.h"

@interface UploadPhotoVC ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,ELCImagePickerControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate>
{
    IBOutlet UICollectionView *collctionVw;
    NSMutableArray *imgArr;
    IBOutlet UIButton *btnNext;
}
@end

@implementation UploadPhotoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    
   // [self csmAddlongPressgestureOncollectionview];
    // Do any additional setup after loading the view from its nib.
}


-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    
    [collctionVw registerNib:[UINib nibWithNibName:@"UploadPhotoCustomeCell" bundle:nil] forCellWithReuseIdentifier:@"UploadPhotoCustomeCell"];

    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
    btnNext.layer.cornerRadius=btnNext.bounds.size.width/2*SCREEN_XScale;
    [btnNext.titleLabel setFont:[UIFont fontWithName:btnNext.titleLabel.font.fontName size:btnNext.titleLabel.font.pointSize*SCREEN_XScale]];
 
    
    btn2=[[UIBarButtonItem alloc]initWithTitle:@"Delete" style:UIBarButtonItemStylePlain target:self action:@selector(deleteBarButtonClicked)];
    [self.navigationItem setRightBarButtonItem:btn2];
    
    [self.navigationItem setTitle:@"Upload Photo"];
    
    imgArr=[[NSMutableArray alloc]init];
}
#pragma mark - IBAction Methods

-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    if (imgArr.count==0)
    {
        [btn2 setTitle:@"Skip"];
    }
    else
    {
        [btn2 setTitle:@"Delete"];
         isForDelete=NO;
    }

}
-(void)deleteBarButtonClicked
{
    if (imgArr.count==0)
    {
        GoToMyAccountVC *gtmvc=[[GoToMyAccountVC alloc]init];
        [self.navigationController pushViewController:gtmvc animated:YES];
    }
    else
    {
        if (isForDelete)
        {
            [btn2 setTitle:@"Delete"];
//            for (int i=0; i<arrObjectsDelete.count; i++)
//            {
//                [imgArr removeObject:[arrObjectsDelete objectAtIndex:i]];
//            }
            [collctionVw reloadData];
            isForDelete=NO;
        }
        else
        {
            [btn2 setTitle:@"Done"];
            isForDelete=YES;
            [collctionVw reloadData];
        }
 
    }
    
    
}

- (IBAction)doneBtnClicked:(id)sender {
    if (imgArr.count>0) {
        
        if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self uploadImagesToServer];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please check your ineternet connection ."];
        }
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please add atleast one photo."];
    }
}
-(IBAction)addImgBtnClicked:(id)sender
{
    UIActionSheet *actnSheet=[[UIActionSheet alloc]initWithTitle:@"Choose Image" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose From Gallery", nil];
    [actnSheet showInView:self.view];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UIActionSheet Delegate Method
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePickerController.allowsEditing = YES;
        imagePickerController.delegate = self;
        
        imagePickerController.modalPresentationCapturesStatusBarAppearance = true;
        
        [self presentViewController:imagePickerController animated:YES completion:NULL];

    }
    else if (buttonIndex ==1)
    {
        ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initImagePicker];
        
        elcPicker.maximumImagesCount = 10; //Set the maximum number of images to select to 100
        elcPicker.returnsOriginalImage = NO; //Only return the fullScreenImage, not the fullResolutionImage
        elcPicker.returnsImage = YES; //Return UIimage if YES. If NO, only return asset location information
        elcPicker.onOrder = YES; //For multiple image selection, display and return order of selected images
        elcPicker.mediaTypes = @[(NSString *)kUTTypeImage, (NSString *)kUTTypeMovie]; //Supports image and movie types
        
        elcPicker.imagePickerDelegate = self;
        
        [self presentViewController:elcPicker animated:YES completion:nil];

    }
}



#pragma mark - ELCImage Picker ContrllerDelegate Method

- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{

        
        NSMutableArray *images = [NSMutableArray arrayWithCapacity:[info count]];
        for (NSDictionary *dict in info)
        {
            if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto)
            {
                if ([dict objectForKey:UIImagePickerControllerOriginalImage])
                {
                    UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
                    [images addObject:image];
                    
                    if (imgArr.count<10) {
                        [imgArr addObject:image];
                        [collctionVw reloadData];
                        [self dismissViewControllerAnimated:YES completion:nil];

                    }
                    else
                    {
                        [CommonFunctions alertTitle:@"" withMessage:@"Maximum 10 photos allowed."];
                                                [self dismissViewControllerAnimated:YES completion:nil];
                        return;
                    }
                    
                
                } else
                {
                    NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
                }
            }
            else {
                NSLog(@"Uknown asset type");
            }
        
    }
    

    [collctionVw reloadData];

}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
//code by chhagan
#pragma mark - UINavigationController Delegate

-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    

    navigationController.navigationBar.backgroundColor=[UIColor colorWithRed:71.0f/255.0f green:13.0f/255.0f blue:52.0f/255.0f alpha:1];
    
    UIView *statusView=[[UIView alloc]initWithFrame:CGRectMake(0, -[UIApplication sharedApplication].statusBarFrame.size.height,[UIApplication sharedApplication].statusBarFrame.size.width, [UIApplication sharedApplication].statusBarFrame.size.height)];
    [statusView setBackgroundColor:[UIColor colorWithRed:71.0f/255.0f green:13.0f/255.0f blue:52.0f/255.0f alpha:1]];
    [navigationController.navigationBar addSubview:statusView];
    [navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:FONT_GULIM size:18.85f]}];
    
    viewController.navigationController.navigationBar.tintColor=[UIColor whiteColor];
 
}


#pragma mark - UIImagePicker Controller Delegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
   NSLog(@"%@",info);
    if (imgArr.count<10) {
        [imgArr addObject:[info objectForKey:UIImagePickerControllerOriginalImage]];
        [collctionVw reloadData];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Maximum 10 photos allowed."];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


#pragma mark - CollectionView Delegate Method

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    int totalImg=(int)imgArr.count+1;
    NSLog(@"%d",totalImg);
    if (imgArr.count<1) {
        return 1;
    }
    else
    {
        return imgArr.count+1;
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    //DLog(@"SETTING SIZE FOR ITEM AT INDEX %d", indexPath.row);
    if ([UIScreen mainScreen].bounds.size.width==414) {
        CGSize mElementSize = CGSizeMake(126,126);
        return mElementSize;
        
    }
    else if ([UIScreen mainScreen].bounds.size.height==667)
    {
        CGSize mElementSize = CGSizeMake(113,113);
        return mElementSize;
        
    }
    else
    {
        CGSize mElementSize = CGSizeMake(96*SCREEN_XScale,96*SCREEN_XScale);
        return mElementSize;

    }
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UploadPhotoCustomeCell *cell = (UploadPhotoCustomeCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"UploadPhotoCustomeCell" forIndexPath:indexPath];
    

        [cell.imgAddBtn addTarget:self action:@selector(addImgBtnClicked:) forControlEvents:UIControlEventTouchUpInside];

        if (imgArr.count>0 && indexPath.row<imgArr.count) {
        [cell.imgPhoto setImage:[imgArr objectAtIndex:indexPath.row]];
            [cell.imgAddBtn setHidden:YES];
        }
        else
        {
        [cell.imgPhoto setImage:[UIImage imageNamed:@"AddPhotoBtn"]];
                        [cell.imgAddBtn setHidden:NO];
        }
    
    if (isForDelete)
    {
        if (indexPath.row==imgArr.count)
        {
            cell.imgDeleteBtn.hidden=YES;
        }
        else
        {
            cell.imgDeleteBtn.hidden=NO;
        }
        
    }
    else
    {
        cell.imgDeleteBtn.hidden=YES;

    }
    

    return cell;
}

-(void)collectionView: (UICollectionView*)collectionView didSelectItemAtIndexPath:   (NSIndexPath*)indexPath
{
    UploadPhotoCustomeCell* cell = (UploadPhotoCustomeCell*)[collctionVw cellForItemAtIndexPath:indexPath];
    if (!cell.imgDeleteBtn.hidden)
    {
        
        [self deleteImgButtonClicked:indexPath.row];
//        if (arrObjectsDelete.count==0)
//        {
//            arrObjectsDelete=[[NSMutableArray alloc]init];
//        }
//        
//        
//        BOOL isContainObj=[arrObjectsDelete containsObject:[imgArr objectAtIndex:indexPath.row]];
//        
//        if (isContainObj)
//        {
//            [arrObjectsDelete removeObject:[imgArr objectAtIndex:indexPath.row]];
//        }
//        else
//        {
//            [arrObjectsDelete addObject:[imgArr objectAtIndex:indexPath.row]];
//        }
        
    }
    
    
}


#pragma mark- method longPress gesture
-(void)csmAddlongPressgestureOncollectionview
{
    UILongPressGestureRecognizer *lpgr= [[UILongPressGestureRecognizer alloc]
                                         initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = .3; //seconds
    lpgr.delegate = self;
    [collctionVw addGestureRecognizer:lpgr];
    
    
    UITapGestureRecognizer *tapgr= [[UITapGestureRecognizer alloc]
                                         initWithTarget:self action:@selector(handleTapPress:)];
    tapgr.numberOfTapsRequired =2; //seconds
    tapgr.delegate = self;
    [collctionVw addGestureRecognizer:tapgr];
    
}

-(void)handleTapPress:(UITapGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded) {
        return;
    }
    
    if (longPressEffect)
    {
        if (!isForDelete)
        {
            isForDelete=YES;
            //  cell.imgDeleteBtn.hidden=NO;
            
        }
        else
        {
            isForDelete=NO;
            
            // cell.imgDeleteBtn.hidden=YES;
        }
        [collctionVw reloadData];
        longPressEffect=NO;

    }
   
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded) {
        return;
    }
//    CGPoint p = [gestureRecognizer locationInView:collctionVw];
//    
//    NSIndexPath *indexPath = [collctionVw indexPathForItemAtPoint:p];
//    if (indexPath == nil)
//    {
//        NSLog(@"couldn't find index path");
//    } else {
//        // get the cell at indexPath (the one you long pressed)
//        UploadPhotoCustomeCell* cell = (UploadPhotoCustomeCell*)[collctionVw cellForItemAtIndexPath:indexPath];

    longPressEffect=YES;
    
        if (!isForDelete)
        {
            isForDelete=YES;
          //  cell.imgDeleteBtn.hidden=NO;
            
        }
        else
        {
            isForDelete=NO;
            
           // cell.imgDeleteBtn.hidden=YES;
        }
    [collctionVw reloadData];
    
    //}
    
}


#pragma  mark - delete on longpressAction
-(void)deleteImgButtonClicked:(int)sender
{
    [collctionVw performBatchUpdates:^{
       
        [imgArr removeObjectAtIndex:sender];
        NSIndexPath *indexPath =[NSIndexPath indexPathForRow:sender inSection:0];
        [collctionVw deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
    } completion:^(BOOL finished) {
        
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark- WebServices API..

-(void)uploadImagesToServer
{
    
    NSString *url = [NSString stringWithFormat:@"%@uploadUserPicture",serverURL];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"sid", nil];
    NSLog(@"%@",param);
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:url]];
    
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID] forHTTPHeaderField:@"token"];
    AFHTTPRequestOperation *op = [manager POST:@"" parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if (imgArr.count>0) {
            for (int i=0; i<imgArr.count; i++) {
                NSData *imageData = UIImageJPEGRepresentation([imgArr objectAtIndex:i], 0.8f);
                NSLog(@"0.8 %lu",(unsigned long)imageData.length);
                NSData *imageData1 = UIImageJPEGRepresentation([imgArr objectAtIndex:i], 0.0f);
                NSLog(@"0.0 - %lu",(unsigned long)imageData1.length);
                
                NSString *fileImg=[NSString stringWithFormat:@"profileImg%d.jpg",i];
                NSString *parameterNm=[NSString stringWithFormat:@"image%d",i];
                
                [formData appendPartWithFileData:imageData name:parameterNm fileName:fileImg mimeType:@"image/jpeg"];
            }
        }
        
   
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        
        if (responseObject != nil)
        {
            NSLog(@"Profile  responseObject error: %@",error);
            
            NSDictionary *jsonDic = (NSDictionary *)responseObject;
            NSLog(@"Profile  JSON: %@",jsonDic);
            
            if (jsonDic != nil)
            {
                if ([[jsonDic objectForKey:@"replyCode"] isEqualToString:@"success"])
                {
                       GoToMyAccountVC *gtmvc=[[GoToMyAccountVC alloc]init];
                        [self.navigationController pushViewController:gtmvc animated:YES];

                                    }
                else
                {
                    [CommonFunctions alertTitle:@"" withMessage:[jsonDic objectForKey:@"replyMsg"]];
                }
                
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[jsonDic objectForKey:@"replyMsg"]];
            }
        }
        [CommonFunctions removeActivityIndicator];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
        [CommonFunctions removeActivityIndicator];
    }];
    
    
    [op start];
    
}

@end
