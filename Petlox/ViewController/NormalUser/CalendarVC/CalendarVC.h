//
//  CalendarVC.h
//  Petlox
//
//  Created by Sumit Sharma on 15/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalendarVC : UIViewController
@property(nonatomic,strong)    IBOutlet UILabel *lblWeekDay;
@property(nonatomic,strong)NSString *strbusiessName;
@property(nonatomic,strong)NSString *strAppointedDate;

@end
