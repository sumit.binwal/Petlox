//
//  ConfirmReservationVC.h
//  Petlox
//
//  Created by Sumit Sharma on 03/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
@interface ConfirmReservationVC : UIViewController<GMSMapViewDelegate,GMSIndoorDisplayDelegate,GMSPanoramaViewDelegate,GMSTileReceiver>
{
    
}
@property (strong, nonatomic) IBOutlet GMSMapView *mapView;
@property(nonatomic,strong)NSMutableDictionary *dictBusinessUser;
@property(nonatomic,strong)NSMutableDictionary *dictPetDetail;
@end
