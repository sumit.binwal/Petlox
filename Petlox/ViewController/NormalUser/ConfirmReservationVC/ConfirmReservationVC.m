//
//  ConfirmReservationVC.m
//  Petlox
//
//  Created by Sumit Sharma on 03/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "ConfirmReservationVC.h"
#import "ThanksVC.h"
#import "ViewFullImageVC.h"
#import "MapVC.h"
@interface ConfirmReservationVC ()
{
    
    IBOutlet UIButton *btnFav;
    IBOutlet UIImageView *imgStar5;
    IBOutlet UIImageView *imgStar4;
    IBOutlet UIImageView *imgStar3;
    IBOutlet UIScrollView *scrllVw;
    IBOutlet UIImageView *imgStar2;
    IBOutlet UIView *vwCntainerVw;
    IBOutlet UIImageView *imgStar1;
    IBOutlet UILabel *lblBusinessAddress;
    IBOutlet UIImageView *imgBusinessProfile;
    IBOutlet UILabel *lblBusinessDistance;
    IBOutlet UILabel *lblBusinessReview;
    IBOutlet UILabel *lblBusinessName;
    IBOutlet UILabel *lblPetName;
    IBOutlet UILabel *lblReservationTime;
    IBOutlet UILabel *lblPetStatus;
    IBOutlet UILabel *lblPetBreed;
    IBOutlet UILabel *lblPetAge;
    IBOutlet UILabel *lblPetWeight;
    IBOutlet UIImageView *imgPetProfileImage;
    __weak IBOutlet UIButton *btnComplete;

}
@end

@implementation ConfirmReservationVC
@synthesize mapView,dictBusinessUser,dictPetDetail;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    wbServiceCount=1;
    [self fillDetails];
    NSLog(@"dictPetDetail - %@",dictPetDetail);
        NSLog(@"DictBusinessDetail - %@",dictBusinessUser);
    // Do any additional setup after loading the view from its nib.
}



-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
    UIBarButtonItem *btn1=[[UIBarButtonItem alloc]initWithTitle:@"Map" style:UIBarButtonItemStylePlain target:self action:@selector(btnMapViewClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setRightBarButtonItem:btn1];
    
    
    [self.navigationItem setTitle:@"Confirm Reservation"];

    imgPetProfileImage.clipsToBounds=YES;
    
    
    mapView.mapType = kGMSTypeNormal;
    mapView.delegate = self;
    
    [self mapWithFrameFortLocation];
    

}
-(void)viewWillAppear:(BOOL)animated
{
    [APPDELEGATE showPetReservationTabBar:YES];
    [super viewWillAppear:YES];
    if ([UIScreen mainScreen].bounds.size.height<568) {
        [scrllVw setContentSize:CGSizeMake(320.0f, 568.0f)];
        [scrllVw setScrollEnabled:YES];
            NSLog(@"%f",self.view.frame.size.height);
    }
}
-(void)viewDidLayoutSubviews
{
    NSLog(@"%f",self.view.frame.size.height);
    if ([UIScreen mainScreen].bounds.size.height<568) {
                [scrllVw setContentSize:CGSizeMake(320.0f, 568.0f)];
        [scrllVw setScrollEnabled:YES];
    }
    if(K_SCREEN_WIDTH<=320){
        btnComplete.titleLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:14.0];
    }
    btnComplete.layer.cornerRadius = btnComplete.frame.size.width/2;
}

-(void)fillDetails
{
    if ([self isNotNull:[dictBusinessUser objectForKey:@"fav"]]) {
        NSString *strFav=[NSString stringWithFormat:@"%@",[dictBusinessUser objectForKey:@"fav"]];
        if ([strFav isEqualToString:@"1"]) {
            [btnFav setImage:[UIImage imageNamed:@"favorite_foucs_icon"] forState:UIControlStateNormal];
        }
        else
        {
            [btnFav setImage:[UIImage imageNamed:@"favorite_icon"] forState:UIControlStateNormal];
        }
    }
    
    lblBusinessName.text=[[dictBusinessUser objectForKey:@"business_name"] capitalizedString];
    lblBusinessDistance.text=[dictBusinessUser objectForKey:@"distance"];
    lblBusinessAddress.text=[dictBusinessUser objectForKey:@"address"];
    NSString *imgStr=[dictBusinessUser objectForKey:@"image"];
    if (imgStr.length>1) {
        [imgBusinessProfile setImageWithURL:[NSURL URLWithString:imgStr] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
     }
    imgBusinessProfile.clipsToBounds=YES;
    lblReservationTime.text=[dictBusinessUser objectForKey:@"appointedDate"];
    if ([dictBusinessUser valueForKey:@"review_count"]!=[NSNull null])
    {
        int reviewCount=[[dictBusinessUser valueForKey:@"review_count"]intValue];
        if (reviewCount>1) {
            lblBusinessReview.text=[NSString stringWithFormat:@"%@ reviews",[dictBusinessUser valueForKey:@"review_count"]];
        }
        else
        {
            lblBusinessReview.text=[NSString stringWithFormat:@"%@ review",[dictBusinessUser valueForKey:@"review_count"]];
        }
    }
    if ([self isNotNull:[dictBusinessUser objectForKey:@"avg_rate"]]) {
        int avgRating=[[dictBusinessUser objectForKey:@"avg_rate"]intValue];
        switch (avgRating) {
            case 0:
            {
                break;
            }
                
            case 1:
            {
                [imgStar1 setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
            case 2:
            {
                [imgStar1 setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgStar2 setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
            case 3:
            {
                [imgStar1 setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgStar2 setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgStar3 setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
            case 4:
            {
                [imgStar1 setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgStar2 setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgStar3 setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgStar4 setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
            case 5:
            {
                [imgStar1 setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgStar2 setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgStar3 setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgStar4 setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgStar5 setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
                
            default:
                break;
        }
        
    }
// Pet details fills
    lblPetAge.text=[NSString stringWithFormat:@"%@ Years",[dictPetDetail objectForKey:@"age"]];
    lblPetBreed.text=[NSString stringWithFormat:@"%@",[dictPetDetail objectForKey:@"pet_details"]];
    lblPetName.text=[CommonFunctions trimSpaceInString:[dictPetDetail objectForKey:@"name"]];
    lblPetWeight.text=[NSString stringWithFormat:@"%@lbs",[dictPetDetail objectForKey:@"weight"]];
    lblPetStatus.text=[CommonFunctions trimSpaceInString:[dictPetDetail objectForKey:@"reservation_status"]];
    NSString *petImgStr=[dictPetDetail objectForKey:@"image"];
    if (petImgStr.length>1) {
        [imgPetProfileImage setImageWithURL:[NSURL URLWithString:petImgStr] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
}

-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnPetImageClicked:(id)sender {
    ViewFullImageVC *vfivc=[[ViewFullImageVC alloc]init];
    vfivc.strImgURL=[dictPetDetail objectForKey:@"image"];
    [self presentViewController:vfivc animated:YES completion:nil];
}
- (IBAction)btnMapViewClicked:(id)sender {
    MapVC *mpVC=[[MapVC alloc]init];
    mpVC.dictUserData=dictBusinessUser;
    [self.navigationController pushViewController:mpVC animated:YES];
    
}

-(void)mapWithFrameFortLocation
{
    float lattitude=[[dictBusinessUser valueForKey:@"lat"] floatValue];
    float longitutde=[[dictBusinessUser valueForKey:@"lon"] floatValue];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lattitude
                                                            longitude:longitutde
                                                                 zoom:10];
    mapView.camera=camera;
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(lattitude, longitutde);
    marker.title = [NSString stringWithFormat:@"%@",[dictBusinessUser valueForKey:@"business_name"]];
    marker.snippet = [NSString stringWithFormat:@"%@",[dictBusinessUser valueForKey:@"address"]];
    
    marker.map = mapView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)completeReservationButtonClicked:(id)sender
{

    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self postConfirmReservation];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection first."];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark- WebServices API..

-(void)postConfirmReservation
{
    NSMutableDictionary *param;
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    
    dateFormatter.dateFormat = @"dd, MMM yyyy hh:mma";
    NSString *myDate=[dictBusinessUser objectForKey:@"appointedDate"];
    //dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *convertDate = [dateFormatter dateFromString:myDate];
    NSString *myString = [dateFormatter stringFromDate:convertDate];
    NSLog(@"%@",myString);
    [dateFormatter setAMSymbol:@"am"];
    [dateFormatter setPMSymbol:@"pm"];
    NSDate *yourDate = [dateFormatter dateFromString:myString];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString *dateString=[dateFormatter stringFromDate:yourDate] ;
    NSLog(@"%@",dateString);
    
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"user_id",[dictBusinessUser objectForKey:@"user_id"],@"business_id",dateString,@"reservation_date",[dictPetDetail objectForKey:@"id"],@"pet_info_id",@"1",@"country_code",[dictBusinessUser objectForKey:@"phone"],@"phone", nil];
NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    
    NSString *url = [NSString stringWithFormat:@"confirmReservation"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/confirmReservation
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            ThanksVC *tvc=[[ThanksVC alloc]initWithNibName:@"ThanksVC" bundle:nil];
            tvc.reservationID=[responseDict objectForKey:@"id"];
            tvc.businessName=lblBusinessName.text;
            tvc.dictReservationDetail=dictBusinessUser;
            [self.navigationController pushViewController:tvc animated:YES];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self postConfirmReservation];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}
@end
