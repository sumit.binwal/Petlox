//
//  FavoriteCustomeCell.h
//  Petlox
//
//  Created by Sumit Sharma on 13/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoriteCustomeCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgBusinessProfile;
@property (strong, nonatomic) IBOutlet UIImageView *imgTickMark;
@property (strong, nonatomic) IBOutlet UILabel *lblBusinessName;
@property (strong, nonatomic) IBOutlet UILabel *lblReviews;
@property (strong, nonatomic) IBOutlet UIImageView *imgStar1;
@property (strong, nonatomic) IBOutlet UIImageView *imgStar2;
@property (strong, nonatomic) IBOutlet UIImageView *imgStar3;
@property (strong, nonatomic) IBOutlet UIImageView *imgStar4;
@property (strong, nonatomic) IBOutlet UIImageView *imgStar5;

@end
