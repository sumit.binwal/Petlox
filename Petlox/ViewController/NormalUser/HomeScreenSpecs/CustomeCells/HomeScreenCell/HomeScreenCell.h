//
//  HomeScreenCell.h
//  DemoPetLox
//
//  Created by Chagan Singh on 14/09/15.
//  Copyright (c) 2015 Chagan Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeScreenCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgCellBG;
@property (weak, nonatomic) IBOutlet UIImageView *imgCellProfile;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@end
