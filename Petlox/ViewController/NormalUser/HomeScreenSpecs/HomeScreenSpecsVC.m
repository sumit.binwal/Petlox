//
//  HomeScreenSpecsVC.m
//  DemoPetLox
//
//  Created by Chagan Singh on 14/09/15.
//  Copyright (c) 2015 Chagan Singh. All rights reserved.
//


// screen size
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_4 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 480.0)
#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE_6PLUS (IS_IPHONE && [[UIScreen mainScreen] nativeScale] == 3.0f)
#define IS_IPHONE_6_PLUS (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0)

#import "IIViewDeckController.h"
#import "HomeScreenSpecsVC.h"
#import "PetGrommingVC.h"
#import "ConnectionManager.h"
#import "ServiceProviderVC.h"
#import "MapVC.h"
@interface HomeScreenSpecsVC ()<UIGestureRecognizerDelegate>
{
    
    IBOutlet UILabel *lblNoResultFound;
    IBOutlet UIButton *leftBtn;
    NSString *businessCategory;
}
@end

@implementation HomeScreenSpecsVC

@synthesize fromSwipe;
#pragma mark - Life Cycle Method
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
      [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [self deselectAll];
    
    [txtSearchField setValue:[UIColor colorWithRed:133.0f/255.0f green:133.0f/255.0f blue:133.0f/255.0f alpha:1]
                   forKeyPath:@"_placeholderLabel.textColor"];

    
    [collectionVew registerClass:[HomeCollectionCell class] forCellWithReuseIdentifier:@"HomeCollectionCell"];
    UINib *cellNib = [UINib nibWithNibName:@"HomeCollectionCell" bundle:nil];
    [collectionVew registerNib:cellNib forCellWithReuseIdentifier:@"HomeCollectionCell"];
    [leftBtn addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    

    arrCollectionRecords=[[NSMutableArray alloc]init];
    if ([fromSwipe isEqualToString:@"yes"])
    {
        arrCollectionRecords=[[NSMutableArray alloc]init];
        
        //For Reteriving
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"collectionViewDataImage"];
         [arrCollectionRecords addObjectsFromArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
        collectionVew.contentSize=[self collectionViewContentSize];
        [collectionVew reloadData];
    }
    else
    {

   // [CommonFunctions showActivityIndicatorWithText:@""];
    }

     tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyBoard)];

      [self dummyDataSetup];
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureClicked)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];
    
}
-(void)tapGestureClicked
                                        {
                                            [self.view endEditing:YES];
                                        }
-(void)viewWillAppear:(BOOL)animated
{
    [self.btnNearby setEnabled:NO];
    [self.btnNearby setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    self.navigationController.navigationBarHidden=YES;
    txtSearchField.text=@"";
    [self updateuserDataWithnewLocation];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.btnNext.layer.cornerRadius = self.btnNext.frame.size.width/2;
}


-(void)hideKeyBoard
{
    [self.view endEditing:YES];
    [tblMenuContents removeGestureRecognizer:tap];
    [txtSearchField resignFirstResponder];
}

-(void)updateuserDataWithnewLocation
{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [APPDELEGATE UpdateUserDataWithCompletetion:^(NSArray *arrayOfImage)
             {
                [CommonFunctions removeActivityIndicator];
                  arrCollectionRecords=[[NSMutableArray alloc]init];
                 [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                // NSLog(@"%@",data);
                 arrCollectionRecords=[[NSMutableArray alloc]init];

                 NSLog(@"%@",arrCollectionRecords);
                 [arrCollectionRecords addObjectsFromArray:arrayOfImage];
                 
                 collectionVew.contentSize=[self collectionViewContentSize];
                 [collectionVew reloadData];
                 
                 
                 //For Saving
                 NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:arrCollectionRecords];
                 [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"collectionViewDataImage"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 
                 
                 if (collectionIndexSelected)
                 {
                     
                   [collectionVew scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:collectionIndexSelected inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:NO];
                 }
                 [self.btnNearby setEnabled:YES];
                 [self.btnNearby setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
             } WithFailure:^(NSString *error)
             {
                 NSLog(@"%@",error);
                 [CommonFunctions removeActivityIndicator];
                 [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
             }];
            
        });
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//Dummy Data
-(void)dummyDataSetup
{
    arrGroomers=[[NSMutableArray alloc]init];
    arrAllrecords=[[NSMutableArray alloc]init];
    [arrAllrecords addObject:@{@"name":@"Grooming",@"imageBG":@"grooming_bg",@"profileImg":@"grooming-icon",@"keyword":@"Grooming"}];
    [arrAllrecords addObject:@{@"name":@"Training",@"imageBG":@"Training_bg",@"profileImg":@"Training-icon",@"keyword":@"Training"}];
    [arrAllrecords addObject:@{@"name":@"Walking",@"imageBG":@"walking_bg",@"profileImg":@"walking-icon",@"keyword":@"Walking"}];
    [arrAllrecords addObject:@{@"name":@"Sitting",@"imageBG":@"Sitting_bg",@"profileImg":@"Sitting-icon",@"keyword":@"Sitting"}];
    
    arrGroomers=arrAllrecords;
    
    [tblMenuContents reloadData];
    
}



#pragma mark- TableViewDelegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrGroomers.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeScreenCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HomeScreenCell"];
    
    if (cell==nil)
    {
        cell=[[HomeScreenCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HomeScreenCell"];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(HomeScreenCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    cell.lblName.text=[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"name"];
    cell.imgCellBG.image=[UIImage imageNamed:[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"imageBG"]];
    cell.imgCellProfile.image=[UIImage imageNamed:[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"profileImg"]];
    [cell.btnNext addTarget:self action:@selector(pushToNextViewWithIndex:) forControlEvents:UIControlEventTouchUpInside];

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self hideKeyBoard];
    PetGrommingVC *petVC=[[PetGrommingVC alloc]initWithNibName:@"PetGrommingVC" bundle:nil];

    petVC.strBusinessCategory=[NSString stringWithFormat:@"%@",[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"keyword"]];

    petVC.strSearchCategoryType=@"0";
    [self.navigationController pushViewController:petVC animated:YES];
}


-(void)pushToNextViewWithIndex:(UIButton*)indexPath
{
    [self hideKeyBoard];
    PetGrommingVC *petVC=[[PetGrommingVC alloc]initWithNibName:@"PetGrommingVC" bundle:nil];
    petVC.strKeyword=[NSString stringWithFormat:@"%@",[[arrGroomers objectAtIndex:indexPath.tag] objectForKey:@"keyword"]];
    petVC.strSearchCategoryType=@"0";
    
    [self.navigationController pushViewController:petVC animated:YES];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPHONE_6)
    {
        return 80;
    }
    else if (IS_IPHONE_6_PLUS)
    {
        return 90;
    }
    else if (IS_IPHONE_4)
    {
        return 60;
    }
    else
    {
        return 65;
    }
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{

    if (arrCollectionRecords.count==0)
    {
        return 1;
    }
    else
    {
         return arrCollectionRecords.count;
    }
   
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectioView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
        HomeCollectionCell   *cell = [collectioView dequeueReusableCellWithReuseIdentifier:@"HomeCollectionCell" forIndexPath:indexPath];
    
    //image of person showing
    
    float circulerValue=19;
    if (IS_IPHONE_6)
    {
        circulerValue=25;
    }
    else if (IS_IPHONE_6_PLUS)
    {
        circulerValue=29;
    }
    cell.imgProfilePic.layer.cornerRadius=circulerValue;
    cell.imgProfilePic.layer.borderWidth=2;
    cell.imgProfilePic.layer.borderColor=[UIColor clearColor].CGColor;
    cell.imgProfilePic.layer.masksToBounds=YES;
    
    if (arrCollectionRecords.count!=0)
    {
                [lblNoResultFound setHidden:YES];
    NSString *firstName=[[arrCollectionRecords objectAtIndex:indexPath.row] objectForKey:@"business_name"];
    
    cell.lblName.text=[NSString stringWithFormat:@"%@",[firstName capitalizedString]];
    if ([[arrCollectionRecords objectAtIndex:indexPath.row] objectForKey:@"review_count"]!=[NSNull null])
    {
        int reviewCount=[[[arrCollectionRecords objectAtIndex:indexPath.row] objectForKey:@"review_count"] intValue];
        
        if (reviewCount>1) {
            cell.lblNoReviews.text=[NSString stringWithFormat:@"%@ reviews",[[arrCollectionRecords objectAtIndex:indexPath.row] objectForKey:@"review_count"]];
            }
        else
        {
            cell.lblNoReviews.text=[NSString stringWithFormat:@"%@ review",[[arrCollectionRecords objectAtIndex:indexPath.row] objectForKey:@"review_count"]];
        }
        
    }

    
    NSString *imgProfileStr=[[arrCollectionRecords objectAtIndex:indexPath.row] objectForKey:@"image"];
    if (imgProfileStr.length>1) {
        [cell.imgProfilePic sd_setImageWithURL:[NSURL URLWithString:[[arrCollectionRecords objectAtIndex:indexPath.row] objectForKey:@"image"]]
                              placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    }
    else
    {
        [cell.imgProfilePic setImage:[UIImage imageNamed:@"DefaultProfileImg"]];
    }
    
    
    //add backgroundImage  defaultCoverImg
    
    NSString *imgStr=[[arrCollectionRecords objectAtIndex:indexPath.row] objectForKey:@"extraImage"];
    if (imgStr.length>1) {
        
        [cell.bgImagePic setImageWithURL:[NSURL URLWithString:[[arrCollectionRecords objectAtIndex:indexPath.row] objectForKey:@"extraImage"]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    else
    {
        [cell.bgImagePic setImage:[UIImage imageNamed:@"defaultCoverImg"]];
    }
    
    
    //distane showing
    if ([[arrCollectionRecords objectAtIndex:indexPath.row] objectForKey:@"distance"] !=[NSNull null])
    {
        NSString *strDistance=[[arrCollectionRecords objectAtIndex:indexPath.row] objectForKey:@"distance"];
        if (strDistance.length>0)
        {
            cell.lblDistance.text=[NSString stringWithFormat:@"%@",strDistance];
        }
    }
    
    // stars showing
    NSString *strDatNull=[NSString stringWithFormat:@"%@",[[arrCollectionRecords objectAtIndex:indexPath.row] objectForKey:@"avg_rate"]];
    [self setCell:cell HighlightedWithStarCount:strDatNull];
    }
    else
    {
        [lblNoResultFound setHidden:NO];
    }
    return cell;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (arrCollectionRecords.count!=0)
    {
        HomeCollectionCell *cell = (HomeCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
        NSString *strDatNull=[NSString stringWithFormat:@"%@",[[arrCollectionRecords objectAtIndex:indexPath.row] objectForKey:@"avg_rate"]];
        [self setCell:cell HighlightedWithStarCount:strDatNull];
        
        collectionIndexSelected=indexPath.row;
        ServiceProviderVC *service=[[ServiceProviderVC alloc]initWithNibName:@"ServiceProviderVC" bundle:nil];
        
        service.dictUserDdetails=(NSMutableDictionary*)[arrCollectionRecords objectAtIndex:indexPath.row];
        
        [self.navigationController pushViewController:service animated:YES];
        
    }
}

-(void)setCell:(HomeCollectionCell*)cell HighlightedWithStarCount:(NSString*)strDatNull
{
    if ([strDatNull isEqualToString:@"<null>"])
    {
        NSLog(@"Getting null here");
        for (int i=0; i<5; i++)
        {
            [[cell.stars objectAtIndex:i] setHighlighted:NO];
        }
    }
    else
    {
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *myNumber = [f numberFromString:strDatNull];
        
        int starCount=[myNumber integerValue];
        for (int i=0; i<starCount; i++)
        {
            [[cell.stars objectAtIndex:i] setHighlighted:YES];
        }
        starCount=0;
    }

}


#pragma mark - UICollectionViewDelegateLeftAlignedLayout
- (CGSize)collectionViewContentSize
{
    // NSInteger itemCount = [collectionnView numberOfItemsInSection:0];
    NSInteger pages = ceil(arrAllrecords.count / 1.0);
    return CGSizeMake(collectionVew.frame.size.width * pages, collectionVew.frame.size.height);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPHONE_6)
    {
        return CGSizeMake(375,292);
    }
    else if (IS_IPHONE_6_PLUS)
    {
        return CGSizeMake(414,316);
    }
    else
    {
        return CGSizeMake(320,244);
    }
    
}

#pragma mark- Textfield delegates
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [tblMenuContents addGestureRecognizer:tap];
    return YES;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [tblMenuContents removeGestureRecognizer:tap];
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)searchText
{
    NSString *strTxtSearch = [searchText.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    
    if (strTxtSearch.length>0)
    {
        PetGrommingVC *petVC=[[PetGrommingVC alloc]initWithNibName:@"PetGrommingVC" bundle:nil];
        petVC.strKeyword=searchText.text;
        petVC.strSearchCategoryType=@"1";
        petVC.strBusinessCategory=@"";

        [self.navigationController pushViewController:petVC animated:YES];
        [self hideKeyBoard];
    }
    else
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:K_APP_NAME message:@"Please enter keyword for your search." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        
        [alert show];
        
        [self hideKeyBoard];
    }

    return YES;

}

- (IBAction)btnNearByPressed:(id)sender {
    MapVC *mapVC=[[MapVC alloc]init];
    mapVC.arrayUserData=arrCollectionRecords;
    [self.navigationController pushViewController:mapVC animated:YES];
}

- (IBAction)txtSearchChanged:(UITextField *)searchText
{
    
    [txtSearchField becomeFirstResponder];
}

- (IBAction)btnMenuPressed:(id)sender
{
    IIViewDeckController *ivdck=[[IIViewDeckController alloc]init];
    [ivdck toggleLeftView];
    
}

- (IBAction)btnGroomingPressed:(id)sender {
    [self selectGrooming];
}

- (IBAction)btnWalkingPressed:(id)sender {
    [self selectWalking];
}

- (IBAction)btnSittingPressed:(id)sender {
    [self selectSitting];
}

- (IBAction)btnTrainingPressed:(id)sender {
    [self selectTraining];
}

-(void)deselectAll{
    [self deselectGrooming];
    [self deselectWalking];
    [self deselectSitting];
    [self deselectTraining];
}

-(void)selectGrooming{
    self.imgGrooming.image = [UIImage imageNamed:@"groomingIconbgActive"];
    businessCategory = @"Grooming";
    [self deselectWalking];
    [self deselectSitting];
    [self deselectTraining];
}

-(void)deselectGrooming{
    self.imgGrooming.image = [UIImage imageNamed:@"groomingIconbg"];
}

-(void)selectWalking{
    self.imgWalking.image = [UIImage imageNamed:@"walkingIconActive"];
    businessCategory = @"Walking";
    [self deselectGrooming];
    [self deselectSitting];
    [self deselectTraining];
}

-(void)deselectWalking{
    self.imgWalking.image = [UIImage imageNamed:@"walkingIcon"];
}

-(void)selectSitting{
    self.imgSitting.image = [UIImage imageNamed:@"sittingIconActive"];
    businessCategory = @"Sitting";
    [self deselectGrooming];
    [self deselectWalking];
    [self deselectTraining];
}

-(void)deselectSitting{
    self.imgSitting.image = [UIImage imageNamed:@"sittingIcon"];
}

-(void)selectTraining{
    self.imgTraining.image = [UIImage imageNamed:@"trainingIconActive"];
    businessCategory = @"Training";
    [self deselectGrooming];
    [self deselectWalking];
    [self deselectSitting];
}

-(void)deselectTraining{
    self.imgTraining.image = [UIImage imageNamed:@"trainingIcon"];
}



- (IBAction)btnNextPressed:(id)sender {
    
    if(businessCategory){
        [self hideKeyBoard];
        PetGrommingVC *petVC=[[PetGrommingVC alloc]initWithNibName:@"PetGrommingVC" bundle:nil];
        petVC.strBusinessCategory=businessCategory;
        petVC.strSearchCategoryType=@"0";
        [self.navigationController pushViewController:petVC animated:YES];
    } else {
        [CommonFunctions alertTitle:@"" withMessage:@"Please select a category first"];
    }
    
}
@end
