//
//  MapVC.m
//  Petlox
//
//  Created by Sumit Sharma on 27/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "MapVC.h"
#import "MyReservationVC.h"
#import <GoogleMaps/GoogleMaps.h>
@interface MapVC ()<GMSMapViewDelegate,GMSIndoorDisplayDelegate,GMSPanoramaViewDelegate,GMSTileReceiver>
{
    
    IBOutlet UIView *vwBusinessNameHeader;
    IBOutlet UIView *vwBusinessAddressHeader;
    IBOutlet GMSMapView *mapView;
    IBOutlet UILabel *lblBusinessAddress;
    IBOutlet UILabel *lblBusinessName;
    float lattitude;
    float longitutde;
    IBOutlet UIButton *btnDirection;
}

@end

@implementation MapVC
@synthesize dictUserData, arrayUserData;
- (void)viewDidLoad {
    [super viewDidLoad];
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Map View Screen"];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
        [self.navigationItem setLeftBarButtonItem:btn];
    
    vwBusinessAddressHeader.layer.cornerRadius=5.0f;
    vwBusinessNameHeader.layer.cornerRadius=5.0f;
    
    mapView.mapType = kGMSTypeNormal;
    mapView.delegate = self;
    if(arrayUserData.count > 0){
        vwBusinessNameHeader.hidden = YES;
        vwBusinessAddressHeader.hidden = YES;
        btnDirection.hidden=YES;
        [self mapWithMultipleLocations];
    } else {
        lblBusinessName.text=[dictUserData valueForKey:@"business_name"];
        lblBusinessAddress.text=[dictUserData valueForKey:@"address"];
        [self mapWithFrameFortLocation];
    }

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [APPDELEGATE showPetReservationTabBar:NO];
}

-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnDirectionCliced:(id)sender {
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]]) {
        NSString *strDirection=[NSString stringWithFormat:@"comgooglemaps://?center=%@,%@&zoom=14&views=traffic",[dictUserData valueForKey:@"lat"],[dictUserData valueForKey:@"lon"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strDirection]];
    } else {
        NSLog(@"Can't use comgooglemaps://");
        //[CommonFunctions alertTitle:@"Petlox" withMessage:@"Not Found Google Maps"];
        
        NSString *strWebUrl=[NSString stringWithFormat:@"https://maps.google.com/?q=@%@,%@",[dictUserData valueForKey:@"lat"],[dictUserData valueForKey:@"lon"]];
        NSURL *url = [NSURL URLWithString:strWebUrl];
        
        if (![[UIApplication sharedApplication] openURL:url]) {
            NSLog(@"%@%@",@"Failed to open url:",[url description]);
        }
    }
    


}

-(void)mapWithMultipleLocations
{
    lattitude=[[[arrayUserData objectAtIndex:0] valueForKey:@"lat"] doubleValue];
    longitutde=[[[arrayUserData objectAtIndex:0] valueForKey:@"lon"] doubleValue];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lattitude
                                                            longitude:longitutde
                                                                 zoom:16];
    mapView.camera=camera;
    
    for(int i=0; i< arrayUserData.count; i++){
        
        float lattitude0=[[[arrayUserData objectAtIndex:i] valueForKey:@"lat"] doubleValue];
        float longitutde0=[[[arrayUserData objectAtIndex:i] valueForKey:@"lon"] doubleValue];
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(lattitude0, longitutde0);
        marker.title = [NSString stringWithFormat:@"%@",[[arrayUserData objectAtIndex:i] valueForKey:@"business_name"]];
        marker.snippet = [NSString stringWithFormat:@"%@",[[arrayUserData objectAtIndex:i] valueForKey:@"address"]];
        marker.icon=[UIImage imageNamed:@"aaa.png"] ;
        marker.map = mapView;
    }   
}

-(void)mapWithFrameFortLocation
{
    lattitude=[[dictUserData valueForKey:@"lat"] floatValue];
    longitutde=[[dictUserData valueForKey:@"lon"] floatValue];

    NSArray *viewControllers = self.navigationController.viewControllers;
    UIViewController *rootViewController = (UIViewController *)[viewControllers objectAtIndex:0];
    
    NSLog(@"%@",viewControllers);
    

    
    if ([rootViewController isKindOfClass:[MyReservationVC class]]) {
        GMSCameraPosition *cameraPosition=[GMSCameraPosition cameraWithLatitude:lattitude longitude:longitutde zoom:9];
        mapView =[GMSMapView mapWithFrame:CGRectZero camera:cameraPosition];
        
        
        mapView.myLocationEnabled=YES;
        GMSMarker *marker=[[GMSMarker alloc]init];
        
        marker.position=CLLocationCoordinate2DMake(lattitude, longitutde);

        marker.icon=[UIImage imageNamed:@"aaa.png"] ;
        marker.groundAnchor=CGPointMake(0.5,0.5);
        marker.map=mapView;

        GMSMarker *marker1=[[GMSMarker alloc]init];
        marker1.position=CLLocationCoordinate2DMake(CurrentLatitude, CurrentLongitude);
        marker1.icon=[UIImage imageNamed:@"aaa.png"] ;
        marker1.groundAnchor=CGPointMake(0.5,0.5);
        marker1.map=mapView;

        
        self.view=mapView;
        CLLocation *LocationAtual = [[CLLocation alloc] initWithLatitude:CurrentLatitude longitude:CurrentLongitude];
        CLLocation *LocationAtual1 = [[CLLocation alloc] initWithLatitude:lattitude longitude:longitutde];
        
        
        [self callGoogleServiceToGetRouteDataFromSource:LocationAtual toDestination:LocationAtual1 onMap:mapView];

    }
    else
    {
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lattitude
                                     
                                                                longitude:longitutde
                                                                     zoom:16];
        mapView.camera=camera;
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(lattitude, longitutde);
        marker.title = [NSString stringWithFormat:@"%@",[dictUserData valueForKey:@"business_name"]];
        marker.snippet = [NSString stringWithFormat:@"%@",[dictUserData valueForKey:@"address"]];
        
        marker.map = mapView;
    }
    
    
    
   
}


- (void)callGoogleServiceToGetRouteDataFromSource:(CLLocation *)sourceLocation toDestination:(CLLocation *)destinationLocation onMap:(GMSMapView *)mapView_{
    

    NSString *baseUrl = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false", CurrentLatitude,CurrentLongitude,lattitude,longitutde];
    
    NSURL *url = [NSURL URLWithString:[baseUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"Url: %@", url);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        GMSMutablePath *path = [GMSMutablePath path];
        
        NSError *error = nil;
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        
        NSArray *routes = [result objectForKey:@"routes"];
        
        NSDictionary *firstRoute;
        if (routes.count > 0)
        {
            firstRoute = [routes objectAtIndex:0];
        }
        
        NSDictionary *leg;
        
        if ([firstRoute objectForKey:@"legs"])
        {
            leg =  [[firstRoute objectForKey:@"legs"] objectAtIndex:0];
        }
        
        NSArray *steps = [leg objectForKey:@"steps"];
        
        int stepIndex = 0;
        
        CLLocationCoordinate2D stepCoordinates[1  + [steps count] + 1];
        
        for (NSDictionary *step in steps) {
            
            NSDictionary *start_location = [step objectForKey:@"start_location"];
            stepCoordinates[++stepIndex] = [self coordinateWithLocation:start_location];
            [path addCoordinate:[self coordinateWithLocation:start_location]];
            
            NSString *polyLinePoints = [[step objectForKey:@"polyline"] objectForKey:@"points"];
            GMSPath *polyLinePath = [GMSPath pathFromEncodedPath:polyLinePoints];
            for (int p=0; p<polyLinePath.count; p++) {
                [path addCoordinate:[polyLinePath coordinateAtIndex:p]];
            }
            
            
            if ([steps count] == stepIndex){
                NSDictionary *end_location = [step objectForKey:@"end_location"];
                stepCoordinates[++stepIndex] = [self coordinateWithLocation:end_location];
                [path addCoordinate:[self coordinateWithLocation:end_location]];
            }
        }
        
        GMSPolyline *polyline = nil;
        polyline = [GMSPolyline polylineWithPath:path];
        polyline.strokeColor = [UIColor blueColor];
        polyline.strokeWidth = 12.f;
        polyline.map = mapView_;
    }];
}

- (CLLocationCoordinate2D)coordinateWithLocation:(NSDictionary*)location
{
    double latitude = [[location objectForKey:@"lat"] doubleValue];
    double longitude = [[location objectForKey:@"lng"] doubleValue];
    
    return CLLocationCoordinate2DMake(latitude, longitude);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
