//
//  PetGrommingVC.m
//  DemoPetLox
//
//  Created by Chagan Singh on 11/09/15.
//  Copyright (c) 2015 Chagan Singh. All rights reserved.
//

#import "MessageBusinessListVC.h"
#import "PetGromingCell.h"
#import "WriteMsgVC.h"
#import "ConnectionManager.h"
#import "UIImageView+WebCache.h"
#import "ServiceProviderVC.h"
@interface MessageBusinessListVC ()

@end

@implementation MessageBusinessListVC

@synthesize strKeyword,strSearchCategoryType;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    
    [searchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"search-bg"] forState:UIControlStateNormal];

    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    [txtSearchField setValue:[UIColor colorWithRed:133.0f/255.0f green:133.0f/255.0f blue:133.0f/255.0f alpha:1]
                  forKeyPath:@"_placeholderLabel.textColor"];


    [self.navigationItem setHidesBackButton:YES];
    
    UIBarButtonItem *backButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self
action:@selector(barButtonBackPressed)];
    
    self.navigationItem.leftBarButtonItem = backButton;
    self.navigationController.navigationBarHidden=YES;
 
    
//    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 5, txtSearchField.frame.size.height-8, txtSearchField.frame.size.height-8)];
//    imgView.image = [UIImage imageNamed:@"search-icon.png"];
//    
//    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, txtSearchField.frame.size.height+17, txtSearchField.frame.size.height)];
//    [paddingView addSubview:imgView];
//    [txtSearchField setLeftViewMode:UITextFieldViewModeAlways];
//    [txtSearchField setLeftView:paddingView];
    
    
    arrGroomers=[[NSMutableArray alloc]init];
    arrAllrecords=[[NSMutableArray alloc]init];
    
    pageValue=1;
    lastLoad = 0;
    
    
  }


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    arrGroomers=[[NSMutableArray alloc]init];
    arrAllrecords=[[NSMutableArray alloc]init];
        [self.navigationController.navigationBar setHidden:YES];
    pageValue=1;
    lastLoad = 0;
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getResultsForKey:strKeyword andForCategory:strSearchCategoryType withPage:pageValue];
       
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.btnSearch.imageView.contentMode = UIViewContentModeScaleAspectFit;
}

-(void)barButtonBackPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrGroomers.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PetGromingCell *cell=[tableView dequeueReusableCellWithIdentifier:@"PetGromingCell"];
    
    if (cell==nil)
    {
        cell=[[PetGromingCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PetGromingCell"];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(PetGromingCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *firstName=[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"business_name"];

    cell.lblNameGrommer.text=[NSString stringWithFormat:@"%d. %@",(int)indexPath.row+1,[firstName capitalizedString]];
    cell.lblAddress.text=[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"address"];
    
    if ([[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"review_count"]!=[NSNull null])
    {
        int reviewCount=[[[arrGroomers objectAtIndex:indexPath.row]objectForKey:@"review_count"] intValue];
        if (reviewCount>1) {
            cell.lblNoOfreview.text=[NSString stringWithFormat:@"%@ reviews",[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"review_count"]];
        }
        else
        {
            cell.lblNoOfreview.text=[NSString stringWithFormat:@"%@ review",[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"review_count"]];
        }
    }
    
    if ([self isNotNull:[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"fav"]]) {
        NSString *strFav=[NSString stringWithFormat:@"%@",[[arrGroomers objectAtIndex:indexPath.row] valueForKey:@"fav"]];
        if ([strFav isEqualToString:@"1"]) {
            [cell.imgFavourate setImage:[UIImage imageNamed:@"favorite_foucs_icon"]];
        }
        else
        {
            [cell.imgFavourate setImage:[UIImage imageNamed:@"favorite_icon"]];
        }
    }
    else
    {
          //  [cell.imgFavourate setImage:[UIImage imageNamed:@"hart"]];
    }
    
//    cell.imgProfileView.layer.cornerRadius=cell.imgProfileView.frame.size.width/2;
//    cell.imgProfileView.layer.borderWidth=2;
//    cell.imgProfileView.layer.borderColor=[UIColor clearColor].CGColor;
    cell.imgProfileView.layer.masksToBounds=YES;
    
    NSString *imgURL=[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"image"];
    if (imgURL.length>1) {

        [cell.imgProfileView setImageWithURL:[NSURL URLWithString:[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"image"]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
    }
    else
    {
        [cell.imgProfileView setImage:[UIImage imageNamed:@"DefaultProfileImg"]];
    }
    

    if ([[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"distance"] !=[NSNull null])
    {
        NSString *strDistance=[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"distance"];
        if (strDistance.length>0)
        {
            NSString *strMile=[NSString stringWithFormat:@"%@ mi",strDistance];
            strMile=[strMile stringByReplacingOccurrencesOfString:@" mi" withString:@""];
             cell.lblDistance.text=[NSString stringWithFormat:@"%@mi",strMile];
        }
   
    }
    
    
    if ([[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"avg_rate"] !=[NSNull null])
    {
        int stars=[[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"avg_rate"] intValue];
        
        for (int i=0; i<stars; i++)
        {
            [[cell.imsStars objectAtIndex:i] setHighlighted:YES];
        }

    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    WriteMsgVC *service=[[WriteMsgVC alloc]initWithNibName:@"WriteMsgVC" bundle:nil];
    service.dataDict=[arrGroomers objectAtIndex:indexPath.row];
   // service.dictUserDdetails=(NSMutableDictionary*)[arrGroomers objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:service animated:YES];
}


- (IBAction)searchTextChanged:(UITextField *)searchText {
    
    arrGroomers=[[NSMutableArray alloc]init];
    
    NSString *strTxtSearch = [searchText.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    if (strTxtSearch.length>0)
    {
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"self.first_name contains[cd] %@ OR self.last_name contains[cd] %@",strTxtSearch,strTxtSearch];
        arrGroomers = [[arrAllrecords filteredArrayUsingPredicate:resultPredicate] mutableCopy];
        [tblPetGromming reloadData];
    }
    else
    {
        arrGroomers = arrAllrecords;
        [tblPetGromming reloadData];
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
     NSString *strTxtSearch = [txtSearchField.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    arrGroomers=[[NSMutableArray alloc]init];
    if (strTxtSearch.length>0)
    {
        NSString *strTeXtSearch=[NSString stringWithFormat:@"%@ Searching...",strTxtSearch];
        [CommonFunctions showActivityIndicatorWithText:strTeXtSearch];
        [self getResultsForKey:strTxtSearch andForCategory:@"1" withPage:pageValue];
        //        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"self.first_name contains[cd] %@",strTxtSearch];
        //        arrGroomers = [[arrAllrecords filteredArrayUsingPredicate:resultPredicate] mutableCopy];
        //        [tblPetGromming reloadData];
        [textField resignFirstResponder];
    }
    else
    {
        
        arrGroomers = arrAllrecords;
        [tblPetGromming reloadData];
        [textField resignFirstResponder];
    }
    return YES;
}


- (IBAction)btnSearchText:(id)sender {
    NSString *strTxtSearch = [txtSearchField.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    arrGroomers=[[NSMutableArray alloc]init];
    if (strTxtSearch.length>0)
    {
        NSString *strTeXtSearch=[NSString stringWithFormat:@"%@ Searching...",strTxtSearch];
        [CommonFunctions showActivityIndicatorWithText:strTeXtSearch];
        [self getResultsForKey:strTxtSearch andForCategory:@"1" withPage:pageValue];
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"self.first_name contains[cd] %@",strTxtSearch];
        arrGroomers = [[arrAllrecords filteredArrayUsingPredicate:resultPredicate] mutableCopy];
        [tblPetGromming reloadData];
    }
    else
    {
        
        arrGroomers = arrAllrecords;
        [tblPetGromming reloadData];
    }

}

- (IBAction)btnBackPressed:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- WebService integration

-(void)getResultsForKey:(NSString*)strKey andForCategory:(NSString*)strCategory withPage:(int)page
{
    NSString *tokenID=[[NSUserDefaults standardUserDefaults]valueForKey:UD_TOKEN_ID];
    NSMutableDictionary *params=[[NSMutableDictionary alloc]initWithDictionary:@{@"user_id":tokenID,@"lat":[NSString stringWithFormat:@"%f",CurrentLatitude],@"lon":[NSString stringWithFormat:@"%f",CurrentLongitude],@"reservation_type":@"1"}];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    
    [[ConnectionManager sharedInstance]startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:@"userbusinesslisting" withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
    {
         [CommonFunctions removeActivityIndicator];
        [actiVItyLoader stopAnimating];
        actiVItyLoader.hidden=YES;
        txtSearchField.text=@"";
        [txtSearchField resignFirstResponder];
        NSDictionary* result = [NSJSONSerialization JSONObjectWithData:responseObject
                                                               options:kNilOptions
                                                                 error:nil];
        NSLog(@"%@",result);
        
        if ([[result valueForKey:@"replyCode"] isEqualToString:@"error"])
        {
            lblNoResultsFound.hidden=NO;
            
            tblPetGromming.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
           
            arrAllrecords=[[NSMutableArray alloc]init];

            [arrGroomers addObjectsFromArray:arrAllrecords];
            [tblPetGromming reloadData];
        }
        else
        {
        tblPetGromming.tableFooterView=footerView;
        
        arrAllrecords=[[NSMutableArray alloc]init];
        arrAllrecords=[[result valueForKey:@"Reservation"] mutableCopy];
        
        NSLog(@"%d",arrAllrecords.count);
        
        
        int totalRecords=[[result valueForKey:@"total_records"] intValue];
            if (arrAllrecords.count==0)
            {
                 lblNoResultsFound.hidden=NO;
            }
            else
            {
                 lblNoResultsFound.hidden=YES;
            }
            
        [arrGroomers addObjectsFromArray:arrAllrecords];
        [tblPetGromming reloadData];
        
        if (totalRecords<21)
        {
            
            tblPetGromming.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
            return ;
        }
        
        if (lastLoad == 0)
        {
           
            
            int nextLoadCount = (page+1) * 20;
            
            NSString *strTesults=@"Results";
            
            float records=totalRecords%20;
            
            
            if (records==1)
            {
                strTesults=@"Result";
            }
            else
            {
                strTesults=@"Results";
            }
            
            if (totalRecords > nextLoadCount)
            {
                [btnFooter setTitle:[NSString stringWithFormat:@"Next %d %@",20,strTesults] forState:UIControlStateNormal];
            }
            else{
                lastLoad = 1;
                [btnFooter setTitle:[NSString stringWithFormat:@"Next %d %@",totalRecords%20,strTesults] forState:UIControlStateNormal];
            }
        }
        else{
             actiVItyLoader.hidden=YES;
            tblPetGromming.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        }
        }
        
        
        

    } withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [actiVItyLoader stopAnimating];
          actiVItyLoader.hidden=YES;
        [CommonFunctions removeActivityIndicator];

    }];
}

- (IBAction)btnFooterPressed:(id)sender {
    
    [actiVItyLoader startAnimating];
    actiVItyLoader.hidden=NO;
    pageValue+=1;
    [self getResultsForKey:strKeyword andForCategory:strSearchCategoryType withPage:pageValue];
}
@end
