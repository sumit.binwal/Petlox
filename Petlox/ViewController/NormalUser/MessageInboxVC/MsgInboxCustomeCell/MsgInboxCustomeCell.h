//
//  MsgInboxCustomeCell.h
//  Petlox
//
//  Created by Sumit Sharma on 15/12/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MsgInboxCustomeCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgVwProfileImg;
@property (strong, nonatomic) IBOutlet UILabel *lblBusinessName;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UILabel *lblReviewDiscription;

@end
