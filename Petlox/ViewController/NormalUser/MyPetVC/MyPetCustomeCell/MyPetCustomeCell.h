//
//  MyPetCustomeCell.h
//  Petlox
//
//  Created by Sumit Sharma on 11/12/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPetCustomeCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgVwPetImg;
@property (strong, nonatomic) IBOutlet UILabel *lblPetName;
@property (strong, nonatomic) IBOutlet UILabel *lblPetWeight;
@property (strong, nonatomic) IBOutlet UILabel *lblPetAge;
@property (strong, nonatomic) IBOutlet UILabel *lblPetBreed;

@end
