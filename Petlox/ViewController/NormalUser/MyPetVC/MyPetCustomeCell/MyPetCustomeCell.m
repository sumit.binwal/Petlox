//
//  MyPetCustomeCell.m
//  Petlox
//
//  Created by Sumit Sharma on 11/12/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "MyPetCustomeCell.h"

@implementation MyPetCustomeCell

- (void)awakeFromNib {
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    return self=[[[NSBundle mainBundle]loadNibNamed:@"MyPetCustomeCell" owner:self options:nil]objectAtIndex:0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
