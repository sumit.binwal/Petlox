//
//  MyPetVC.m
//  Petlox
//
//  Created by Sumit Sharma on 11/12/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "MyPetVC.h"
#import "MyPetCustomeCell.h"
#import "PetDetailVC.h"
#import "ChoosePetVC.h"
@interface MyPetVC ()<UITableViewDataSource,UITableViewDelegate>
{
    
    IBOutlet UITableView *tblVw;
    NSMutableArray *arrPets;
}
@end

@implementation MyPetVC

-(AppDelegate *)appDelegateObj
{
    return (AppDelegate *) [[UIApplication sharedApplication]delegate];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    wbServiceCount=1;
    // Do any additional setup after loading the view from its nib.
    [self setUpView];

}

-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    
    
    UIBarButtonItem *backButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStylePlain target:self.viewDeckController action:@selector(toggleLeftView)];
    [self.navigationItem setLeftBarButtonItem:backButton];
    
    UIBarButtonItem *rightBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"addPetIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(rightBarBtnClicked)];
    [self.navigationItem setRightBarButtonItem:rightBtn];

    [self.navigationItem setTitle:@"My Pets"];
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
            [self getPetDetail];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)rightBarBtnClicked
{
    ChoosePetVC *cpvc=[[ChoosePetVC alloc]init];
    [self.navigationController pushViewController:cpvc animated:YES];
}

#pragma mark - UITableView Delegate Method
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrPets.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    return 85.0f;
    return (85.0 * [self appDelegateObj].window.bounds.size.height)/568;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"MyPetCustomeCell";
    
    MyPetCustomeCell *cell=[[MyPetCustomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
//    cell.imgVwPetImg.layer.cornerRadius=cell.imgVwPetImg.frame.size.height/2;
    cell.imgVwPetImg.clipsToBounds=YES;
    
//    cell.imgVwPetImg.layer.borderColor=[UIColor whiteColor].CGColor;
//    cell.imgVwPetImg.layer.borderWidth=1.0f;
    

    [cell.imgVwPetImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[arrPets objectAtIndex:indexPath.row] objectForKey:@"image"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    cell.lblPetName.text=[[arrPets objectAtIndex:indexPath.row] objectForKey:@"name"];
        cell.lblPetBreed.text=[NSString stringWithFormat:@"Breed : %@",[[arrPets objectAtIndex:indexPath.row] objectForKey:@"breed"]];
        cell.lblPetWeight.text=[NSString stringWithFormat:@"Weight : %@ lbs",[[arrPets objectAtIndex:indexPath.row] objectForKey:@"weight"]];
        cell.lblPetAge.text=[NSString stringWithFormat:@"Age : %@ Years",[[arrPets objectAtIndex:indexPath.row] objectForKey:@"age"]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PetDetailVC *pdvc=[[PetDetailVC alloc]init];
    pdvc.dictPetDetail=[arrPets objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:pdvc animated:YES];
}

#pragma mark- WebServices API..

-(void)getPetDetail
{
    NSMutableDictionary *param;
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"user_id", nil];
    
    NSString *url = [NSString stringWithFormat:@"mypetList"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/userpetList
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            arrPets=[responseDict objectForKey:@"data"];
            [tblVw reloadData];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self getPetDetail];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}


@end
