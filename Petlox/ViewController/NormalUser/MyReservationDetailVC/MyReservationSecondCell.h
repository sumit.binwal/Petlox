//
//  MyReservationSecondCell.h
//  Petlox
//
//  Created by Suchita Bohra on 21/07/16.
//  Copyright © 2016 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyReservationSecondCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *lblTimeRemainingTitle;
@property (nonatomic, strong) IBOutlet UILabel *lblTimeRemainingValue;
@property (nonatomic, strong) IBOutlet UILabel *lblDistanceTitle;
@property (nonatomic, strong) IBOutlet UILabel *lblDistanceValue;

@end
