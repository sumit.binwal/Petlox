//
//  MyReservationCustomeCell.m
//  Petlox
//
//  Created by Sumit Sharma on 07/12/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "MyReservationCustomeCell.h"

@implementation MyReservationCustomeCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    return self=[[[NSBundle mainBundle]loadNibNamed:@"MyReservationCustomeCell" owner:self options:nil]objectAtIndex:0];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
