//
//  NotificationCell.h
//  Petlox
//
//  Created by Suchita Bohra on 21/07/16.
//  Copyright © 2016 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell

@property (nonatomic, strong)IBOutlet UILabel *lblTitle;
@property (nonatomic, strong)IBOutlet UILabel *lblDesc;
@property (nonatomic, strong)IBOutlet UISwitch *switchToggle;

@end
