//
//  NotificationVC.m
//  Petlox
//
//  Created by Sumit Sharma on 16/12/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "NotificationVC.h"
#import "NotificationCell.h"

@interface NotificationVC ()<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UISwitch *switchReservation;
    IBOutlet UISwitch *switchMsg;
    int intMsgStatus;
    int intReservationStatus;
    IBOutlet UILabel *lblMessage;
    IBOutlet UILabel *lblReservation;
    
    IBOutlet UITableView *tblViewNotification;
}
@end

@implementation NotificationVC

-(AppDelegate *)appDelegateObj
{
    return (AppDelegate *) [[UIApplication sharedApplication]delegate];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    [self.navigationItem setTitle:@"Notifications"];
    wbServiceCount=1;
    
//    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:UD_BUSINESS_TYPE]);
//    if(![[[NSUserDefaults standardUserDefaults] objectForKey:UD_BUSINESS_TYPE]isEqualToString:@"user"])
//    {
//        lblReservation.text=@"Get notified when you receive reservation.";
//    }
  
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
    [self checkNotificationSetting];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }

}

-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)switchMessageBtnClicked:(id)sender
{
    UISwitch *switchDemo = (UISwitch *)sender;
    intMsgStatus = switchDemo.isOn;
    [self updateNotificationSetting];
}

- (IBAction)switchReservationBtnClicked:(id)sender
{
    UISwitch *switchDemo = (UISwitch *)sender;
    intReservationStatus = switchDemo.isOn;
    [self updateNotificationSetting];
}

#pragma mark - WebService API
-(void)updateNotificationSetting
{
    NSLog(@"%d",intReservationStatus);
        NSLog(@"%d",intMsgStatus);
    NSMutableDictionary *param;
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"user_id",[NSString stringWithFormat:@"%d",intMsgStatus],@"message",[NSString stringWithFormat:@"%d",intReservationStatus],@"reservation", nil];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    NSString *url = [NSString stringWithFormat:@"pushOnOff"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/userReview
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
//        [activityIndicator stopAnimating];
//        [btnSend setHidden:NO];
//        [activityIndicator setHidden:YES];
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            // arrReviewData=[responseDict objectForKey:@"data"];
            //  [reviewTbleView reloadData];
            [tblViewNotification reloadData];
        }
        else if([operation.response statusCode]  == 206 ){
            NSLog(@"impo response %@",operation.response);
            // lblMsgError.text=@"No review found";
            
        }
        
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self updateNotificationSetting];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}

-(void)checkNotificationSetting
{
    NSMutableDictionary *param;
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"user_id", nil];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];

    NSString *url = [NSString stringWithFormat:@"checkPushOnOff"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/userReview
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        //        [activityIndicator stopAnimating];
        //        [btnSend setHidden:NO];
        //        [activityIndicator setHidden:YES];
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            intMsgStatus=[[responseDict objectForKey:@"message_status"]intValue];
            intReservationStatus=[[responseDict objectForKey:@"reservation_status"]intValue];
            
//            if (intReservationStatus ==0) {
//                [switchReservation setOn:NO];
//            }
//            else
//            {
//                [switchReservation setOn:YES];
//            }
//            if (intMsgStatus==0) {
//                [switchMsg setOn:NO];
//            }
//            else
//            {
//                [switchMsg setOn:YES];
//            }
            [tblViewNotification reloadData];
            
        }
        else if([operation.response statusCode]  == 206 ){
            NSLog(@"impo response %@",operation.response);
            // lblMsgError.text=@"No review found";
            
        }
        
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self checkNotificationSetting];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - UITableViewDelegate / DataSource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float rowHeight = 0.0f;
    rowHeight = (85 * [self appDelegateObj].window.bounds.size.height)/568;
    return rowHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 2;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *notificationCell   = @"NotificationCell";
    
    NotificationCell *cell = (NotificationCell *)[tableView dequeueReusableCellWithIdentifier:notificationCell];
    
    if (cell == nil)
    {
        cell = (NotificationCell*)[[[NSBundle mainBundle] loadNibNamed:@"NotificationCell" owner:self options:nil] objectAtIndex:0];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    if (indexPath.row == 0)
    {
        cell.lblTitle.text = @"Messages";
        cell.lblDesc.attributedText = [[NSAttributedString alloc]initWithString:@"Get notified when someone \nsends you a message"];
        
//        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//        paragraphStyle.lineSpacing = -5.0;
//        paragraphStyle.alignment = NSTextAlignmentLeft;
//        NSDictionary *attributes = @{NSParagraphStyleAttributeName: paragraphStyle};
//        NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:@"Get notified when someone \nsends you a message"
//                                                                             attributes:attributes];
//        cell.lblDesc.attributedText = attributedText;
        
        [cell.switchToggle addTarget:self action:@selector(switchMessageBtnClicked:) forControlEvents:UIControlEventValueChanged];
        
        if (intMsgStatus == 0) {
            [cell.switchToggle setOn:NO];
        }
        else
        {
            [cell.switchToggle setOn:YES];
        }
    }
    else if (indexPath.row == 1)
    {
        cell.lblTitle.text = @"Privacy settings";
        
        NSString *strText = @"";
        if(![[[NSUserDefaults standardUserDefaults] objectForKey:UD_BUSINESS_TYPE]isEqualToString:@"user"])
        {
            strText = @"Get notified when you receive \nreservation.";
        }
        else
        {
            strText = @"Get notified when reservation \nis confirmed";
        }
        
//        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//        paragraphStyle.lineSpacing = 0.0;
//        paragraphStyle.alignment = NSTextAlignmentLeft;
//        NSDictionary *attributes = @{NSParagraphStyleAttributeName: paragraphStyle};
//        NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:strText
//                                                                             attributes:attributes];
        cell.lblDesc.attributedText = [[NSAttributedString alloc]initWithString:strText];
        
        [cell.switchToggle addTarget:self action:@selector(switchReservationBtnClicked:) forControlEvents:UIControlEventValueChanged];
        
        if (intReservationStatus ==0) {
            [cell.switchToggle setOn:NO];
        }
        else
        {
            [cell.switchToggle setOn:YES];
        }
    }
    
    return cell;
}


@end
