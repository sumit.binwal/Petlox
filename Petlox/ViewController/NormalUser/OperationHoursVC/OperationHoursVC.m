//
//  OperationHoursVC.m
//  Petlox
//
//  Created by Sumit Sharma on 02/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "OperationHoursVC.h"
#import "ChooseHoursCustomeCell.h"
#import "ServiceProviderVC.h"
@interface OperationHoursVC ()
{
    
    IBOutlet UITableView *tblView;
    NSMutableArray *dataHoursArr;
        NSMutableArray *dataHoursArr1;
}
@end

@implementation OperationHoursVC
@synthesize businessUserID;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    wbServiceCount=1;
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [APPDELEGATE showPetReservationTabBar:NO];
}

-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];

    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
    dataHoursArr=[[NSMutableArray alloc]init];
    
    [tblView setScrollEnabled:NO];
    
    [self.navigationItem setTitle:@"Hours of Operation"];
    
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getOperationHours];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }
    
    dataHoursArr=[[NSMutableArray alloc]init];
    if (dataHoursArr.count<1) {
        for (int day=0; day<7; day++) {
            NSMutableDictionary *hoursDict=[[NSMutableDictionary alloc]init];
            switch (day) {
                case 0:
                    [hoursDict setObject:@"Monday" forKey:@"day"];
                    [hoursDict setObject:@"MonIcon" forKey:@"imgName"];
                    [hoursDict setObject:@"1" forKey:@"mode"];
                    break;
                case 1:
                    [hoursDict setObject:@"Tuesday" forKey:@"day"];
                    [hoursDict setObject:@"TueIcon" forKey:@"imgName"];
                    [hoursDict setObject:@"1" forKey:@"mode"];
                    break;
                case 2:
                    [hoursDict setObject:@"Wednesday" forKey:@"day"];
                    [hoursDict setObject:@"WedIcon" forKey:@"imgName"];
                    [hoursDict setObject:@"1" forKey:@"mode"];
                    break;
                case 3:
                    [hoursDict setObject:@"Thursday" forKey:@"day"];
                    [hoursDict setObject:@"ThuIcon" forKey:@"imgName"];
                    [hoursDict setObject:@"1" forKey:@"mode"];
                    break;
                case 4:
                    [hoursDict setObject:@"Friday" forKey:@"day"];
                    [hoursDict setObject:@"FriIcon" forKey:@"imgName"];
                    [hoursDict setObject:@"1" forKey:@"mode"];
                    break;
                case 5:
                    [hoursDict setObject:@"Saturday" forKey:@"day"];
                    [hoursDict setObject:@"SatIcon" forKey:@"imgName"];
                    [hoursDict setObject:@"1" forKey:@"mode"];
                    break;
                case 6:
                    [hoursDict setObject:@"Sunday" forKey:@"day"];
                    [hoursDict setObject:@"SunIcon" forKey:@"imgName"];
                    [hoursDict setObject:@"1" forKey:@"mode"];
                    break;
            }
            [hoursDict setObject:@"11:00am" forKey:@"start"];
            [hoursDict setObject:@"10:30pm" forKey:@"end"];
            
            [dataHoursArr addObject:hoursDict];
        }
        
    }
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)backBarButtonClicked:(id)sender
{
    
//[self.navigationController popViewControllerAnimated:YES];
    
    ServiceProviderVC *serviceProviderVC = (ServiceProviderVC *)[CommonFunctions exists:[ServiceProviderVC class] in:self.navigationController];
    if(serviceProviderVC == nil){
        serviceProviderVC = [[ServiceProviderVC alloc] init];
        [self.navigationController pushViewController:serviceProviderVC animated:YES];
    } else {
        [self.navigationController popToViewController:serviceProviderVC animated:YES];
    }
    
}

#pragma mark - UITableView Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 52.0f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataHoursArr1.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"ChooseHoursCustomeCell";
    ChooseHoursCustomeCell *cell=[[ChooseHoursCustomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }



    if ([[[dataHoursArr1 objectAtIndex:indexPath.row] objectForKey:@"openStatus"]isEqualToString:@"0"]) {
        

        [cell.btnOpenClose setBackgroundImage:[UIImage imageNamed:@"CloseImg"] forState:UIControlStateNormal];
        [cell.btnOpenClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cell.btnOpenClose setTitle:@"Close" forState:UIControlStateNormal];
        [cell.lblTime setText:@""];
    }
    else
    {

        NSString *str1=[[dataHoursArr1 objectAtIndex:indexPath.row]objectForKey:@"start_time"];
                NSLog(@"%@",str1);

        //str1=[str1 substringToIndex:[str1 length]-3];
     
        NSLog(@"%@",str1);
        NSString *str2=[[dataHoursArr1 objectAtIndex:indexPath.row] objectForKey:@"end_time"];
                NSLog(@"%@",str2);
       // str2= [str2 substringToIndex:[str2 length]-3];                NSLog(@"%@",str2);
        NSLog(@"%@",str2);
        [cell.btnOpenClose setBackgroundImage:[UIImage imageNamed:@"OpenImg"] forState:UIControlStateNormal];
        [cell.btnOpenClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cell.lblTime setText:[NSString stringWithFormat:@"%@-%@",str1,str2]];
    }
    [cell.imgDayIcon setImage:[UIImage imageNamed:[[dataHoursArr objectAtIndex:indexPath.row] objectForKey:@"imgName"]]];
    [cell.lblDayName setText:[[dataHoursArr1 objectAtIndex:indexPath.row] objectForKey:@"day"]];
    NSLog(@"%@",[[dataHoursArr objectAtIndex:indexPath.row] objectForKey:@"day"]);
    return cell;
}

#pragma mark- WebServices API..

-(void)getOperationHours
{
    NSMutableDictionary *param;
    
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:businessUserID,@"user_id", nil];
//        param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"user_id", nil];
        
NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    NSString *url = [NSString stringWithFormat:@"operationHours"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.173/petlox_svn/mobile/operationHours
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            dataHoursArr1=[responseDict objectForKey:@"data"];
            [tblView reloadData];
            
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self getOperationHours];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
