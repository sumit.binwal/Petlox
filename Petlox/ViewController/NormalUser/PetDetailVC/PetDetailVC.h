//
//  PetInfoVC.h
//  Petlox
//
//  Created by Sumit Sharma on 11/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PetDetailVC : UIViewController
@property(nonatomic,strong)NSString *petBreed;
@property(nonatomic,strong)NSString *petType;
@property(nonatomic,strong)NSMutableDictionary *dictPetDetail;
@end
