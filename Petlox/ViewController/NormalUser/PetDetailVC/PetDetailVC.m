//
//  PetInfoVC.m
//  Petlox
//
//  Created by Sumit Sharma on 11/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//
#import "PetImageVC.h"
#import "PetDetailVC.h"

@interface PetDetailVC ()<UIGestureRecognizerDelegate,UITextFieldDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate>
{
    IBOutlet UIView *VwbtnCntinw;
    IBOutlet UIView *cntainrVw;
    IBOutlet UITextField *txtPetName;
    IBOutlet UIButton *btnImagePicker;
    IBOutlet UIButton *btnCntinue;
    IBOutlet UITextField *txtPetAge;
    IBOutlet UITextField *txtPetWeight;
    IBOutlet UIScrollView *scrllVw;
    IBOutlet UIView *vq;
    IBOutlet UIView *v2;
    IBOutlet UIView *v3;
    IBOutlet UIView *v4;
    IBOutlet UILabel *lblTxView;
    IBOutlet UIView *toolbarVw;
    IBOutlet UILabel *lblTextLimit;
    IBOutlet UITextView *txtVwDetail;
    UITextField *activeTf;
    NSString *petID;
    UIBarButtonItem *btn1;
    
    IBOutlet UIImageView *imgViewPic;
}
@end

@implementation PetDetailVC
@synthesize petBreed,petType,dictPetDetail;
- (void)viewDidLoad {
    [super viewDidLoad];
    wbServiceCount=1;
    // Do any additional setup after loading the view from its nib.
    [self setUpView];
    NSLog(@"%@",dictPetDetail);
}

-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Pet Info"];
    txtPetAge.inputAccessoryView=toolbarVw;
    txtPetWeight.inputAccessoryView=toolbarVw;
    [btnImagePicker setUserInteractionEnabled:NO];
    [txtPetAge setUserInteractionEnabled:NO];
    [txtPetName setUserInteractionEnabled:NO];
    [txtPetWeight setUserInteractionEnabled:NO];
    [txtVwDetail setUserInteractionEnabled:NO];
    
    [VwbtnCntinw setHidden:YES];
    
    
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
//    UIBarButtonItem *btn1=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    
    btn1=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editBtnClicked:)];
    [self.navigationItem setRightBarButtonItem:btn1];
        btn1.tag=0;

    [txtPetAge setValue:[UIColor colorWithRed:63.0f/255.0f green:63.0f/255.0f blue:63.0f/255.0f alpha:1]
              forKeyPath:@"_placeholderLabel.textColor"];
    [txtPetName setValue:[UIColor colorWithRed:63.0f/255.0f green:63.0f/255.0f blue:63.0f/255.0f alpha:1]
              forKeyPath:@"_placeholderLabel.textColor"];
    [txtPetWeight setValue:[UIColor colorWithRed:63.0f/255.0f green:63.0f/255.0f blue:63.0f/255.0f alpha:1]
              forKeyPath:@"_placeholderLabel.textColor"];
    
    if (dictPetDetail.count>0) {
        txtPetAge.text=[dictPetDetail objectForKey:@"age"];
        txtPetName.text=[dictPetDetail objectForKey:@"name"];
        txtPetWeight.text=[dictPetDetail objectForKey:@"weight"];
        txtVwDetail.text=[dictPetDetail objectForKey:@"pet_details"];
        petBreed=[dictPetDetail objectForKey:@"breed_id"];
        if (txtVwDetail.text.length>0) {
            [lblTxView setHidden:YES];
            lblTextLimit.text=[NSString stringWithFormat:@"%u Characters",200-txtVwDetail.text.length];
            NSLog(@"%lu",(unsigned long)lblTextLimit.text.length);
            if (txtVwDetail.text.length>=200) {
                lblTextLimit.text=[NSString stringWithFormat:@"200 Characters"];
            }
        }
        

    }
    
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureBtnClicked)];
    tapGesture.delegate=self;
    tapGesture.numberOfTapsRequired=1;
    [self.view addGestureRecognizer:tapGesture];
    
    NSString *strPetImage = [dictPetDetail objectForKey:@"image"];
    if (strPetImage.length>0) {
        [imgViewPic setImageWithURL:[NSURL URLWithString:strPetImage] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        imgViewPic.clipsToBounds=YES;
    }
  
}


-(void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    UINavigationBar *navBar=navigationController.navigationBar;
    navigationController.navigationBar.barTintColor=[UIColor blackColor];
    //    navController.navigationBar.barTintColor=[UIColor colorWithRed:0.0f/255.0f green:172.0f/255.0f blue:238.0f/255.0f alpha:1];
    navBar.tintColor=[UIColor whiteColor];
    
    // UIImage *img=[UIImage imageNamed:@"topNavBar"];
    
    // [navBar setBackgroundImage:img forBarMetrics:UIBarMetricsDefault];
    [navigationController setNavigationBarHidden:FALSE];
    
    
    [navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:FONT_GULIM size:17],NSFontAttributeName,[UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    [navigationController.navigationBar setTranslucent:NO];
    
    
    UIView *statusView=[[UIView alloc]initWithFrame:CGRectMake(0, -[UIApplication sharedApplication].statusBarFrame.size.height,[UIApplication sharedApplication].statusBarFrame.size.width, [UIApplication sharedApplication].statusBarFrame.size.height)];
    [statusView setBackgroundColor:[UIColor blackColor]];
    //    [statusView setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f green:172.0f/255.0f blue:238.0f/255.0f alpha:1.0]];
    [navBar addSubview:statusView];
    
    
    
    [[UINavigationBar appearance] setTitleTextAttributes: @{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"OpenSans-Semibold" size:18]}];
}
-(void)viewDidLayoutSubviews
{
    if(K_SCREEN_WIDTH<=320){
        btnCntinue.titleLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:14.0];
    }
    btnCntinue.layer.cornerRadius = btnCntinue.frame.size.width/2;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TextFalidate Method
-(BOOL)isTextFieldValidate
{
    if (![CommonFunctions isValueNotEmpty:txtPetName.text]) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter pet name."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtPetAge.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter pet age."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtPetWeight.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter pet weight."];
        return NO;
    }
//    else if (txtVwDetail.text.length<1)
//    {
//        [CommonFunctions alertTitle:@"" withMessage:@"Please enter your message."];
//        return NO;
//    }
    return YES;
}

- (IBAction)imageButtonAction:(id)sender {
    UIActionSheet *actnSheet=[[UIActionSheet alloc]initWithTitle:@"Choose Photo" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose From Gallery", nil];
    [actnSheet showInView:self.view];
}
#pragma mark - UIScrollView Methods
-(void)scrollViewToCenterOfScreen:(UITextField *)textField cntainter:(UIView *)view
{
    [scrllVw setScrollEnabled:YES];
    float difference;
    if (scrllVw.contentSize.height == 600)
        difference = 50.0f;
    else
        difference = 50.0f;
    CGFloat viewCenterY = textField.center.y+view.frame.origin.y;
    NSLog(@"center.y -- %f",textField.center.y);
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];

    CGFloat avaliableHeight = applicationFrame.size.height - 5.0f;
    NSLog(@"avaliableHeight---%f",applicationFrame.size.height);
    
    CGFloat y = viewCenterY - avaliableHeight / 10.0f;
    if (y < 0)
        y = 0;
    NSLog(@"%f",y);
    [scrllVw setContentOffset:CGPointMake(0, y) animated:YES];
    [scrllVw setContentSize:CGSizeMake(320.0f, 630.0f)];
}
-(void)scrollViewToCenterOfScreenForTextView:(UITextView *)textView cntainter:(UIView *)view
{
    [scrllVw setScrollEnabled:YES];
    float difference;
    if (scrllVw.contentSize.height == 600)
        difference = 50.0f;
    else
        difference = 50.0f;
    CGFloat viewCenterY = textView.center.y+view.frame.origin.y;
    NSLog(@"center.y -- %f",textView.center.y);
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    CGFloat avaliableHeight = applicationFrame.size.height - 5.0f;
    NSLog(@"avaliableHeight---%f",applicationFrame.size.height);
    
    CGFloat y = viewCenterY - avaliableHeight / 10.0f;
    if (y < 0)
        y = 0;
    NSLog(@"%f",y);
    [scrllVw setContentOffset:CGPointMake(0, y) animated:YES];
    [scrllVw setContentSize:CGSizeMake(320.0f, 630.0f)];
}


-(void)scrollToNormalView
{
    [scrllVw setScrollEnabled:NO];
    [scrllVw setContentOffset:CGPointZero];
}

#pragma mark - IBAction Methods
-(IBAction)editBtnClicked:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if (btn.tag==0) {
        [txtPetAge setUserInteractionEnabled:YES];
        [btnImagePicker setUserInteractionEnabled:YES];
        [txtPetName setUserInteractionEnabled:YES];
        [txtPetWeight setUserInteractionEnabled:YES];
        [txtVwDetail setUserInteractionEnabled:YES];
        [VwbtnCntinw setHidden:NO];
        
        btn1 = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(editBtnClicked:)];
        self.navigationItem.rightBarButtonItem = btn1;

        btn1.tag=1;
    }
    else
    {
        [txtPetAge setUserInteractionEnabled:NO];
        [btnImagePicker setUserInteractionEnabled:NO];
        [txtPetName setUserInteractionEnabled:NO];
        [txtPetWeight setUserInteractionEnabled:NO];
        [txtVwDetail setUserInteractionEnabled:NO];
        [VwbtnCntinw setHidden:YES];
        btn1.tag=0;
        btn1 = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editBtnClicked:)];
        self.navigationItem.rightBarButtonItem = btn1;

        
        
    }
    
}


-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)toolBarNextBtnClicked:(id)sender {
    if (activeTf==txtPetAge) {
        [txtPetWeight becomeFirstResponder];
    }
    else if (activeTf==txtPetWeight)
    {
        [txtVwDetail becomeFirstResponder];
    }
}
- (IBAction)continueBtnClicked:(id)sender {
    [self.view endEditing:YES];
    [scrllVw setContentOffset:CGPointZero];
    if ([self isTextFieldValidate]) {
        if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self serviceListingAPI];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
        }
    }

}

-(void)tapGestureBtnClicked
{
    [self.view endEditing:YES];
    [self scrollToNormalView];
}

#pragma mark - UITextField Delegate Method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeTf=textField;
    if (txtPetName==textField) {
        [self scrollViewToCenterOfScreen:textField cntainter:vq];
    }
    else if (txtPetAge==textField)
    {
        [self scrollViewToCenterOfScreen:textField cntainter:v2];
    }
    else if (txtPetWeight==textField)
    {
        [self scrollViewToCenterOfScreen:textField cntainter:v3];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==txtPetName) {
        
        [txtPetAge becomeFirstResponder];
    }
    else if (textField==txtPetAge)
    {
        [txtPetWeight becomeFirstResponder];
    }
    else if(textField==txtPetWeight)
    {
        [textField resignFirstResponder];
        [scrllVw setContentOffset:CGPointZero];
    }
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField==txtPetWeight) {
        if (range.location>3) {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else if (textField==txtPetAge) {
        if (range.location>3) {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else if (textField==txtPetName) {
        if (range.location>25) {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else
    {
        return YES;
    }
}

#pragma mark - UITextView Delegate Method
-(void)textViewDidChange:(UITextView *)textView
{
    if (textView.text.length>0) {
        [lblTxView setHidden:YES];
    }
    else
    {
        [lblTxView setHidden:NO];
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (textView==txtVwDetail) {
        lblTextLimit.text=[NSString stringWithFormat:@"%u Characters",200-range.location];


        if (range.location>=200) {
            lblTextLimit.text=@"0 Character";
            return NO;


        }
        else
        {
            if ([text isEqualToString:@"\n"]) {
                [self scrollToNormalView];
                [txtVwDetail resignFirstResponder];
                return NO;
            }
            return YES;
        }
    }
    
    else
    {
        return YES;
    }
    return YES;
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self scrollViewToCenterOfScreenForTextView:textView cntainter:v4];
}

#pragma mark- WebServices API..

-(void)serviceListingAPI
{
    NSMutableDictionary *param;
    if (dictPetDetail.count>0) {
        param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:txtVwDetail.text,@"description",[CommonFunctions trimSpaceInString:txtPetWeight.text],@"weight",[CommonFunctions trimSpaceInString:txtPetAge.text],@"age",[CommonFunctions trimSpaceInString:txtPetName.text],@"name",[dictPetDetail objectForKey:@"breed_id"],@"breed",[dictPetDetail objectForKey:@"pet"],@"pet",[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"sid",[dictPetDetail objectForKey:@"id"],@"pet_id", nil];
    }
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    NSString *url = [NSString stringWithFormat:@"petsInfo"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.173/petlox_svn/mobile/callapi
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            PetImageVC *pivc=[[PetImageVC alloc]init];
            pivc.petID=[responseDict objectForKey:@"pet_id"];
            pivc.strPetImage=[dictPetDetail objectForKey:@"image"];
            petID=[responseDict objectForKey:@"pet_id"];
            [txtPetAge setUserInteractionEnabled:NO];
            [btnImagePicker setUserInteractionEnabled:NO];
            [txtPetName setUserInteractionEnabled:NO];
            [txtPetWeight setUserInteractionEnabled:NO];
            [txtVwDetail setUserInteractionEnabled:NO];
            [VwbtnCntinw setHidden:YES];
            btn1.tag=0;
            
            [btn1 setTitle:@"Edit"];

            
            petID=[responseDict objectForKey:@"pet_id"];
            
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self uploadPetImageForPet:[responseDict objectForKey:@"pet_id"]];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //[self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self serviceListingAPI];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}


#pragma mark - UIAction Sheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        UIImagePickerController *imgPicker=[[UIImagePickerController alloc]init];
        imgPicker.sourceType=UIImagePickerControllerSourceTypeCamera;
        imgPicker.delegate=self;
        imgPicker.allowsEditing=YES;
        [self presentViewController:imgPicker animated:YES completion:nil];
    }
    else if (buttonIndex ==1)
    {
        UIImagePickerController *imgPicker=[[UIImagePickerController alloc]init];
        imgPicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        imgPicker.delegate=self;
        imgPicker.allowsEditing=YES;
        [self presentViewController:imgPicker animated:YES completion:nil];
    }
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *imageEdited=[self scaleAndRotateImage:[info objectForKey:UIImagePickerControllerEditedImage]];
    imgViewPic.image=imageEdited;
    imgViewPic.clipsToBounds=YES;
    NSData *imgData4 = UIImageJPEGRepresentation(imgViewPic.image, 0.0f);
    imgViewPic.image=[UIImage imageWithData:imgData4];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIImagePickerCntroller Delegate Methods

-(UIImage *)scaleAndRotateImage:(UIImage *)image
{
    int kMaxResolution = 1242; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

-(void)uploadPetImageForPet:(NSString *)pet_id
{
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID]);
    NSMutableDictionary *param;
    NSString *url;
    
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:petID,@"id", nil];
    url = [NSString stringWithFormat:@"%@updatepetPicture",serverURL];
    
    
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:url]];
    
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:UD_TOKEN_ID]] forHTTPHeaderField:@"token"];
    
    AFHTTPRequestOperation *op = [manager POST:@"" parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if (imgViewPic.image != nil) {
            NSData *imageData = UIImageJPEGRepresentation(imgViewPic.image, 0.8f);
            
            [formData appendPartWithFileData:imageData name:@"image" fileName:@"ProfileImage.jpg" mimeType:@"image/jpeg"];
        }
        
        
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        
        if (responseObject != nil)
        {
            NSLog(@"Profile  responseObject error: %@",error);
            
            NSDictionary *jsonDic = (NSDictionary *)responseObject;
            NSLog(@"Profile  JSON: %@",jsonDic);
            
            if (jsonDic != nil)
            {
                if ([[jsonDic objectForKey:@"replyCode"] isEqualToString:@"success"])
                {
                    
                    [CommonFunctions alertTitle:@"Petlox" withMessage:@"Pet info updated successfully."];
                }
                else
                {
                    [CommonFunctions alertTitle:@"" withMessage:[jsonDic objectForKey:@"replyMsg"]];
                }
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[jsonDic objectForKey:@"replyMsg"]];
            }
        }
        [CommonFunctions removeActivityIndicator];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
        [CommonFunctions removeActivityIndicator];
    }];
    
    
    [op start];
    
}

@end
