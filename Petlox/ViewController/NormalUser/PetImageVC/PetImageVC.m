//
//  PetImageVC.m
//  Petlox
//
//  Created by Sumit Sharma on 14/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "PetImageVC.h"
#import "SignInVC.h"
#import "CongratulationVC.h"
@interface PetImageVC ()<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate>
{
    IBOutlet UIImageView *imgVwPet;
    
}
@end

@implementation PetImageVC
@synthesize petID,strPetImage;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUpView];
}

-(void)viewDidLayoutSubviews{
    self.btnContinue.layer.cornerRadius = self.btnContinue.frame.size.width/2;
}

-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Pet Profile Picture"];
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
    if (strPetImage.length>0) {
        [imgVwPet setImageWithURL:[NSURL URLWithString:strPetImage] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        imgVwPet.clipsToBounds=YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction Button Method
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)continueBtnClicked:(id)sender {
    if (imgVwPet.image==nil) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please select pet image."];
    }
    else
    {
        if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self uploadPetImage];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
        }
    }
}
- (IBAction)imageButtonClicked:(id)sender {
    
    UIActionSheet *actnSheet=[[UIActionSheet alloc]initWithTitle:@"Choose Photo" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose From Gallery", nil];
    [actnSheet showInView:self.view];
}

#pragma mark - UIAction Sheet Delegate 
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        UIImagePickerController *imgPicker=[[UIImagePickerController alloc]init];
        imgPicker.sourceType=UIImagePickerControllerSourceTypeCamera;
        imgPicker.delegate=self;
        imgPicker.allowsEditing=YES;
        [self presentViewController:imgPicker animated:YES completion:nil];
    }
    else if (buttonIndex ==1)
    {
        UIImagePickerController *imgPicker=[[UIImagePickerController alloc]init];
        imgPicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        imgPicker.delegate=self;
        imgPicker.allowsEditing=YES;
        [self presentViewController:imgPicker animated:YES completion:nil];
    }
}

#pragma mark - UIImagePicker Delegate Method
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *imageEdited=[self scaleAndRotateImage:[info objectForKey:UIImagePickerControllerOriginalImage]];
    imgVwPet.image=imageEdited;
    imgVwPet.clipsToBounds=YES;
    NSData *imgData4 = UIImageJPEGRepresentation(imgVwPet.image, 0.0f);
    imgVwPet.image=[UIImage imageWithData:imgData4];

    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UINavigation Controller Delegate Method
-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{

    navigationController.navigationBar.backgroundColor=[UIColor colorWithRed:71.0f/255.0f green:13.0f/255.0f blue:52.0f/255.0f alpha:1];
    
    UIView *statusView=[[UIView alloc]initWithFrame:CGRectMake(0, -[UIApplication sharedApplication].statusBarFrame.size.height,[UIApplication sharedApplication].statusBarFrame.size.width, [UIApplication sharedApplication].statusBarFrame.size.height)];
    [statusView setBackgroundColor:[UIColor colorWithRed:71.0f/255.0f green:13.0f/255.0f blue:52.0f/255.0f alpha:1]];
    [navigationController.navigationBar addSubview:statusView];
    [navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:FONT_GULIM size:18.85f]}];
    
    viewController.navigationController.navigationBar.tintColor=[UIColor whiteColor];
}


#pragma mark- WebServices API..

-(void)uploadPetImage
{
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID]);
    NSMutableDictionary *param;
    NSString *url;
    
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:petID,@"id", nil];
    url = [NSString stringWithFormat:@"%@updatepetPicture",serverURL];
    
    
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:url]];
    
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:UD_TOKEN_ID]] forHTTPHeaderField:@"token"];
    
    AFHTTPRequestOperation *op = [manager POST:@"" parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if (imgVwPet.image != nil) {
            NSData *imageData = UIImageJPEGRepresentation(imgVwPet.image, 0.8f);
            
            [formData appendPartWithFileData:imageData name:@"image" fileName:@"ProfileImage.jpg" mimeType:@"image/jpeg"];
        }
        
        
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        
        if (responseObject != nil)
        {
            NSLog(@"Profile  responseObject error: %@",error);
            
            NSDictionary *jsonDic = (NSDictionary *)responseObject;
            NSLog(@"Profile  JSON: %@",jsonDic);
            
            if (jsonDic != nil)
            {
                if ([[jsonDic objectForKey:@"replyCode"] isEqualToString:@"success"])
                {
                    CongratulationVC *cvc=[[CongratulationVC alloc]init];
                    [[NSUserDefaults standardUserDefaults]setObject:[jsonDic objectForKey:@"is_completed"] forKey:UD_IS_PET_PROFILE_COMPLETE];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    
                    NSLog(@":::::%@",[[NSUserDefaults standardUserDefaults]objectForKey:UD_IS_PET_PROFILE_COMPLETE]);
                    
                    NSArray *viewControllers = self.navigationController.viewControllers;
                    UIViewController *rootViewController = (UIViewController *)[viewControllers objectAtIndex:0];
                    
                    NSLog(@"%@",rootViewController);
                    

                    if ([rootViewController isKindOfClass:[SignInVC class]]) {
                    [self.navigationController pushViewController:cvc animated:YES];
                    }
                    else{
                        [CommonFunctions alertTitle:@"" withMessage:@"Pet added sucessfully." withDelegate:self withTag:100];
                    }

                }
                else
                {
                    [CommonFunctions alertTitle:@"" withMessage:[jsonDic objectForKey:@"replyMsg"]];
                }
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[jsonDic objectForKey:@"replyMsg"]];
            }
        }
        [CommonFunctions removeActivityIndicator];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
        [CommonFunctions removeActivityIndicator];
    }];
    
    
    [op start];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

#pragma mark - UIImagePickerCntroller Delegate Methods

-(UIImage *)scaleAndRotateImage:(UIImage *)image
{
    int kMaxResolution = 1242; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}
@end
