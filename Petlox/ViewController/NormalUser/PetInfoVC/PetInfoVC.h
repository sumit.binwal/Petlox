//
//  PetInfoVC.h
//  Petlox
//
//  Created by Sumit Sharma on 11/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PetInfoVC : UIViewController
@property(nonatomic,strong)NSString *petBreed;
@property(nonatomic,strong)NSString *petType;
@property (weak, nonatomic) IBOutlet UIButton *btnContinue;
@property (weak, nonatomic) IBOutlet UIImageView *petImageView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *toolBarNextButton;


- (IBAction)imageButtonAction:(id)sender;
@end
