//
//  PetInfoVC.m
//  Petlox
//
//  Created by Sumit Sharma on 11/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//
#import "PetImageVC.h"
#import "PetInfoVC.h"
#import "CongratulationVC.h"
#import "SignInVC.h"
#import "MainHomeVC.h"

@interface PetInfoVC ()<UIGestureRecognizerDelegate,UITextFieldDelegate,UITextViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate>
{
    IBOutlet UIView *cntainrVw;
    IBOutlet UITextField *txtPetName;
    IBOutlet UITextField *txtPetAge;
    IBOutlet UITextField *txtPetWeight;
    IBOutlet UIScrollView *scrllVw;
    IBOutlet UIView *vq;
    IBOutlet UIView *v2;
    IBOutlet UIView *v3;
    IBOutlet UIView *v4;
    IBOutlet UILabel *lblTxView;
    IBOutlet UIView *toolbarVw;
    IBOutlet UILabel *lblTextLimit;
    IBOutlet UITextView *txtVwDetail;
    UITextField *activeTf;
    NSString *petID;
}
@end

@implementation PetInfoVC
@synthesize petBreed,petType;
- (void)viewDidLoad {
    [super viewDidLoad];
    wbServiceCount=1;
    // Do any additional setup after loading the view from its nib.
    [self setUpView];
}

-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Pet Info"];
    txtPetAge.inputAccessoryView=toolbarVw;
    txtPetWeight.inputAccessoryView=toolbarVw;
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];

    [txtPetAge setValue:[UIColor colorWithRed:133.0f/255.0f green:133.0f/255.0f blue:133.0f/255.0f alpha:1]
              forKeyPath:@"_placeholderLabel.textColor"];
    [txtPetName setValue:[UIColor colorWithRed:133.0f/255.0f green:133.0f/255.0f blue:133.0f/255.0f alpha:1]
              forKeyPath:@"_placeholderLabel.textColor"];
    [txtPetWeight setValue:[UIColor colorWithRed:133.0f/255.0f green:133.0f/255.0f blue:133.0f/255.0f alpha:1]
              forKeyPath:@"_placeholderLabel.textColor"];
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureBtnClicked)];
    tapGesture.delegate=self;
    tapGesture.numberOfTapsRequired=1;
    [self.view addGestureRecognizer:tapGesture];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if(K_SCREEN_WIDTH<=320){
        self.btnContinue.titleLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:14.0];
    }
    self.btnContinue.layer.cornerRadius = self.btnContinue.frame.size.width/2;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation Controller Delegate Method
-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    navigationController.navigationBar.backgroundColor=[UIColor colorWithRed:71.0f/255.0f green:13.0f/255.0f blue:52.0f/255.0f alpha:1];
    
    UIView *statusView=[[UIView alloc]initWithFrame:CGRectMake(0, -[UIApplication sharedApplication].statusBarFrame.size.height,[UIApplication sharedApplication].statusBarFrame.size.width, [UIApplication sharedApplication].statusBarFrame.size.height)];
    [statusView setBackgroundColor:[UIColor colorWithRed:71.0f/255.0f green:13.0f/255.0f blue:52.0f/255.0f alpha:1]];
    [navigationController.navigationBar addSubview:statusView];
    [navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:FONT_GULIM size:18.85f]}];
    [[UIApplication
      sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    viewController.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
}


#pragma mark - TextFalidate Method
-(BOOL)isTextFieldValidate
{
    if (self.petImageView.image==nil) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please select pet image."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtPetName.text]) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter pet name."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtPetAge.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter pet age."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtPetWeight.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter pet weight."];
        return NO;
    }
//    else if (txtVwDetail.text.length<1)
//    {
//        [CommonFunctions alertTitle:@"" withMessage:@"Please enter your message."];
//        return NO;
//    }
    return YES;
}
#pragma mark - UIScrollView Methods
-(void)scrollViewToCenterOfScreen:(UITextField *)textField cntainter:(UIView *)view
{
    [scrllVw setScrollEnabled:YES];
    float difference;
    if (scrllVw.contentSize.height == 600)
        difference = 50.0f;
    else
        difference = 50.0f;
    CGFloat viewCenterY = textField.center.y+view.frame.origin.y;
    NSLog(@"center.y -- %f",textField.center.y);
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];

    CGFloat avaliableHeight = applicationFrame.size.height - 5.0f;
    NSLog(@"avaliableHeight---%f",applicationFrame.size.height);
    
    CGFloat y = viewCenterY - avaliableHeight / 10.0f;
    if (y < 0)
        y = 0;
    NSLog(@"%f",y);
    if(IS_IPHONE_4){
        [scrllVw setContentOffset:CGPointMake(0, y-difference) animated:YES];
    } else if(IS_IPHONE_5){
        [scrllVw setContentOffset:CGPointMake(0, y+difference) animated:YES];
    } else {
        [scrllVw setContentOffset:CGPointMake(0, y) animated:YES];
    }
    [scrllVw setContentSize:CGSizeMake(320.0f, 630.0f)];
}
-(void)scrollViewToCenterOfScreenForTextView:(UITextView *)textView cntainter:(UIView *)view
{
    [scrllVw setScrollEnabled:YES];
    float difference;
    if (scrllVw.contentSize.height == 600)
        difference = 50.0f;
    else
        difference = 50.0f;
    CGFloat viewCenterY = textView.center.y+view.frame.origin.y;
    NSLog(@"center.y -- %f",textView.center.y);
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    CGFloat avaliableHeight = applicationFrame.size.height - 5.0f;
    NSLog(@"avaliableHeight---%f",applicationFrame.size.height);
    
    CGFloat y = viewCenterY - avaliableHeight / 10.0f;
    if (y < 0)
        y = 0;
    NSLog(@"%f",y);
    [scrllVw setContentOffset:CGPointMake(0, y) animated:YES];
    [scrllVw setContentSize:CGSizeMake(320.0f, 630.0f)];
}


-(void)scrollToNormalView
{
    [scrllVw setScrollEnabled:NO];
    [scrllVw setContentOffset:CGPointZero];
}

#pragma mark - IBAction Methods
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)toolBarNextBtnClicked:(id)sender {
    
    if([self.toolBarNextButton.title isEqualToString:@"Done"]){
        [self.view endEditing:YES];
        [self scrollToNormalView];
    }
    
    self.toolBarNextButton.title = @"Next";
    if (activeTf==txtPetAge) {
        [txtPetWeight becomeFirstResponder];
        self.toolBarNextButton.title = @"Done";
    }
//    else if (activeTf==txtPetWeight)
//    {
//        [txtVwDetail becomeFirstResponder];
//    }
}
- (IBAction)continueBtnClicked:(id)sender {
    [self.view endEditing:YES];
    [scrllVw setContentOffset:CGPointZero];
    if ([self isTextFieldValidate]) {
        if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self serviceListingAPI];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
        }
    }

}

- (IBAction)imageButtonAction:(id)sender {
    UIActionSheet *actnSheet=[[UIActionSheet alloc]initWithTitle:@"Choose Photo" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose From Gallery", nil];
    [actnSheet showInView:self.view];
}

-(void)tapGestureBtnClicked
{
    [self.view endEditing:YES];
    [self scrollToNormalView];
}

#pragma mark - UITextField Delegate Method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeTf=textField;
    self.toolBarNextButton.title = @"Next";
    if (txtPetName==textField) {
        [self scrollViewToCenterOfScreen:textField cntainter:vq];
    }
    else if (txtPetAge==textField)
    {
        [self scrollViewToCenterOfScreen:textField cntainter:v2];
    }
    else if (txtPetWeight==textField)
    {
        [self scrollViewToCenterOfScreen:textField cntainter:v3];
        self.toolBarNextButton.title = @"Done";
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==txtPetName) {
        
        [txtPetAge becomeFirstResponder];
    }
    else if (textField==txtPetAge)
    {
        [txtPetWeight becomeFirstResponder];
    }
    else if(textField==txtPetWeight)
    {
        [textField resignFirstResponder];
        [scrllVw setContentOffset:CGPointZero];
    }
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField==txtPetWeight) {
        if (range.location>3) {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else if (textField==txtPetAge) {
        if (range.location>3) {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else if (textField==txtPetName) {
        if (range.location>25) {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else
    {
        return YES;
    }
}

#pragma mark - UITextView Delegate Method
-(void)textViewDidChange:(UITextView *)textView
{
    if (textView.text.length>0) {
        [lblTxView setHidden:YES];
    }
    else
    {
        [lblTxView setHidden:NO];
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (textView==txtVwDetail) {
        lblTextLimit.text=[NSString stringWithFormat:@"%u Characters",200-range.location];


        if (range.location>=200) {
            lblTextLimit.text=@"0 Character";
            return NO;


        }
        else
        {
            if ([text isEqualToString:@"\n"]) {
                [self scrollToNormalView];
                [txtVwDetail resignFirstResponder];
                return NO;
            }
            return YES;
        }
    }
    
    else
    {
        return YES;
    }
    return YES;
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self scrollViewToCenterOfScreenForTextView:textView cntainter:v4];
}

#pragma mark- WebServices API..

-(void)uploadPetImageForPet:(NSString *)pet_id
{
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID]);
    NSMutableDictionary *param;
    NSString *url;
    
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:petID,@"id", nil];
    url = [NSString stringWithFormat:@"%@updatepetPicture",serverURL];
    
    
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:url]];
    
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:UD_TOKEN_ID]] forHTTPHeaderField:@"token"];
    
    AFHTTPRequestOperation *op = [manager POST:@"" parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if (self.petImageView.image != nil) {
            NSData *imageData = UIImageJPEGRepresentation(self.petImageView.image, 0.8f);
            
            [formData appendPartWithFileData:imageData name:@"image" fileName:@"ProfileImage.jpg" mimeType:@"image/jpeg"];
        }
        
        
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        
        if (responseObject != nil)
        {
            NSLog(@"Profile  responseObject error: %@",error);
            
            NSDictionary *jsonDic = (NSDictionary *)responseObject;
            NSLog(@"Profile  JSON: %@",jsonDic);
            
            if (jsonDic != nil)
            {
                if ([[jsonDic objectForKey:@"replyCode"] isEqualToString:@"success"])
                {
                    CongratulationVC *cvc=[[CongratulationVC alloc]init];
                    [[NSUserDefaults standardUserDefaults]setObject:[jsonDic objectForKey:@"is_completed"] forKey:UD_IS_PET_PROFILE_COMPLETE];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    
                    NSLog(@":::::%@",[[NSUserDefaults standardUserDefaults]objectForKey:UD_IS_PET_PROFILE_COMPLETE]);
                    
                    NSArray *viewControllers = self.navigationController.viewControllers;
                    UIViewController *rootViewController = (UIViewController *)[viewControllers objectAtIndex:0];
                    
                    NSLog(@"%@",rootViewController);
                    
                    
                    if ([rootViewController isKindOfClass:[MainHomeVC class]]) {
                        [self.navigationController pushViewController:cvc animated:YES];
                    }
                    else{
                        [CommonFunctions alertTitle:@"" withMessage:@"Pet added sucessfully." withDelegate:self withTag:100];
                    }
                    
                }
                else
                {
                    [CommonFunctions alertTitle:@"" withMessage:[jsonDic objectForKey:@"replyMsg"]];
                }
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[jsonDic objectForKey:@"replyMsg"]];
            }
        }
        [CommonFunctions removeActivityIndicator];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
        [CommonFunctions removeActivityIndicator];
    }];
    
    
    [op start];
    
}

-(void)serviceListingAPI
{
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    
    NSMutableDictionary *param;
    if (petID.length>0) {
        param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"",@"description",[CommonFunctions trimSpaceInString:txtPetWeight.text],@"weight",[CommonFunctions trimSpaceInString:txtPetAge.text],@"age",[CommonFunctions trimSpaceInString:txtPetName.text],@"name",petBreed,@"breed",petType,@"pet",[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"sid",petID,@"pet_id", nil];
        
    }
    else
    {
        param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"",@"description",[CommonFunctions trimSpaceInString:txtPetWeight.text],@"weight",[CommonFunctions trimSpaceInString:txtPetAge.text],@"age",[CommonFunctions trimSpaceInString:txtPetName.text],@"name",petBreed,@"breed",petType,@"pet",[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"sid", nil];

    }
    NSString *url = [NSString stringWithFormat:@"petsInfo"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.173/petlox_svn/mobile/callapi
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            petID=[responseDict objectForKey:@"pet_id"];
            
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self uploadPetImageForPet:[responseDict objectForKey:@"pet_id"]];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self serviceListingAPI];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

#pragma mark - UIAction Sheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        UIImagePickerController *imgPicker=[[UIImagePickerController alloc]init];
        imgPicker.sourceType=UIImagePickerControllerSourceTypeCamera;
        imgPicker.delegate=self;
        imgPicker.allowsEditing=YES;
        [self presentViewController:imgPicker animated:YES completion:nil];
    }
    else if (buttonIndex ==1)
    {
        UIImagePickerController *imgPicker=[[UIImagePickerController alloc]init];
        imgPicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        imgPicker.delegate=self;
        imgPicker.allowsEditing=YES;
        [self presentViewController:imgPicker animated:YES completion:nil];
    }
}

#pragma mark - UIImagePicker Delegate Method
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *imageEdited=[self scaleAndRotateImage:[info objectForKey:UIImagePickerControllerEditedImage]];
    self.petImageView.image=imageEdited;
    self.petImageView.clipsToBounds=YES;
    NSData *imgData4 = UIImageJPEGRepresentation(self.petImageView.image, 0.0f);
    self.petImageView.image=[UIImage imageWithData:imgData4];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIImagePickerCntroller Delegate Methods

-(UIImage *)scaleAndRotateImage:(UIImage *)image
{
    int kMaxResolution = 1242; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}
@end
