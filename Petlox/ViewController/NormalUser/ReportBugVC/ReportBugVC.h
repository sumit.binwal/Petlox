//
//  ReportBugVC.h
//  Petlox
//
//  Created by Sumit Sharma on 14/12/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportBugVC : UIViewController<UITextFieldDelegate,UITextViewDelegate>
@property (strong, nonatomic) IBOutlet UITextField *txtTitle;
@property (strong, nonatomic) IBOutlet UITextView *txtBugDiscription;
@property (strong, nonatomic) IBOutlet UILabel *lblTxtDiscription;

@end
