//
//  ReservationCanceledVC.m
//  Petlox
//
//  Created by Sumit Sharma on 06/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "ReservationCanceledVC.h"

@interface ReservationCanceledVC ()
{
    IBOutlet UILabel *lblAddress;
    IBOutlet UIImageView *imgStar1;
    IBOutlet UIImageView *imgStar2;
    IBOutlet UILabel *lblStatus;
    IBOutlet UIImageView *imgStar3;
    IBOutlet UIButton *btnFav;
    IBOutlet UIImageView *imgStar4;
    IBOutlet UIImageView *imgStar5;
    IBOutlet UILabel *lblBusinessName;
    IBOutlet UILabel *lblReviews;
    IBOutlet UIImageView *imgVwBusinessProfileImg;
    IBOutlet UIImageView *imgVwPetProfileImg;
    IBOutlet UILabel *lblReservationDate;
    IBOutlet UILabel *lblMiles;
    IBOutlet UILabel *lblPetBreed;
    IBOutlet UILabel *lblPetName;
    IBOutlet UILabel *lblPetAge;
    IBOutlet UILabel *lblPetWeight;
}
@end

@implementation ReservationCanceledVC
@synthesize dictCancleReservationData;
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@",dictCancleReservationData);
    [self setUpView];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.btnBack.layer.cornerRadius = self.btnBack.frame.size.width/2;
}

-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Reservation Canceled"];
    self.navigationItem.hidesBackButton = YES;
//    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
//    [self.navigationItem setLeftBarButtonItem:btn];
    
    [self fillCancleReservationInformation];

}

-(void)fillCancleReservationInformation
{
    lblBusinessName.text=[dictCancleReservationData objectForKey:@"business"];
    lblMiles.text=[dictCancleReservationData objectForKey:@"distance"];

    if ([self isNotNull:[dictCancleReservationData objectForKey:@"fav"]]) {
        if ([[[dictCancleReservationData objectForKey:@"fav"] stringValue]isEqualToString:@"1"]) {
            [btnFav setImage:[UIImage imageNamed:@"favorite_foucs_icon"] forState:UIControlStateNormal];
        }
        else
        {
            [btnFav setImage:[UIImage imageNamed:@"favorite_icon"] forState:UIControlStateNormal];
        }
    }
    lblStatus.text=[dictCancleReservationData valueForKey:@"reservation_status"];
    lblAddress.text=[dictCancleReservationData objectForKey:@""];
    NSString *imgStr=[dictCancleReservationData objectForKey:@"business_image"];
    if (imgStr.length>1) {
        [imgVwBusinessProfileImg setImageWithURL:[NSURL URLWithString:imgStr] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
//    imgVwBusinessProfileImg.layer.cornerRadius=imgVwBusinessProfileImg.frame.size.width/2;
//    imgVwBusinessProfileImg.layer.borderColor=[[UIColor whiteColor]CGColor];
//    imgVwBusinessProfileImg.layer.borderWidth=1;
    imgVwBusinessProfileImg.clipsToBounds=YES;
    lblReservationDate.text=[dictCancleReservationData objectForKey:@"reservation_date"];
    if ([dictCancleReservationData valueForKey:@"ratingcount"]!=[NSNull null])
    {
        int reviewCount=[[dictCancleReservationData valueForKey:@"ratingcount"]intValue];
        if (reviewCount>1) {
            lblReviews.text=[NSString stringWithFormat:@"%@ reviews",[dictCancleReservationData valueForKey:@"ratingcount"]];
        }
        else
        {
            lblReviews.text=[NSString stringWithFormat:@"%@ review",[dictCancleReservationData valueForKey:@"ratingcount"]];
        }
    }
    if ([self isNotNull:[dictCancleReservationData objectForKey:@"ratingavg"]]) {
        int avgRating=[[dictCancleReservationData objectForKey:@"ratingavg"]intValue];
        switch (avgRating) {
            case 0:
            {
                break;
            }
                
            case 1:
            {
                [imgStar1 setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
            case 2:
            {
                [imgStar1 setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgStar2 setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
            case 3:
            {
                [imgStar1 setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgStar2 setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgStar3 setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
            case 4:
            {
                [imgStar1 setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgStar2 setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgStar3 setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgStar4 setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
            case 5:
            {
                [imgStar1 setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgStar2 setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgStar3 setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgStar4 setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgStar5 setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
                
            default:
                break;
        }
        
    }
   //  Pet details fills
//    imgVwPetProfileImg.layer.borderWidth=2.0f;
//    imgVwPetProfileImg.layer.borderColor=[[UIColor colorWithRed:149.0f/255.0f green:149.0f/255.0f blue:149.0f/255.0f alpha:1.0f] CGColor];
//    imgVwPetProfileImg.layer.cornerRadius=5.0f;
    imgVwPetProfileImg.clipsToBounds=YES;

    lblPetAge.text=[NSString stringWithFormat:@"%@ Years",[dictCancleReservationData objectForKey:@"age"]];
    lblPetBreed.text=[NSString stringWithFormat:@"%@, %@",[dictCancleReservationData objectForKey:@"pet"],[dictCancleReservationData objectForKey:@"breed"]];
    lblPetName.text=[dictCancleReservationData objectForKey:@"pet_name"];
    lblAddress.text=[dictCancleReservationData objectForKey:@"address"];
    lblPetWeight.text=[NSString stringWithFormat:@"%@LBS",[dictCancleReservationData objectForKey:@"weight"]];
    NSString *petImgStr=[dictCancleReservationData objectForKey:@"image"];
    if (petImgStr.length>1) {
        [imgVwPetProfileImg setImageWithURL:[NSURL URLWithString:petImgStr] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
}

-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backToPetLoxBtnClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
