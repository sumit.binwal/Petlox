//
//  ReserveNowVC.h
//  Petlox
//
//  Created by Sumit Sharma on 02/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReserveNowVC : UIViewController
{

}
@property (strong,nonatomic) NSMutableDictionary *dictUserDetails;
@property (weak, nonatomic) IBOutlet UIView *datePickerViewBack;
@property (weak, nonatomic) IBOutlet UIButton *btnContinue;
@end
