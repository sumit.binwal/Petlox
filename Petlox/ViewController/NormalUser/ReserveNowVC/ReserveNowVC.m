//
//  ReserveNowVC.m
//  Petlox
//
//  Created by Sumit Sharma on 02/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "ReserveNowVC.h"
#import "ShowPetVC.h"
#import "ConfirmReservationVC.h"
@interface ReserveNowVC ()
{
    IBOutlet UIImageView *imgVwProfile;
    
    IBOutlet UIButton *btnFav;
    IBOutlet NSLayoutConstraint *constraintDatePickerStrips;
    IBOutlet UILabel *lblAddress;
    IBOutlet NSLayoutConstraint *constraintBgDatepicker;
    IBOutlet NSLayoutConstraint *constraintCntinueTopBtn;
    IBOutlet UIImageView *imgFivStar;
    IBOutlet UIImageView *imgFrthStar;
    IBOutlet UIImageView *imgTrdStar;
    IBOutlet UIImageView *imgScndStar;
    IBOutlet UIImageView *imgFrstStar;
    IBOutlet UIDatePicker *datePicker;
    IBOutlet UILabel *lblMiles;
    IBOutlet UILabel *lblReviews;
    IBOutlet UILabel *lblBusinessName;
}
@end

@implementation ReserveNowVC
@synthesize dictUserDetails;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [datePicker setLocale:locale];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.datePickerViewBack.layer.cornerRadius = 5.0f;
    self.btnContinue.layer.cornerRadius = self.btnContinue.frame.size.width/2;
    if(K_SCREEN_HEIGHT<=568){
        self.btnContinue.titleLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:14.0];
    }
}

-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    
    datePicker.minimumDate = [NSDate date];

    
    UIBarButtonItem *backButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked)];
    backButton.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:backButton];

//    imgVwProfile.layer.cornerRadius=imgVwProfile.frame.size.width/2;
    imgVwProfile.clipsToBounds=YES;
    imgVwProfile.layer.borderColor=[[UIColor whiteColor]CGColor];
    imgVwProfile.layer.borderWidth=1;
    
    NSString *imgUrlstr=[dictUserDetails objectForKey:@"image"];
    if (imgUrlstr.length>1) {
        [imgVwProfile setImageWithURL:[NSURL URLWithString:[dictUserDetails objectForKey:@"image"]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    

    if ([self isNotNull:[dictUserDetails objectForKey:@"fav"]]) {
            NSString *favValue=[NSString stringWithFormat:@"%@",[dictUserDetails objectForKey:@"fav"]];
        if ([favValue isEqualToString:@"1"]) {
            [btnFav setImage:[UIImage imageNamed:@"favorite_foucs_icon"] forState:UIControlStateNormal];
        }
        else
        {
            [btnFav setImage:[UIImage imageNamed:@"favorite_icon"] forState:UIControlStateNormal];
        }
    }
    lblAddress.text=[dictUserDetails objectForKey:@"address"];
    if ([self isNotNull:[dictUserDetails objectForKey:@"avg_rate"]]) {
        int avgRating=[[dictUserDetails objectForKey:@"avg_rate"]intValue];
        switch (avgRating) {
            case 0:
            {
                break;
            }
                
            case 1:
            {
                [imgFrstStar setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
            case 2:
            {
                [imgFrstStar setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgScndStar setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
            case 3:
            {
                [imgFrstStar setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgScndStar setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgTrdStar setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
            case 4:
            {
                [imgFrstStar setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgScndStar setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgTrdStar setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgFrthStar setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
            case 5:
            {
                [imgFrstStar setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgScndStar setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgTrdStar setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgFrthStar setImage:[UIImage imageNamed:@"star_foucs"]];
                [imgFivStar setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
                
            default:
                break;
        }

    }
    [self.navigationItem setTitle:[[dictUserDetails objectForKey:@"business_name"] capitalizedString]];
    lblBusinessName.text=[[dictUserDetails objectForKey:@"business_name"] capitalizedString];
    lblMiles.text=[dictUserDetails objectForKey:@"distance"];
    if ([self isNotNull:[dictUserDetails objectForKey:@"review_count"]]) {
        int reviewCount=[[dictUserDetails objectForKey:@"review_count"] intValue];
        if (reviewCount>1) {
            lblReviews.text=[NSString stringWithFormat:@"%@ reviews",[dictUserDetails objectForKey:@"review_count"]];
        }
        else
        {
            lblReviews.text=[NSString stringWithFormat:@"%@ review",[dictUserDetails objectForKey:@"review_count"]];
        }
    }

    NSLog(@"%@",dictUserDetails);
    if ([UIScreen mainScreen].bounds.size.height<568)
    {
        constraintBgDatepicker.constant=220.0f;
        constraintCntinueTopBtn.constant=5.0f;
        
    }

}

-(void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)cntinueBtnClicked:(id)sender {
    

    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString *myString = [dateFormatter stringFromDate:datePicker.date];
    NSLog(@"%@",myString);
    [dateFormatter setAMSymbol:@"am"];
    [dateFormatter setPMSymbol:@"pm"];
    NSDate *yourDate = [dateFormatter dateFromString:myString];
    dateFormatter.dateFormat = @"dd, MMM yyyy hh:mma";
    NSString *dateString=[dateFormatter stringFromDate:yourDate] ;
    NSLog(@"%@",dateString);

    NSMutableDictionary *dictTempData=[[NSMutableDictionary alloc]initWithDictionary:dictUserDetails];
    dictUserDetails=[[NSMutableDictionary alloc]initWithDictionary:dictTempData];
   // [(NSMutableDictionary*)dictUserDetails setValue:dateString forKey:@"appointedDate"];
    [dictUserDetails setObject:dateString forKey:@"appointedDate"];

    
    ShowPetVC *spvc=[[ShowPetVC alloc]initWithNibName:@"ShowPetVC" bundle:nil];
    spvc.dictBusinessDetail=dictUserDetails;
    [spvc.arrPets addObject:dictUserDetails];
    [self.navigationController pushViewController:spvc animated:YES];
}

@end
