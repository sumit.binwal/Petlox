//
//  RightMenuMessageCell.h
//  Petlox
//
//  Created by Suchita Bohra on 22/07/16.
//  Copyright © 2016 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RightMenuMessageCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *lblTxt;
@property (nonatomic, strong) IBOutlet UILabel *lblBadge;

@end
