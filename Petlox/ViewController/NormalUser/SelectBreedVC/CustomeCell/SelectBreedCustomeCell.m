//
//  SelectBreedCustomeCell.m
//  Petlox
//
//  Created by Sumit Sharma on 11/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "SelectBreedCustomeCell.h"

@implementation SelectBreedCustomeCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
return self=[[[NSBundle mainBundle]loadNibNamed:@"SelectBreedCustomeCell" owner:self options:nil]objectAtIndex:0];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
