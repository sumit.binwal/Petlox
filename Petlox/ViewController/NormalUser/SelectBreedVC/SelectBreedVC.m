//
//  SelectBreedVC.m
//  Petlox
//
//  Created by Sumit Sharma on 11/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "SelectBreedVC.h"
#import "SelectBreedCustomeCell.h"
#import "PetInfoVC.h"
@interface SelectBreedVC ()<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate>
{
    
    IBOutlet UITableView *tblVw;
    IBOutlet UITextField *txtSearchbar;
    NSMutableArray *breedDataArr;
    NSString *petBreedID;
//    IBOutlet UIImageView *imgVwSearchimg;
    IBOutlet UILabel *norecordFound;
    NSInteger currentPage, totalPage,totalPost, nextPage;
    
}
@end

@implementation SelectBreedVC
@synthesize strBreed;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    wbServiceCount=1;
    
    arrAllrecords=[[NSMutableArray alloc]init];

    // Do any additional setup after loading the view from its nib.
}



-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    
    //TextField Placeholder Color Change
    [txtSearchbar setValue:[UIColor colorWithRed:133.0f/255.0f green:133.0f/255.0f blue:133.0f/255.0f alpha:1]
                forKeyPath:@"_placeholderLabel.textColor"];
    
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureBtnClicked)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [tapGesture setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tapGesture];
}
-(void)tapGestureBtnClicked
{
    [self.view endEditing:YES];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch
{
    NSLog(@"%@",[touch.view class]);
    if([touch.view class] == tblVw.class){
        return YES;
    }
    return YES;
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden=YES;
    currentPage = 1;
    if([CommonFunctions reachabiltyCheck])
    {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getBreed:strBreed andSearchKeyword:nil];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction Button Method

- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextBtnClicked:(id)sender {
    
    [self goToNextView];
}

-(void)goToNextView{
    if (petBreedID.length>=1) {
        PetInfoVC *pivc=[[PetInfoVC alloc]init];
        pivc.petType=strBreed;
        pivc.petBreed=petBreedID;
        [self.navigationController pushViewController:pivc animated:YES];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please select a pet breed."];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableView Delegate Method

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return breedDataArr.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0f*SCREEN_XScale;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"SelectBreedCustomeCell";
    SelectBreedCustomeCell *cell=[[SelectBreedCustomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.lblBreedName.text=[[[[breedDataArr objectAtIndex:indexPath.row] objectForKey:@"Breed"] objectForKey:@"title"]capitalizedString];
    cell.imgVwSelected.tag=indexPath.row;
    

    NSLog(@"** nextPage ** %ld", (long)nextPage);
    if ((indexPath.row == [breedDataArr count]-1) && ([breedDataArr count] != totalPost) && nextPage != 0)
    {
        NSString *strTxtSearch = [txtSearchbar.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
        [self getBreed:strBreed andSearchKeyword:[strTxtSearch isEqualToString:@""]?nil:strTxtSearch];
    }
    
    return cell;
}

#pragma mark - UITextField Delegate Method

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSLog(@"%lu",(unsigned long)range.location);
 
    return YES;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
             [tblVw reloadData];
    SelectBreedCustomeCell *cell = (SelectBreedCustomeCell*)[tableView cellForRowAtIndexPath:indexPath];
    if (cell.imgVwSelected.tag==indexPath.row) {

//            [cell.imgVwSelected setImage:[UIImage imageNamed:@"SlctBreedSelction"]];
        petBreedID=[[[breedDataArr objectAtIndex:indexPath.row] objectForKey:@"Breed"] objectForKey:@"id"];
       
    }
    [self goToNextView];
}

#pragma mark - WebServiceAPI
-(void)getBreed:(NSString *)breedType andSearchKeyword:(NSString *)keyword
{
    
    NSLog(@"search--keyword :: %@",keyword);
    
    if(totalPost==[breedDataArr count])
    {
        if (currentPage==totalPage)
        {
            return;
        }
    }
    
NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:breedType,@"pet",keyword?keyword:@"", @"keyword", nil];
    
    NSString *url = [NSString stringWithFormat:@"getBreeds/page:%d",currentPage];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.173/petlox_svn/mobile/getBreeds
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"Responce Code : %ld",(long)operation.response.statusCode);
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        
    
        if(operation.response.statusCode==200)
        {
            if(responseDict==Nil)        {
                [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
                
            }
            
            else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            {
                
                NSArray *arrListing = [responseDict objectForKey:@"data"];
                if(keyword && currentPage == 1){
                    arrAllrecords = [[NSMutableArray alloc] init];
                    arrAllrecords = [arrListing mutableCopy];
                } else {
                    if (arrListing.count > 0)
                    {
                        if (currentPage == 1)
                        {
                            if (arrAllrecords.count > 0)
                            {
                                [arrAllrecords removeAllObjects];
                            }
                            if (arrAllrecords == nil)
                            {
                                arrAllrecords = [NSMutableArray array];
                            }
                            arrAllrecords = [arrListing mutableCopy];
                        }
                        else
                        {
                            [arrAllrecords addObjectsFromArray:arrListing];
                        }
                    }
                }
                
                 breedDataArr = arrAllrecords;
                if (breedDataArr.count>0) {
                    [norecordFound setHidden:YES];
                }
                else
                {
                    [norecordFound setHidden:NO];
                }

                totalPage=[[responseDict objectForKey:@"total_page"] integerValue];
                totalPost=[[responseDict objectForKey:@"total_results"] integerValue];
                nextPage = [[responseDict objectForKey:@"nextpage"] integerValue];
                currentPage=[[responseDict objectForKey:@"current_page"] integerValue];
                
                [tblVw reloadData];
                if (currentPage<totalPage) {
                currentPage++;
                }
                
                
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
                
            }
        }
        else if(operation.response.statusCode==206)
        {
            [breedDataArr removeAllObjects];
            if (breedDataArr.count>0) {
                [norecordFound setHidden:YES];
            }
            else
            {
                [norecordFound setHidden:NO];
            }
            [tblVw reloadData];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo   responce%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              //  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self getBreed:strBreed andSearchKeyword:keyword?keyword:nil];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}

- (IBAction)searchTextFiled:(UITextField *)searchText {
    
    breedDataArr=[[NSMutableArray alloc]init];
    
    NSString *strTxtSearch = [searchText.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    currentPage = 1;
    [self getBreed:strBreed andSearchKeyword:strTxtSearch];
//    if (strTxtSearch.length>0)
//    {
//        [self getBreed:strBreed andSearchKeyword:strTxtSearch];
//       // [[[[breedDataArr objectAtIndex:indexPath.row] objectForKey:@"Breed"] objectForKey:@"title"]
//         
////        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"self.Breed.title contains[cd] %@",strTxtSearch];
////        breedDataArr = [[arrAllrecords filteredArrayUsingPredicate:resultPredicate] mutableCopy];
////        
////        [tblVw reloadData];
////        
////        if (breedDataArr.count>0) {
////            [norecordFound setHidden:YES];
////        }
////        else
////        {
////            [norecordFound setHidden:NO];
////        }
////
//            }
//    else
//    {
//        breedDataArr = arrAllrecords;
//        [tblVw reloadData];
//        if (breedDataArr.count>0) {
//            [norecordFound setHidden:YES];
//        }
//        else
//        {
//            [norecordFound setHidden:NO];
//        }
//
//        
//    }
    
}

@end
