//
//  ServiceProviderVC.h
//  DemoPetLox
//
//  Created by Chagan Singh on 14/09/15.
//  Copyright (c) 2015 Chagan Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface ServiceProviderVC : UIViewController
{
    IBOutlet UILabel *lblName;
    IBOutlet UILabel *lblDistance;
    IBOutlet UILabel *lblNoOfRewies;
    IBOutlet UILabel *lblAddress;
    IBOutlet UIImageView *imgProfileView;
    IBOutlet UINavigationItem *navItemTitle;
    
}
- (IBAction)btnBackPressed:(id)sender;
- (IBAction)btnNextPressed:(id)sender;
- (IBAction)btnFavratePressed:(id)sender;
- (IBAction)btnReverse:(id)sender;


@property (strong,nonatomic) NSMutableDictionary *dictUserDdetails;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *Stars;
@property (strong, nonatomic) IBOutlet GMSMapView *mapView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *btnReserve;
@end
