//
//  ServiceProviderVC.m
//  DemoPetLox
//
//  Created by Chagan Singh on 14/09/15.
//  Copyright (c) 2015 Chagan Singh. All rights reserved.
//

#import "ServiceProviderVC.h"
#import "UIImageView+WebCache.h"
#import "ReserveNowVC.h"
#import "OperationHoursVC.h"
#import "WriteReviewVC.h"
#import "ViewPhotoVC.h"
#import "ReviewShowVC.h"
#import "MapVC.h"

@interface ServiceProviderVC ()<UIAlertViewDelegate>
{
    IBOutlet UIButton *btnFavrite;
    
}
@end

@implementation ServiceProviderVC

@synthesize mapView,dictUserDdetails;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
wbServiceCount=1;
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationController.navigationBar setHidden:NO];
 
    UIBarButtonItem *backButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked)];
    backButton.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:backButton];
    
    
    
    lblName.adjustsFontSizeToFitWidth = true;
    
    mapView.mapType = kGMSTypeNormal;
    mapView.delegate = self;
    [APPDELEGATE createNormalUserPetReserveTabBar];
    [APPDELEGATE showPetReservationTabBar:YES];
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self setViewCount];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }
    
    [self mapWithFrameFortLocation];
    [self userDetailsSetup];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [APPDELEGATE showPetReservationTabBar:YES];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if(K_SCREEN_WIDTH<=320){
        self.btnReserve.titleLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:14.0];
    }
    
    self.btnReserve.layer.cornerRadius = self.btnReserve.frame.size.width/2;
}

-(void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)mapWithFrameFortLocation
{
    float lattitude=[[dictUserDdetails valueForKey:@"lat"] floatValue];
    float longitutde=[[dictUserDdetails valueForKey:@"lon"] floatValue];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lattitude
                                                            longitude:longitutde
                                                                 zoom:10];
    mapView.camera=camera;
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(lattitude, longitutde);
    marker.title = [NSString stringWithFormat:@"%@",[dictUserDdetails valueForKey:@"business_name"]];
    marker.snippet = [NSString stringWithFormat:@"%@",[dictUserDdetails valueForKey:@"address"]];
    
    marker.map = mapView;
}




-(void)userDetailsSetup
{
    NSLog(@"%@",dictUserDdetails);
    NSString *firstName=[dictUserDdetails valueForKey:@"business_name"];
    lblName.text=[[NSString stringWithFormat:@"%@",firstName] capitalizedString];
    navItemTitle.title=[lblName.text capitalizedString];
    [self.navigationItem setTitle:[lblName.text capitalizedString]];
    lblAddress.text=[dictUserDdetails valueForKey:@"address"];
    lblName.textAlignment=NSTextAlignmentLeft;
    
    if ([dictUserDdetails valueForKey:@"review_count"]!=[NSNull null])
    {
        int reviewCount=[[dictUserDdetails valueForKey:@"review_count"]intValue];
        if (reviewCount>1) {
               lblNoOfRewies.text=[NSString stringWithFormat:@"%@ reviews",[dictUserDdetails valueForKey:@"review_count"]];
        }
        else
        {
               lblNoOfRewies.text=[NSString stringWithFormat:@"%@ review",[dictUserDdetails valueForKey:@"review_count"]];
        }
    }
    else
    {
        lblNoOfRewies.text=@"0 review";
    }
    
    
    NSLog(@"%f",imgProfileView.frame.size.height);
    
    float circulerValue=30.5;
    if (IS_IPHONE_6)
    {
         circulerValue=circulerValue+5;
    }
    else if (IS_IPHONE_6_PLUS)
    {
        circulerValue=circulerValue+10;
    }

    
//    imgProfileView.layer.cornerRadius=imgProfileView.frame.size.width/2;
    imgProfileView.layer.borderWidth=2;
    imgProfileView.layer.borderColor=[UIColor clearColor].CGColor;
    imgProfileView.layer.masksToBounds=YES;
 
    
    [imgProfileView sd_setImageWithURL:[NSURL URLWithString:[dictUserDdetails valueForKey:@"image"]]
                           placeholderImage:[UIImage imageNamed:@"DefaultProfileImg"]];
    
    if ([dictUserDdetails valueForKey:@"distance"] !=[NSNull null])
    {
        NSString *strDistance=[dictUserDdetails valueForKey:@"distance"];
        if (strDistance.length>0)
        {
            NSString *strMile=[NSString stringWithFormat:@"%@",strDistance];
            strMile=[strMile stringByReplacingOccurrencesOfString:@" " withString:@""];
        lblDistance.text=[NSString stringWithFormat:@"%@",strMile];
        }
    }
   
    
    if ([self isNotNull:[dictUserDdetails objectForKey:@"fav"]]) {
        NSString *favFlagValue=[NSString stringWithFormat:@"%@",[dictUserDdetails objectForKey:@"fav"]];
        if ([favFlagValue isEqualToString:@"1"]) {
            [btnFavrite setImage:[UIImage imageNamed:@"favorite_foucs_icon"] forState:UIControlStateNormal];
            btnFavrite.tag=0;
        }
        else
        {
            [btnFavrite setImage:[UIImage imageNamed:@"favorite_icon"] forState:UIControlStateNormal];
                        btnFavrite.tag=1;
        }
    }

    if ([dictUserDdetails valueForKey:@"avg_rate"] !=[NSNull null])
    {
        int stars=[[dictUserDdetails valueForKey:@"avg_rate"] intValue];
        
        for (int i=0; i<stars; i++)
        {
            [[self.Stars objectAtIndex:i] setHighlighted:YES];
        }
        
    }

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnNextPressed:(id)sender {
}

- (IBAction)btnFavratePressed:(id)sender {
    
    NSLog(@"%@",dictUserDdetails);
        UIButton *btn=(UIButton *)sender;
    if (btnFavrite.tag==1) {
        [btnFavrite setImage:[UIImage imageNamed:@"favorite_foucs_icon"] forState:UIControlStateNormal];
        [self favBusiness];

        btn.tag=0;
    }
    else if (btnFavrite.tag==0)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Petlox"
                                                            message:@"Do you want to unfavorite this business."
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:@"OK", nil];
        alertView.tag=100;
        [alertView show];
//        [CommonFunctions alertTitle:@"" withMessage:@"Do you want to unfavorite this business." withDelegate:self withTag:100];
    }
}

- (IBAction)btnReverse:(id)sender {
    ReserveNowVC *rnvc=[[ReserveNowVC alloc]initWithNibName:@"ReserveNowVC" bundle:nil];
    rnvc.dictUserDetails=dictUserDdetails;
    [self.navigationController pushViewController:rnvc animated:YES];
}

//- (IBAction)btnWriteReview:(id)sender {
//    WriteReviewVC *wrvc=[[WriteReviewVC alloc]init];
//    wrvc.dictBusinessDetail=dictUserDdetails;
//    [CommonFunctions showActivityIndicatorWithText:@""];
////    [self checkUserReviewStatus];
//   // [self.navigationController pushViewController:wrvc animated:YES];
//}

#pragma mark- WebServices API..

-(void)favBusiness
{
    NSMutableDictionary *param;
NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"user_id",[dictUserDdetails objectForKey:@"user_id"],@"business_id", nil];
    
    NSString *url = [NSString stringWithFormat:@"favUnfav"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/favUnfav
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
           if([[[responseDict objectForKey:@"favstatus"] stringValue]isEqualToString:@"1"])
           {
               [btnFavrite setImage:[UIImage imageNamed:@"favorite_foucs_icon"] forState:UIControlStateNormal];
               
               NSMutableDictionary *tempDict=[[NSMutableDictionary alloc]initWithDictionary:[dictUserDdetails mutableCopy]];
               
               NSLog(@"%@",[responseDict valueForKey:@"favstatus"]);
               [tempDict setObject:[responseDict valueForKey:@"favstatus"] forKey:@"fav"];
               
               dictUserDdetails=tempDict;
               btnFavrite.tag=0;
           }
            else
            {
                [btnFavrite setImage:[UIImage imageNamed:@"favorite_icon"] forState:UIControlStateNormal];
                NSMutableDictionary *tempDict=[[NSMutableDictionary alloc]initWithDictionary:[dictUserDdetails mutableCopy]];
                
                NSLog(@"%@",[responseDict valueForKey:@"favstatus"]);
                [tempDict setObject:[responseDict valueForKey:@"favstatus"] forKey:@"fav"];
                
                dictUserDdetails=tempDict;
            }
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self favBusiness];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}

#pragma mark- WebServices API..


-(void)setViewCount
{
    NSMutableDictionary *param;
NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"user_id",[dictUserDdetails objectForKey:@"user_id"],@"business_id", nil];
    
    NSString *url = [NSString stringWithFormat:@"userViews"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/userViews
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            
        }
        else
        {
           // [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self setViewCount];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}





- (IBAction)mapbtnClicked:(id)sender {
    MapVC *mapVC=[[MapVC alloc]init];
    mapVC.dictUserData=dictUserDdetails;
    [self.navigationController pushViewController:mapVC animated:YES];
}
#pragma mark - UIAlertView Delegate methods
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1) {
        if (alertView.tag==100) {
            [btnFavrite setImage:[UIImage imageNamed:@"favorite_icon"] forState:UIControlStateNormal];
            btnFavrite.tag=1;
            [self favBusiness];
        }
        
    }
}
@end
