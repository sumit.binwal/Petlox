//
//  SettingVC.m
//  Petlox
//
//  Created by Sumit Sharma on 21/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//
#import "SignInVC.h"
#import "SettingVC.h"
#import "PrivacyPolicyVC.h"
#import "ChangePasswordVC.h"
#import "NotificationVC.h"
#import "SettingsCell.h"
#import "MainHomeVC.h"

@interface SettingVC ()<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *tblViewSettings;
    IBOutlet UIButton *leftBtnClicked;
}
@end

@implementation SettingVC

-(AppDelegate *)appDelegateObj
{
    return (AppDelegate *) [[UIApplication sharedApplication]delegate];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    wbServiceCount=1;
    [CommonFunctions setNavigationBar:self.navigationController];

    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self.viewDeckController action:@selector(toggleLeftView)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];    // Do any additional setup after loading the view from its nib.
    [self.navigationItem setTitle:@"Settings"];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - IBAction Methods
- (IBAction)logoutBtnClicked:(id)sender {
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self logoutUser];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network"];
    }
    
}

- (IBAction)privacySetting:(id)sender {
    PrivacyPolicyVC *ppvc=[[PrivacyPolicyVC alloc]init];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:UD_BUSINESS_TYPE]isEqualToString:@"user"]) {
    ppvc.strUrlString=@"http://mymeetingdesk.com/mobile/petlox/pages/privacy_policy";
    }
    else
    {
        
         ppvc.strUrlString=@"http://mymeetingdesk.com/mobile/petlox/pages/privacy_business_policy";
    }
    
    [self.navigationController pushViewController:ppvc animated:YES];
}
- (IBAction)notificationBtnclicked:(id)sender
{
    NotificationVC *cpvc=[[NotificationVC alloc]init];
    [self.navigationController pushViewController:cpvc animated:YES];
}
- (IBAction)changePasswordBtnClicked:(id)sender {
    ChangePasswordVC *cpvc=[[ChangePasswordVC alloc]init];
    [self.navigationController pushViewController:cpvc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)logoutUser
{
    
    NSString *url = [NSString stringWithFormat:@"logout"];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"user_id", nil];
    
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/userReview
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_TOKEN_ID];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_BUSINESS_TYPE];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            MainHomeVC *sivc=[[MainHomeVC alloc]init];
            APPDELEGATE.navController = [[UINavigationController alloc] initWithRootViewController:sivc];
            APPDELEGATE.window.rootViewController = APPDELEGATE.navController;

            
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self logoutUser];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}

#pragma mark - UITableViewDelegate / DataSource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float rowHeight = 0.0f;
    rowHeight = (44 * [self appDelegateObj].window.bounds.size.height)/568;
    return rowHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 4;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *settingsCell   = @"SettingsCell";
    
    SettingsCell *cell = (SettingsCell *)[tableView dequeueReusableCellWithIdentifier:settingsCell];
    
    if (cell == nil)
    {
        cell = (SettingsCell*)[[[NSBundle mainBundle] loadNibNamed:@"SettingsCell" owner:self options:nil] objectAtIndex:0];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    if (indexPath.row == 0)
    {
        cell.lblTxt.text = @"Notifications";
    }
    else if (indexPath.row == 1)
    {
        cell.lblTxt.text = @"Privacy settings";
    }
    else if (indexPath.row == 2)
    {
        cell.lblTxt.text = @"Change password";
    }
    else if (indexPath.row == 3)
    {
        cell.lblTxt.text = @"Sign out";
        cell.lblTxt.textColor = [UIColor colorWithRed:222/255.0 green:0/255.0 blue:0/255.0 alpha:1.0];
        
        cell.imgViewArrow.image = [UIImage imageNamed:@"redArrow"];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        [self notificationBtnclicked:nil];
    }
    else if (indexPath.row == 1)
    {
        [self privacySetting:nil];
    }
    else if (indexPath.row == 2)
    {
        [self changePasswordBtnClicked:nil];
    }
    else if (indexPath.row == 3)
    {
        [self logoutBtnClicked:nil];
    }
}

@end
