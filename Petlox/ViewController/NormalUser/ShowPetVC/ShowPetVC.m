//
//  ShowPetVC.m
//  Petlox
//
//  Created by Sumit Sharma on 03/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "ShowPetVC.h"
#import "ConfirmReservationVC.h"
#import "ShowPetCellVC.h"

#define defaultIndex 9999

@interface ShowPetVC ()<UIAlertViewDelegate>
{
    
    IBOutlet UIImageView *imgStar1;
    IBOutlet UIImageView *imgStar2;
    IBOutlet UIImageView *imgStar3;
    IBOutlet UIImageView *imgStar4;
    IBOutlet UIImageView *imgStar5;
    IBOutlet UILabel *lblBusinessName;
    IBOutlet UIButton *btnFav;
    UILabel *label;
    IBOutlet UILabel *lblBusinessDistance;
    IBOutlet UILabel *lblBusinessAddress;
    IBOutlet UILabel *lblReview;
    IBOutlet UIImageView *imgVwBsinssProfileImg;
    NSMutableDictionary *dictReservation;
    IBOutlet UIImageView *imgVwPetImages;
    IBOutlet UIView *vwBusinessDiscription;
    NSMutableDictionary *dictSelectedPetInfo;
    IBOutlet NSLayoutConstraint *constraintWhitespace;
    int selectedItemIndex;

}

@end
@implementation ShowPetVC
@synthesize dictBusinessDetail,arrPets;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    wbServiceCount=1;
    selectedItemIndex = defaultIndex;
    [self setUpView];   
  
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getPetDetail];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }
    
    if ([UIScreen mainScreen].bounds.size.height<568) {
        constraintWhitespace.constant=1.0f;
    }
    // Do any additional setup after loading the view from its nib.
    
    [self.petsCollectionView registerClass:[ShowPetCellVC class] forCellWithReuseIdentifier:@"showPetCell"];
    UINib *cellNib = [UINib nibWithNibName:@"ShowPetCell" bundle:nil];
    [self.petsCollectionView registerNib:cellNib forCellWithReuseIdentifier:@"showPetCell"];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if(K_SCREEN_HEIGHT<=568){
        self.btnContinue.titleLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:14.0];
    }
    self.btnContinue.layer.cornerRadius = self.btnContinue.frame.size.width/2;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
  
    //free up memory by releasing subviews
    self.carousel = nil;
}
-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];

    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
    [self.navigationItem setTitle:@"Choose Your Pet"];
    
}
#pragma mark - IBAction Methods
- (IBAction)favBtnClicked:(id)sender {
    
    UIButton *btn=(UIButton *)sender;
    if (btnFav.tag==0) {
        [btnFav setImage:[UIImage imageNamed:@"FavButton"] forState:UIControlStateNormal];
        [self favBusiness];
        
        btn.tag=1;
    }
    else if (btnFav.tag==1)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Petlox"
                                                            message:@"Do you want to unfavorite this business."
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:@"OK", nil];
        alertView.tag=100;
        [alertView show];
    
        //        [CommonFunctions alertTitle:@"" withMessage:@"Do you want to unfavorite this business." withDelegate:self withTag:100];
    }

    
}

-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)cntinueBtnClicked:(id)sender
{
    if (arrPets.count>0) {
        ConfirmReservationVC *crvc=[[ConfirmReservationVC alloc]initWithNibName:@"ConfirmReservationVC" bundle:nil];
        crvc.dictBusinessUser=dictBusinessDetail;
        
        crvc.dictPetDetail=dictSelectedPetInfo;
        [self.navigationController pushViewController:crvc animated:YES];
    }
}

#pragma mark- WebServices API..

-(void)getPetDetail
{
    
NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    
    NSMutableDictionary *param;
    
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"user_id",[dictBusinessDetail objectForKey:@"user_id"],@"business_id",[NSString stringWithFormat:@"%f",CurrentLatitude],@"lat",[NSString stringWithFormat:@"%f",CurrentLongitude],@"lon", nil];
    
    NSString *url = [NSString stringWithFormat:@"userpetList"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/userpetList
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            arrPets=[responseDict objectForKey:@"data"];
            iCarousel *icrusal=[[iCarousel alloc]init];
            [icrusal reloadData];
            _carousel.delegate=self;
            _carousel.dataSource=self;
             _carousel.type = iCarouselTypeInvertedRotary;
            [self.petsCollectionView reloadData];
            NSLog(@"%@",arrPets);
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self getPetDetail];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}



#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    NSLog(@"%lu",(unsigned long)[arrPets count]);
    return [arrPets count];
    
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
     label = nil;
    NSLog(@"%@",    carousel.preferredFocusedView);

    UIImageView *imgView=nil;
    //create new view if no view is available for recycling
    if (view == nil)
    {
        //don't do anything specific to the index within
        //this `if (view == nil) {...}` statement because the view will be
        //recycled and used with other index values later
        view =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 230.0f, carousel.frame.size.height)];
 
        imgView = [[UIImageView alloc] initWithFrame:view.frame];
        [imgView setFrame:CGRectMake(0, 0, 138.0f, 138.0f)];
        imgView.layer.cornerRadius=imgView.frame.size.width/2;
        imgView.clipsToBounds=YES;
        imgView.layer.borderColor=[[UIColor whiteColor]CGColor];
        imgView.layer.borderWidth=4.0f;
        imgView.layer.shadowColor=[[UIColor colorWithRed:208.0f/255.0f green:214.0f/255.0f blue:217.0f/255.0f alpha:1.0f]CGColor];
        
        // ((UIImageView *)view).layer.shadowColor = [UIColor purpleColor].CGColor;
//        imgView.layer.shadowOffset = CGSizeMake(0, 99);
//        imgView.layer.shadowOpacity = 33;
//        imgView.layer.shadowRadius = 14.0;
        
        [imgView.layer setShadowOffset:CGSizeMake(-5.0, 5.0)];
        [imgView.layer setShadowRadius:10.0];
        [imgView.layer setShadowOpacity:1.0];
        
    
        [imgView setImageWithURL:[NSURL URLWithString:[[arrPets objectAtIndex:index]  objectForKey:@"image"]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//        [imgView setBackgroundColor:[UIColor redColor]];
        imgView.contentMode = UIViewContentModeCenter;
        
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, imgView.frame.size.height+20,view.frame.size.width,20.0f)];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;

        label.font=[UIFont fontWithName:FONT_GULIM size:17.0f];
        label.tag = 100;
        
        [view addSubview:imgView];
        [view addSubview:label];
        
        imgView.center=view.center;
        
        [label bringSubviewToFront:view];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:100];

label.text=@"";
    }
            NSLog(@"%d",        label.tag);
    //set item label
    label.text=@"";
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel

    label.text = [[arrPets objectAtIndex:index] objectForKey:@"name"];
    
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    
    if (option == iCarouselOptionSpacing)
    {
        return value * 0.8;
    }
    return value;
}

-(void)carouselDidEndScrollingAnimation:(iCarousel * __nonnull)carousel
{
    dictSelectedPetInfo=[arrPets objectAtIndex:carousel.currentItemIndex];
    
  //  UIView *curretnView = [carousel itemViewAtIndex:carousel.currentItemIndex];
  //  UILabel *lbl = (UILabel*)[curretnView viewWithTag:100];
  //  lbl.text = [[arrPets objectAtIndex:carousel.currentItemIndex] objectForKey:@"name"];
    if ([[[arrPets objectAtIndex:carousel.currentItemIndex]objectForKey:@"reservation_details"]count]>0) {
        
        dictReservation=[[arrPets objectAtIndex:carousel.currentItemIndex] objectForKey:@"reservation_details"];
        
        [vwBusinessDiscription setHidden:NO];

        lblBusinessAddress.text=[dictReservation objectForKey:@"business_address"];

        imgVwBsinssProfileImg.layer.cornerRadius=imgVwBsinssProfileImg.frame.size.width/2;
        imgVwBsinssProfileImg.clipsToBounds=YES;
        imgVwBsinssProfileImg.layer.borderColor=[[UIColor whiteColor]CGColor];
        imgVwBsinssProfileImg.layer.borderWidth=1;
        
        NSString *imgUrlstr=[dictReservation objectForKey:@"business_image"];
        if (imgUrlstr.length>1) {
            [imgVwBsinssProfileImg setImageWithURL:[NSURL URLWithString:imgUrlstr] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        }
        
        if ([self isNotNull:[dictReservation objectForKey:@"fav"]]) {
            [btnFav setHidden:NO];
            NSString *favValue=[NSString stringWithFormat:@"%@",[dictReservation objectForKey:@"fav"]];
            if ([favValue isEqualToString:@"1"]) {
                [btnFav setImage:[UIImage imageNamed:@"FavButton"] forState:UIControlStateNormal];
                btnFav.tag=1;
            }
            else
            {
                [btnFav setImage:[UIImage imageNamed:@"favorite_icon"] forState:UIControlStateNormal];
                                btnFav.tag=0;
            }
        }
        
        if ([self isNotNull:[dictReservation objectForKey:@"avg_rating"]]) {
            int avgRating=[[dictReservation objectForKey:@"avg_rating"]intValue];
            switch (avgRating) {
                case 0:
                {
                    break;
                }
                    
                case 1:
                {
                    [imgStar1 setImage:[UIImage imageNamed:@"star-icon"]];
                    break;
                }
                case 2:
                {
                    [imgStar1 setImage:[UIImage imageNamed:@"star-icon"]];
                    [imgStar2 setImage:[UIImage imageNamed:@"star-icon"]];
                    break;
                }
                case 3:
                {
                    [imgStar1 setImage:[UIImage imageNamed:@"star-icon"]];
                    [imgStar2 setImage:[UIImage imageNamed:@"star-icon"]];
                    [imgStar3 setImage:[UIImage imageNamed:@"star-icon"]];
                    break;
                }
                case 4:
                {
                    [imgStar1 setImage:[UIImage imageNamed:@"star-icon"]];
                    [imgStar2 setImage:[UIImage imageNamed:@"star-icon"]];
                    [imgStar3 setImage:[UIImage imageNamed:@"star-icon"]];
                    [imgStar4 setImage:[UIImage imageNamed:@"star-icon"]];
                    break;
                }
                case 5:
                {
                    [imgStar1 setImage:[UIImage imageNamed:@"star-icon"]];
                    [imgStar2 setImage:[UIImage imageNamed:@"star-icon"]];
                    [imgStar3 setImage:[UIImage imageNamed:@"star-icon"]];
                    [imgStar4 setImage:[UIImage imageNamed:@"star-icon"]];
                    [imgStar4 setImage:[UIImage imageNamed:@"star-icon"]];
                    break;
                }
                    
                default:
                    break;
            }
            
        }

        lblBusinessName.text=[[dictReservation objectForKey:@"business_name"] capitalizedString];
        lblBusinessDistance.text=[dictReservation objectForKey:@"distance"];
        if ([self isNotNull:[dictReservation objectForKey:@"reviewcount"]]) {
            int reviewCount=[[dictReservation objectForKey:@"review_count"] intValue];
            if (reviewCount>1) {
                lblReview.text=[NSString stringWithFormat:@"%d reviews",reviewCount];
            }
            else
            {
                lblReview.text=[NSString stringWithFormat:@"%d review",reviewCount];
            }
        }
    
    }
    else
    {
                [vwBusinessDiscription setHidden:YES];
    }
    
}

#pragma mark - UICollection View Datasource and Delegate Methods
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [arrPets count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ShowPetCellVC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"showPetCell" forIndexPath:indexPath];
    
    [cell.petImageView setImageWithURL:[NSURL URLWithString:[[arrPets objectAtIndex:indexPath.item]  objectForKey:@"image"]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    cell.lblPetName.text = [[arrPets objectAtIndex:indexPath.item] objectForKey:@"name"];
    
//    cell.petImageView.image = [UIImage imageNamed:@"business_grooming"];
//    cell.lblPetName.text = @"testing";
    
    cell.lblPetName.hidden = YES;
    if(selectedItemIndex == indexPath.item){
        cell.lblPetName.hidden = NO;
        selectedItemIndex = defaultIndex;
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat formattedSide = MIN((124.0/375.0)*[UIScreen mainScreen].bounds.size.width, (124.0/667.0)*[UIScreen mainScreen].bounds.size.height);
    return CGSizeMake(formattedSide, formattedSide);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    selectedItemIndex = indexPath.item;
    dictSelectedPetInfo=[arrPets objectAtIndex:indexPath.item];
    [collectionView reloadData];
}


#pragma mark - WebService API 
#pragma mark- WebServices API..

-(void)favBusiness
{
    NSMutableDictionary *param;
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"user_id",[dictReservation objectForKey:@"business_id"],@"business_id", nil];
    
    NSString *url = [NSString stringWithFormat:@"favUnfav"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/favUnfav
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            if([[[responseDict objectForKey:@"favstatus"] stringValue]isEqualToString:@"1"])
            {
                [btnFav setImage:[UIImage imageNamed:@"FavButton"] forState:UIControlStateNormal];
                btnFav.tag=1;
            }
            else
            {
                [btnFav setImage:[UIImage imageNamed:@"favorite_icon"] forState:UIControlStateNormal];
                
            }
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self favBusiness];
                                              }
                                              else
                                              {
                                                  [CommonFunctions removeActivityIndicator];
                                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}

#pragma mark - UIAlertView Delegate methods
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1) {
        if (alertView.tag==100) {
            [btnFav setImage:[UIImage imageNamed:@"favorite_icon"] forState:UIControlStateNormal];
            btnFav.tag=0;
            [self favBusiness];
        }
        
    }
}
@end
