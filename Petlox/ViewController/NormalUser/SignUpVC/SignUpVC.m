//
//  SignUpVC.m
//  Petlox
//
//  Created by Sumit Sharma on 27/07/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "SignUpVC.h"
#import "ChoosePetVC.h"
#import "CSFacebook.h"
#import "CSStream.h"

@interface SignUpVC ()<UITextFieldDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    IBOutlet UIView *txtVw1;
    IBOutlet UIView *txtVw2;
    IBOutlet UIView *txtVw3;
    IBOutlet UIView *txtVw4;
    IBOutlet UIView *txtVw5;
    IBOutlet UIView *txtVw6;
    IBOutlet UIView *vwProfileImg;
    IBOutlet UIView *cntainerVw;
    IBOutlet UIView *toolbarVw;
    
    IBOutlet UIScrollView *scrllVw;
    
    IBOutlet UITextField *txtFirstName;
    IBOutlet UITextField *txtLName;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtPhnNumber;
    IBOutlet UITextField *txtPass;
    IBOutlet UITextField *txtCnfirmPass;
    
    IBOutlet UIImageView *profileImg;
    
}
@end

@implementation SignUpVC

#pragma mark - View Controller Life Cycle Method
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    // Do any additional setup after loading the view from its nib.
    [self setupGoogleintegration];
    
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    txtVw1.layer.cornerRadius = 5.0f;
    txtVw2.layer.cornerRadius = 5.0f;
    txtVw3.layer.cornerRadius = 5.0f;
    txtVw4.layer.cornerRadius = 5.0f;
    txtVw5.layer.cornerRadius = 5.0f;
    txtVw6.layer.cornerRadius = 5.0f;
    if(K_SCREEN_WIDTH<=320){
        [self.profilePhotoLabel setFont:[UIFont fontWithName:FONT_OPENSANS size:14]];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self disconnect];
    [[CSTwitterEngine sharedEngine] clearAccessToken];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)setUpView
{
    // Change Textfield Place Holder Color
    [txtEmail setValue:[UIColor colorWithRed:129.0f/255.0f green:128.0f/255.0f blue:128.0f/255.0f alpha:1]
            forKeyPath:@"_placeholderLabel.textColor"];
    [txtCnfirmPass setValue:[UIColor colorWithRed:129.0f/255.0f green:128.0f/255.0f blue:128.0f/255.0f alpha:1]
                 forKeyPath:@"_placeholderLabel.textColor"];
    [txtFirstName setValue:[UIColor colorWithRed:129.0f/255.0f green:128.0f/255.0f blue:128.0f/255.0f alpha:1]
                forKeyPath:@"_placeholderLabel.textColor"];
    [txtLName setValue:[UIColor colorWithRed:129.0f/255.0f green:128.0f/255.0f blue:128.0f/255.0f alpha:1]
            forKeyPath:@"_placeholderLabel.textColor"];
    [txtPass setValue:[UIColor colorWithRed:129.0f/255.0f green:128.0f/255.0f blue:128.0f/255.0f alpha:1]
           forKeyPath:@"_placeholderLabel.textColor"];
    [txtPhnNumber setValue:[UIColor colorWithRed:129.0f/255.0f green:128.0f/255.0f blue:128.0f/255.0f alpha:1]
                forKeyPath:@"_placeholderLabel.textColor"];
    
    txtPhnNumber.inputAccessoryView=toolbarVw;
    profileImg.clipsToBounds=YES;
    
    
    //Set Navigation And Navigation Bar Buttons
    [CommonFunctions setNavigationBar:self.navigationController];
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(bckBtnClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
//    UIBarButtonItem *rgtbtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"nextIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(nextbtnClicked:)];    
    
    UIBarButtonItem *rgtbtn = [[UIBarButtonItem alloc]initWithTitle:@"Sign Up" style:UIBarButtonItemStylePlain target:self action:@selector(nextbtnClicked:)];
    [rgtbtn setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"OpenSans" size:18]} forState:UIControlStateNormal];
    [self.navigationItem setRightBarButtonItem:rgtbtn];
    
    //UITap Gesture
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureBtnClicked)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];
    
    
    [self eventListnerForEventorMemberChange];
}


#pragma mark - UIScrollView Methods
-(void)scrollViewToCenterOfScreen:(UITextField *)textField cntainter:(UIView *)view
{
    [scrllVw setScrollEnabled:YES];
    float difference;
    if (scrllVw.contentSize.height == 600)
        difference = 50.0f;
    else
        difference = 50.0f;
    CGFloat viewCenterY = textField.center.y+view.frame.origin.y;
    NSLog(@"center.y -- %f",textField.center.y);
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    CGFloat avaliableHeight = applicationFrame.size.height - 5.0f;
    NSLog(@"avaliableHeight---%f",applicationFrame.size.height);
    
    CGFloat y = viewCenterY - avaliableHeight / 10.0f;
    if (y < 0)
        y = 0;
    NSLog(@"%f",y);
    [scrllVw setContentOffset:CGPointMake(0, y) animated:YES];
    [scrllVw setContentSize:CGSizeMake(320.0f, 630.0f)];
}

-(void)scrollToNormalView
{
    [scrllVw setContentOffset:CGPointZero];
    [scrllVw setScrollEnabled:NO];
}

#pragma mark - UITextField Delegate Methods
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSLog(@"%lu",(unsigned long)range.location);
    if (textField==txtFirstName ||textField==txtLName) {
        if (range.location>20) {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else if(textField==txtPhnNumber) {
        if (range.location>12) {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else
    {
        return YES;
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField==txtFirstName) {
        [self scrollViewToCenterOfScreen:textField cntainter:txtVw1];
    }
    else if (textField==txtLName)
    {
        [self scrollViewToCenterOfScreen:textField cntainter:txtVw2];
    }
    else if (textField==txtEmail)
    {
        [self scrollViewToCenterOfScreen:textField cntainter:txtVw3];
    }
    else if (textField==txtPhnNumber)
    {
        [self scrollViewToCenterOfScreen:textField cntainter:txtVw4];
    }
    else if (textField==txtPass)
    {
        [self scrollViewToCenterOfScreen:textField cntainter:txtVw5];
    }
    else if (textField==txtCnfirmPass)
    {
        [self scrollViewToCenterOfScreen:textField cntainter:txtVw6];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (txtFirstName==textField) {
        [txtLName becomeFirstResponder];
    }
    else if (txtLName==textField)
    {
        [txtEmail becomeFirstResponder];
    }
    else if (txtEmail==textField)
    {
        [txtPhnNumber becomeFirstResponder];
    }
    else if (txtPass==textField)
    {
        [txtCnfirmPass becomeFirstResponder];
    }
    else if (txtCnfirmPass==textField)
    {
        [textField resignFirstResponder];
        [self scrollToNormalView];
    }
    return YES;
}

#pragma mark - Validation For Textfield
-(BOOL)isTextFieldValidate
{
    if (![CommonFunctions isValueNotEmpty:txtFirstName.text]) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter first name."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtLName.text]) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter last name."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtEmail.text]) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter email address."];
        return NO;
    }
    else if (![CommonFunctions IsValidEmail:txtEmail.text]) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter valid email address."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtPhnNumber.text]) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter phone number."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtPass.text]) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter password."];
        return NO;
    }
    else if (txtPass.text.length<6) {
        [CommonFunctions alertTitle:@"" withMessage:@"Password length between 6 to 14 characters."];
        return NO;
    }
    else if (![txtPass.text isEqualToString:txtCnfirmPass.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Password and confirm password does not match."];
        return NO;
    }
    return YES;
}


#pragma mark - UINavigation Controller Delegate Method

-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    navigationController.navigationBar.backgroundColor=[UIColor colorWithRed:71.0f/255.0f green:13.0f/255.0f blue:52.0f/255.0f alpha:1];
    
    UIView *statusView=[[UIView alloc]initWithFrame:CGRectMake(0, -[UIApplication sharedApplication].statusBarFrame.size.height,[UIApplication sharedApplication].statusBarFrame.size.width, [UIApplication sharedApplication].statusBarFrame.size.height)];
    [statusView setBackgroundColor:[UIColor colorWithRed:71.0f/255.0f green:13.0f/255.0f blue:52.0f/255.0f alpha:1]];
    [navigationController.navigationBar addSubview:statusView];
    [navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:FONT_GULIM size:18.85f]}];
    
    viewController.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
}


#pragma mark - UIActionSheet Delegate Method

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        UIImagePickerController *imgPicker=[[UIImagePickerController alloc]init];
        imgPicker.delegate=self;
        imgPicker.sourceType=UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imgPicker animated:YES completion:nil];
    }
    else if (buttonIndex==1)
    {
        UIImagePickerController *imgPicker=[[UIImagePickerController alloc]init];
        imgPicker.delegate=self;
        imgPicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:imgPicker animated:YES completion:nil];
        
    }
}

#pragma mark - UIImagePickerCntroller Delegate Methods

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *imageEdited=[self scaleAndRotateImage:[info objectForKey:UIImagePickerControllerOriginalImage]];
    
    //Compress Image
    NSData *imgData4 = UIImageJPEGRepresentation(imageEdited, 0.3f);
    
    // Don't convert NSData back to UIImage before writing to disk
    
        profileImg.image=[UIImage imageWithData:imgData4];

    [self dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark-IBAction Methods
- (IBAction)bckBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)profileImgBtnClicked:(id)sender {
    [self.view endEditing:YES];
    UIActionSheet *actnSheet=[[UIActionSheet alloc]initWithTitle:@"Choose Image" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose From Gallery", nil];
    [actnSheet showInView:self.view];
}

-(IBAction)nextbtnClicked:(id)sender
{
    [self.view endEditing:YES];
    [scrllVw setContentOffset:CGPointZero];
    if ([self isTextFieldValidate])
    {
        if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self submitUserRegisterDetail];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
        }
    }
}

- (IBAction)toolbarNextBtnClicked:(id)sender {
    [txtPass becomeFirstResponder];
    
}
-(void)tapGestureBtnClicked
{
    [self.view endEditing:YES];
    [self scrollToNormalView];
}


#pragma mark- WebServices API..
//Submit User details
-(void)submitUserRegisterDetail
{
    
    NSMutableDictionary *param;
    NSString *url;
    if (pushDeviceToken.length==0)
    {
        pushDeviceToken=[UserDefaults objectForKey:@"deviceToken"];
    }
    if ([[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID])
    {
        param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CommonFunctions trimSpaceInString:txtFirstName.text],@"first_name",[CommonFunctions trimSpaceInString:txtLName.text],@"last_name",[CommonFunctions trimSpaceInString:txtEmail.text],@"email",[CommonFunctions trimSpaceInString:txtPass.text],@"password",@"user",@"usertype",[CommonFunctions trimSpaceInString:txtPhnNumber.text],@"phone",[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"sid",[NSString stringWithFormat:@"%f",CurrentLatitude],@"lat",[NSString stringWithFormat:@"%f",CurrentLongitude],@"lon",@"iphone",@"device_type",pushDeviceToken,@"device_token", nil];
        
    }
    else
    {
        param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CommonFunctions trimSpaceInString:txtFirstName.text],@"first_name",[CommonFunctions trimSpaceInString:txtLName.text],@"last_name",[CommonFunctions trimSpaceInString:txtEmail.text],@"email",[CommonFunctions trimSpaceInString:txtPass.text],@"password",@"user",@"usertype",[CommonFunctions trimSpaceInString:txtPhnNumber.text],@"phone",[NSString stringWithFormat:@"%f",CurrentLatitude],@"lat",[NSString stringWithFormat:@"%f",CurrentLongitude],@"lon",@"iphone",@"device_type",pushDeviceToken,@"device_token",@"iphone",@"device_type", nil];
        
    }
    
    url = [NSString stringWithFormat:@"%@signup",serverURL];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:url]];
    
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    AFHTTPRequestOperation *op = [manager POST:@"" parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if (profileImg.image != nil)
        {
            NSData *imageData = UIImageJPEGRepresentation(profileImg.image, 0.8f);
            
            [formData appendPartWithFileData:imageData name:@"image" fileName:@"ProfileImage.jpg" mimeType:@"image/jpeg"];
        }
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        
        if (responseObject != nil)
        {
            NSLog(@"Profile  responseObject error: %@",error);
            
            NSDictionary *jsonDic = (NSDictionary *)responseObject;
            NSLog(@"Profile  JSON: %@",jsonDic);
            
            if (jsonDic != nil)
            {
                if ([[jsonDic objectForKey:@"replyCode"] isEqualToString:@"success"])
                {
                    ChoosePetVC *cbvc=[[ChoosePetVC alloc]init];
                    [[NSUserDefaults standardUserDefaults]setObject:[jsonDic objectForKey:@"token"] forKey:UD_TOKEN_ID];
                    [[NSUserDefaults standardUserDefaults]setObject:[jsonDic objectForKey:@"user_id"] forKey:UD_CLAIM_ID];
                    [[NSUserDefaults standardUserDefaults]setObject:[jsonDic objectForKey:@"first_name"] forKey:UD_FIRST_NAME];
                    [[NSUserDefaults standardUserDefaults]setObject:[jsonDic objectForKey:@"last_name"] forKey:UD_LAST_NAME];
                    [[NSUserDefaults standardUserDefaults]setObject:[jsonDic objectForKey:@"image"] forKey:UD_USER_IMG];
                    [[NSUserDefaults standardUserDefaults]setObject:[jsonDic objectForKey:@"type"] forKey:UD_BUSINESS_TYPE];
                    [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_STATUS_COMPLETE];
                    
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    [self.navigationController pushViewController:cbvc animated:YES];
                }
                else
                {
                    [CommonFunctions alertTitle:@"" withMessage:[jsonDic objectForKey:@"replyMsg"]];
                }
                
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[jsonDic objectForKey:@"replyMsg"]];
            }
        }
        [CommonFunctions removeActivityIndicator];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
        [CommonFunctions removeActivityIndicator];
        
    }];
    [op start];
    
}



#pragma mark- Social Logins

-(void)eventListnerForEventorMemberChange
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeProfileChange:) name:FBSDKProfileDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeTokenChange:) name:FBSDKAccessTokenDidChangeNotification object:nil];
    
    [FBSDKAccessToken setCurrentAccessToken:nil];
    [FBSDKProfile setCurrentProfile:nil];
}

- (IBAction)facebookBtnClicked:(id)sender
{
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        [CommonFunctions showActivityIndicatorWithText:@"Please wait..."];
        CSFacebook *fbcallcs=[[CSFacebook alloc]init];
        [fbcallcs FbImplementMethodwithCompletetion:^(NSDictionary *result)
         {
             NSLog(@"result %@",result);
             
            
             
             dispatch_main_async_safe((^{
             
                 if ([result valueForKey:@"error"]) {
                     
                     NSLog(@"error");
                     
                     if ([FBSDKAccessToken currentAccessToken])
                     {
                         // User is logged in, do work such as go to next view controller.
                         NSLog(@"user logged in");
                         
                         [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
                         [self userProfileDetailsFacebook];
                     }
                     else
                     {
                         FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
                         
                         [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"]//,@"userID",@"expiresIn",@"signedRequest"]
                                                 handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
                          {
                              if (error)
                              {
                                  // Process error
                              } else if (result.isCancelled)
                              {
                                  // Handle cancellations
                              } else {
                                  // If you ask for multiple permissions at once, you
                                  // should check if specific permissions missing
                                  if ([result.grantedPermissions containsObject:@"email"])
                                  {
                                      [self userProfileDetailsFacebook];
                                  }
                              }
                          }];
                     }

                 }
                 else
                 {
                     NSString *imageUrl = [[NSString alloc] initWithFormat: @"http://graph.facebook.com/%@/picture?type=large", [result valueForKey:@"id"]];
                     
                     [profileImg sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                     txtEmail.text=[result valueForKey:@"email"];
                     txtFirstName.text=[result valueForKey:@"first_name"];
                     txtLName.text=[result valueForKey:@"last_name"];
                     [CommonFunctions removeActivityIndicator];
                 }
             }));
             
             
             
         }
        WithFailure:^(NSString *error)
         {
             NSLog(@"%@",error);
             [CommonFunctions removeActivityIndicator];

         }];
        
        
    }
    else
    {
        if ([FBSDKAccessToken currentAccessToken])
        {
            // User is logged in, do work such as go to next view controller.
            NSLog(@"user logged in");
            
            [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
            [self userProfileDetailsFacebook];
        }
        else
        {
            FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
            
            [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"]//,@"userID",@"expiresIn",@"signedRequest"]
                                    handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
             {
                 if (error)
                 {
                     // Process error
                 } else if (result.isCancelled)
                 {
                     // Handle cancellations
                 } else {
                     // If you ask for multiple permissions at once, you
                     // should check if specific permissions missing
                     if ([result.grantedPermissions containsObject:@"email"])
                     {
                         [self userProfileDetailsFacebook];
                     }
                 }
             }];
        }
        
    }
    
}


-(void)userProfileDetailsFacebook
{
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"/me"
                                  parameters:@{ @"fields": @"email,first_name,id,name,last_name",}
                                  HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
     {
         if (!error) {
             NSLog(@"fetched again user results:%@", result);
             NSString *imageUrl = [[NSString alloc] initWithFormat: @"http://graph.facebook.com/%@/picture?type=large", [result valueForKey:@"id"]];
             
             [profileImg sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
             txtEmail.text=[result valueForKey:@"email"];
             txtFirstName.text=[result valueForKey:@"first_name"];
             txtLName.text=[result valueForKey:@"last_name"];
             [CommonFunctions removeActivityIndicator];
         }
         else
         {
             NSLog(@"Error again user results:%@", error);
             [CommonFunctions removeActivityIndicator];
         }
     }];
    
}

#pragma mark - Observations

- (void)observeProfileChange:(NSNotification *)notfication
{
    if ([FBSDKProfile currentProfile]) {
        NSString *title = [NSString stringWithFormat:@"continue as %@", [FBSDKProfile currentProfile].name];
        NSLog(@"%@", title);
    }
}

- (void)observeTokenChange:(NSNotification *)notfication
{
    if (![FBSDKAccessToken currentAccessToken])
    {
        NSLog(@"not getting accessToken..");
    } else {
        [self observeProfileChange:nil];
    }
}

#pragma mark- Google plus social login integration Chhagan
-(void)setupGoogleintegration
{
    [GPPSignIn sharedInstance].clientID = kClientId;
    [GPPSignIn sharedInstance].scopes= [NSArray arrayWithObjects:kGTLAuthScopePlusLogin, nil];
    [GPPSignIn sharedInstance].shouldFetchGoogleUserID=YES;
    [GPPSignIn sharedInstance].shouldFetchGoogleUserEmail=YES;
    [GPPSignIn sharedInstance].shouldFetchGooglePlusUser=YES;
    [GPPSignIn sharedInstance].delegate=self;
}

- (IBAction)googlePlusBtnClicked:(id)sender {
    [[GPPSignIn sharedInstance] authenticate];
}
- (void)disconnect {
    [[GPPSignIn sharedInstance] disconnect];
    [[GPPSignIn sharedInstance] signOut];
}

- (void)didDisconnectWithError:(NSError *)error
{
    if (error)
    {
        NSLog(@"Received error %@", error);
    }
    else
    {
        // The user is signed out and disconnected.
        // Clean up user data as specified by the Google+ terms.
        [self refreshinfo];
    }
}

-(void)finishedWithAuth:(GTMOAuth2Authentication *)auth error:(NSError *)error
{
    if (error)
    {
        NSLog(@"%@",error);
    }
    else
    {
        [self refreshinfo];
    }
}


-(void)refreshinfo
{
    //txtEmail.text=[GPPSignIn sharedInstance].userEmail;
    
    GTLPlusPerson *person = [GPPSignIn sharedInstance].googlePlusUser;
    if (person == nil)
    {
        return;
    }
    
    txtLName.text=person.name.familyName;
    txtFirstName.text=person.name.givenName;
    txtEmail.text = [[person.emails objectAtIndex:0] valueForKey:@"value"];
    NSLog(@"%@",person.JSON);
    
    //  Load avatar image asynchronously, in background
    dispatch_queue_t backgroundQueue =
    dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(backgroundQueue, ^{
        NSData *avatarData = nil;
        NSString *imageURLString = person.image.url;
        if (imageURLString)
        {
            NSURL *imageURL = [NSURL URLWithString:imageURLString];
            avatarData = [NSData dataWithContentsOfURL:imageURL];
        }
        
        if (avatarData)
        {
            // Update UI from the main thread when available
            dispatch_async(dispatch_get_main_queue(), ^{
                profileImg.image = [UIImage imageWithData:avatarData];
                
            });
        }
    });
    
}


#pragma mark- Twitterlogin chhagan

- (IBAction)twitterBtnClicked:(id)sender
{
    [[CSTwitterEngine sharedEngine] permanentlySetConsumerKey:k_Consumer_Key andSecret:k_Secreat_Key];
    [CSTwitterEngine sharedEngine].delegate=self;
    [[CSTwitterEngine sharedEngine] loadAccessToken];
    [self twitterloginCall];
    
}
-(void)twitterloginCall
{
    [CSStream loginWithCompletion:^(NSDictionary *twitterDetail, ACAccount *twitterAccount)
     {
         
         NSLog(@"%@", twitterDetail);
         txtEmail.text=[twitterDetail valueForKey:@"name"];
         txtFirstName.text=[twitterDetail valueForKey:@"AcDescp"];
         txtFirstName.text=[txtFirstName.text stringByReplacingOccurrencesOfString:@"@" withString:@""];
         
         NSString *profileImageStringURL = [NSString stringWithFormat:@"https://twitter.com/%@/profile_image?size=normal",txtFirstName.text];
        
         [profileImg sd_setImageWithURL:[NSURL URLWithString:profileImageStringURL]];
         
     } andError:^(NSError *error)
     {
         
         UIViewController *loginView=[[CSTwitterEngine sharedEngine]loginControllerWithCompletionHandler:^(BOOL success) {
             if (success)
             {
                 NSDictionary *dictData= [[CSTwitterEngine sharedEngine] verifyCredentials];
                 
                 txtFirstName.text=[dictData valueForKey:@"name"];
                
                 NSString *profileImageStringURL = [(NSDictionary *)dictData objectForKey:@"profile_image_url_https"];
                 
                 // Get the profile image in the original resolution
                 profileImageStringURL = [profileImageStringURL stringByReplacingOccurrencesOfString:@"_normal" withString:@""];
                 [profileImg sd_setImageWithURL:[NSURL URLWithString:profileImageStringURL]];
                 
             }
             else
             {
                 
             }
         }];
         
         [self presentViewController:loginView animated:YES completion:nil];
         
         
     }];

}



-(void)storeAccessToken:(NSString *)accessToken
{
    [[NSUserDefaults standardUserDefaults]setObject:accessToken forKey:@"Token"];
}

-(NSString *)loadAccessToken
{
    return [[NSUserDefaults standardUserDefaults]objectForKey:@"Token"];
}


//Mirror Image Scaling and rotation method
-(UIImage *)scaleAndRotateImage:(UIImage *)image
{
    int kMaxResolution = 1242; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

@end
