//
//  BusinessReviewVC.m
//  Petlox
//
//  Created by Sumit Sharma on 01/12/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "UserReviewVC.h"
#import "BusinessReviewCustomeCell.h"
#import "UserReviewShowVC.h"
@interface UserReviewVC ()<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *arrReviewData;
    IBOutlet UILabel *lblMsgError;
    IBOutlet UITableView *reviewTbleView;
}
@end

@implementation UserReviewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    wbServiceCount=1;
    // Do any additional setup after loading the view from its nib.
}


-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"All Reviews"];
    
    
    
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStylePlain target:self.viewDeckController action:@selector(toggleLeftView)];
    [self.navigationItem setLeftBarButtonItem:btn];

    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getAllReviews];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }
    
    
}


#pragma mark - UITableView Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrReviewData.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (96.0f * [APPDELEGATE window].frame.size.height) / 568;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"BusinessReviewCustomeCell";
    BusinessReviewCustomeCell *cell=[[BusinessReviewCustomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.imgVwProfileImg.clipsToBounds=YES;
    
    NSString *strImg=[[arrReviewData objectAtIndex:indexPath.row] objectForKey:@"image"];
    if (strImg.length>0) {
        [cell.imgVwProfileImg setImageWithURL:[NSURL URLWithString:strImg] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    cell.lblBusinessName.text=[NSString stringWithFormat:@"%@",[[arrReviewData objectAtIndex:indexPath.row] objectForKey:@"business_name"]];
    cell.lblReviewDiscription.text=[[arrReviewData objectAtIndex:indexPath.row] objectForKey:@"review"];
    cell.lblTime.text=[[arrReviewData objectAtIndex:indexPath.row] objectForKey:@"updated"];
    

    if ([self isNotNull:[[arrReviewData objectAtIndex:indexPath.row] objectForKey:@"rating"]])
    {
        int avgRating=[[[arrReviewData objectAtIndex:indexPath.row]objectForKey:@"rating"]intValue];
        switch (avgRating) {
            case 0:
            {
                [cell.imgStar1 setImage:[UIImage imageNamed:@"small_rating_unfoucs"]];
                [cell.imgStar2 setImage:[UIImage imageNamed:@"small_rating_unfoucs"]];
                [cell.imgStar3 setImage:[UIImage imageNamed:@"small_rating_unfoucs"]];
                [cell.imgStar4 setImage:[UIImage imageNamed:@"small_rating_unfoucs"]];
                [cell.imgStar5 setImage:[UIImage imageNamed:@"small_rating_unfoucs"]];
                
                break;
            }
                
            case 1:
            {
                [cell.imgStar1 setImage:[UIImage imageNamed:@"small_rating_foucs"]];
                [cell.imgStar2 setImage:[UIImage imageNamed:@"small_rating_unfoucs"]];
                [cell.imgStar3 setImage:[UIImage imageNamed:@"small_rating_unfoucs"]];
                [cell.imgStar4 setImage:[UIImage imageNamed:@"small_rating_unfoucs"]];
                [cell.imgStar5 setImage:[UIImage imageNamed:@"small_rating_unfoucs"]];
                break;
            }
            case 2:
            {
                [cell.imgStar1 setImage:[UIImage imageNamed:@"small_rating_foucs"]];
                [cell.imgStar2 setImage:[UIImage imageNamed:@"small_rating_foucs"]];
                [cell.imgStar3 setImage:[UIImage imageNamed:@"small_rating_unfoucs"]];
                [cell.imgStar4 setImage:[UIImage imageNamed:@"small_rating_unfoucs"]];
                [cell.imgStar5 setImage:[UIImage imageNamed:@"small_rating_unfoucs"]];
                break;
            }
            case 3:
            {
                [cell.imgStar1 setImage:[UIImage imageNamed:@"small_rating_foucs"]];
                [cell.imgStar2 setImage:[UIImage imageNamed:@"small_rating_foucs"]];
                [cell.imgStar3 setImage:[UIImage imageNamed:@"small_rating_foucs"]];
                [cell.imgStar4 setImage:[UIImage imageNamed:@"small_rating_unfoucs"]];
                [cell.imgStar5 setImage:[UIImage imageNamed:@"small_rating_unfoucs"]];
                break;
            }
            case 4:
            {
                [cell.imgStar1 setImage:[UIImage imageNamed:@"small_rating_foucs"]];
                [cell.imgStar2 setImage:[UIImage imageNamed:@"small_rating_foucs"]];
                [cell.imgStar3 setImage:[UIImage imageNamed:@"small_rating_foucs"]];
                [cell.imgStar4 setImage:[UIImage imageNamed:@"small_rating_foucs"]];
                [cell.imgStar5 setImage:[UIImage imageNamed:@"small_rating_unfoucs"]];
                break;
            }
            case 5:
            {
                [cell.imgStar1 setImage:[UIImage imageNamed:@"small_rating_foucs"]];
                [cell.imgStar2 setImage:[UIImage imageNamed:@"small_rating_foucs"]];
                [cell.imgStar3 setImage:[UIImage imageNamed:@"small_rating_foucs"]];
                [cell.imgStar4 setImage:[UIImage imageNamed:@"small_rating_foucs"]];
                [cell.imgStar5 setImage:[UIImage imageNamed:@"small_rating_foucs"]];
                break;
            }
                
            default:
                break;
        }
        
    }
    
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserReviewShowVC *brSV=[[UserReviewShowVC alloc]initWithNibName:@"UserReviewShowVC" bundle:nil];
    brSV.dictReviewPost=[arrReviewData objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:brSV animated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getAllReviews
{
    NSMutableDictionary *param;
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"user_id",@"user",@"type", nil];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    NSString *url = [NSString stringWithFormat:@"userReview"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/userReview
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            arrReviewData=[responseDict objectForKey:@"data"];
            [reviewTbleView reloadData];
            
        }
        else if([operation.response statusCode]  == 206 ){
            NSLog(@"impo response %@",operation.response);
            lblMsgError.text=@"No review found";
            
        }

        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self getAllReviews];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          
                                      }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
