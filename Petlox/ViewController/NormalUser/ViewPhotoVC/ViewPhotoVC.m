//
//  ViewPhotoVC.m
//  Petlox
//
//  Created by Sumit Sharma on 02/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "ViewPhotoVC.h"
#import "UploadPhotoCustomeCell.h"
#import "ViewFullImageVC.h"
#import "ServiceProviderVC.h"
@interface ViewPhotoVC ()<UICollectionViewDataSource,UICollectionViewDelegate>
{
    IBOutlet UICollectionView *collctnVw;
    NSMutableArray *arrBusinessPhoto;
    IBOutlet UILabel *lblErrorMsg;
    
}
@end

@implementation ViewPhotoVC
@synthesize businessID;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    wbServiceCount=1;
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [APPDELEGATE showPetReservationTabBar:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    
    [collctnVw registerNib:[UINib nibWithNibName:@"UploadPhotoCustomeCell" bundle:nil] forCellWithReuseIdentifier:@"UploadPhotoCustomeCell"];
    
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
    [self.navigationItem setTitle:@"View Photos"];
    arrBusinessPhoto=[[NSMutableArray alloc]init];
    
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
    [self getBusinessPhotos];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }


    
}
#pragma mark - IBAction Methods

-(IBAction)backBarButtonClicked:(id)sender
{
//    [self.navigationController popViewControllerAnimated:YES];
    
    ServiceProviderVC *serviceProviderVC = (ServiceProviderVC *)[CommonFunctions exists:[ServiceProviderVC class] in:self.navigationController];
    if(serviceProviderVC == nil){
        serviceProviderVC = [[ServiceProviderVC alloc] init];
        [self.navigationController pushViewController:serviceProviderVC animated:YES];
    } else {
        [self.navigationController popToViewController:serviceProviderVC animated:YES];
    }
}


#pragma mark - CollectionView Delegate Method

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrBusinessPhoto.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    //DLog(@"SETTING SIZE FOR ITEM AT INDEX %d", indexPath.row);
    if ([UIScreen mainScreen].bounds.size.height==414) {
        CGSize mElementSize = CGSizeMake(126,126);
        return mElementSize;
        
    }
    else if ([UIScreen mainScreen].bounds.size.height==667)
    {
        CGSize mElementSize = CGSizeMake(113,113);
        return mElementSize;
        
    }
    else
    {
        CGSize mElementSize = CGSizeMake(96*SCREEN_XScale,96*SCREEN_XScale);
        return mElementSize;
        
    }
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{

    UploadPhotoCustomeCell *cell=(UploadPhotoCustomeCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"UploadPhotoCustomeCell" forIndexPath:indexPath];
    
    if (arrBusinessPhoto.count>0) {
        cell.imgPhoto.layer.cornerRadius=5;
        cell.imgPhoto.clipsToBounds=YES;
        cell.imgPhoto.layer.borderWidth=1;
        cell.imgPhoto.layer.borderColor=[UIColor colorWithRed:189.0f/255.0f green:189.0f/255.0f blue:189.0f/255.0f alpha:1.0f].CGColor;
        [cell.imgDeleteBtn setHidden:YES];
        [cell.imgAddBtn setHidden:YES];
        
        [cell.imgPhoto setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[arrBusinessPhoto objectAtIndex:indexPath.row] objectForKey:@"user_image"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    else
    {
        lblErrorMsg.text=@"No Photos found.";
        
    }
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ViewFullImageVC *vfivc=[[ViewFullImageVC alloc]initWithNibName:@"ViewFullImageVC" bundle:nil];
    vfivc.strImgURL=[[arrBusinessPhoto objectAtIndex:indexPath.row] objectForKey:@"user_image"];
    [self presentViewController:vfivc animated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark- WebServices API..

-(void)getBusinessPhotos
{
    NSMutableDictionary *param;
NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:businessID,@"user_id", nil];
    
    NSString *url = [NSString stringWithFormat:@"userPictures"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/userPictures
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            arrBusinessPhoto=[responseDict objectForKey:@"data"];
            [collctnVw reloadData];
            if (arrBusinessPhoto.count>0) {
            lblErrorMsg.text=@"";
            }
            else
            {
                lblErrorMsg.text=@"No Photos found.";
            }
        }
        else
        {
            lblErrorMsg.text=@"No Photos found.";
           // [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
            
        }
               }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                             // [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self getBusinessPhotos];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
    }];
}

@end
