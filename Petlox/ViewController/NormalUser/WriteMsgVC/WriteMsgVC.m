//
//  WriteMsgVC.m
//  Petlox
//
//  Created by Sumit Sharma on 15/12/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "WriteMsgVC.h"
#import "ChatMessageCustomeCell.h"
#import "ChatSenderMsgCustomeCell.h"
@interface WriteMsgVC ()<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>
{
    
    IBOutlet UIButton *btnSend;
    IBOutlet UIView *chatInputView;
    IBOutlet MBAutoGrowingTextView *txtTxtViw;
    IBOutlet UITableView *chatTableVw;
    IBOutlet MBAutoGrowingTextView *txtVwTemp;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    NSMutableArray *arrChatMsgs;
}
@end

@implementation WriteMsgVC
@synthesize dataDict;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    wbServiceCount=1;
    chatTableVw.estimatedRowHeight = 90.0f;
    chatTableVw.rowHeight = UITableViewAutomaticDimension;

    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController.navigationBar setHidden:NO];
    self.navigationController.navigationBarHidden=NO;
    NSLog(@"%@",dataDict);
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:UD_BUSINESS_TYPE]isEqualToString:@"business"]) {

        NSString *strName=[NSString stringWithFormat:@"%@",dataDict[@"first_name"]];

        [self.navigationItem setTitle:strName];
    }
    else
    {
                NSString *strName=[NSString stringWithFormat:@"%@",dataDict[@"business_name"]];
                [self.navigationItem setTitle:strName];
    }
    
    [activityIndicator setHidden:YES];
    [self getUserMsgs];
    
    txtTxtViw.layer.cornerRadius = 5.0f;
    txtVwTemp.layer.cornerRadius = 5.0f;

    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureClicked)];
    tapGesture.delegate=self;
    tapGesture.numberOfTapsRequired=1;
    [self.view addGestureRecognizer:tapGesture];

}
-(void)tapGestureClicked
{
    [self.view endEditing:YES];
}


-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    if([dataDict objectForKey:@"business_name"]){
        [self.navigationItem setTitle:[dataDict objectForKey:@"business_name"]];
    } else {
        [self.navigationItem setTitle:@"Message"];
    }
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
    txtVwTemp.inputAccessoryView=chatInputView;
    
  //  arrChatMsgs =[[NSMutableArray alloc]initWithObjects:@"ssfsa fsdafsaf safasdfdsaf",@"ss",@"ss",@"ss",@"ss",@"ss",@"ss",@" fsdaflkjsafljsad fjlsadfk; jsad;lkf jsd;klfj s;ldk jf;lksdajf ;lksdajf;lksajf;lk sda;lkf jsa;lf j;lsadjfsaf sd fsadfss",@"sf s;lkfjsad;kfj ;ksjf ;ksdjfasf jslk;fj ;lksajf;lj s",@"ss", nil];
    [self adjustTblViewScrollPosition];
    


}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UItextView Delegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
  //  chatFlag=YES;
    [self keyboardWillShow];
    [txtTxtViw becomeFirstResponder];
    [self adjustTblViewScrollPosition];
}
-(void)adjustTblViewScrollPosition
{
    if (arrChatMsgs.count>0)
    {
        NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:(arrChatMsgs.count-1) inSection:0];
        
        [chatTableVw scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
}
- (void) keyboardWillShow
{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0,250, 0);
    chatTableVw.contentInset = contentInsets;
    chatTableVw.scrollIndicatorInsets = contentInsets;
    chatTableVw.scrollsToTop=YES;
    
}
- (void) keyboardWillBeHidden
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    chatTableVw.contentInset = contentInsets;
    chatTableVw.scrollIndicatorInsets = contentInsets;
}


//
//- (void) textViewDidChange:(UITextView *)textView
//{
//    if(![textView hasText]) {
//        placeholderLbl.hidden = NO;
//        [self.chatTblView setScrollEnabled:YES];
//    }
//    else{
//        placeholderLbl.hidden = YES;
//        [self.chatTblView setScrollEnabled:NO];
//        [self adjustTblViewScrollPosition];
//    }
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (CGSize)getSizeForText:(NSString *)text maxWidth:(CGFloat)width font:(NSString *)fontName fontSize:(float)fontSize {
    CGSize constraintSize;
    constraintSize.height = 9999;
    constraintSize.width = width;
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:fontName size:fontSize], NSFontAttributeName,
                                          nil];
    
    CGRect frame = [text boundingRectWithSize:constraintSize
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:attributesDictionary
                                      context:nil];
    
    CGSize stringSize = frame.size;
    return stringSize;
}
#pragma mark - UITableView Delegate Method
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrChatMsgs.count;
}
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UIFont * font = [UIFont systemFontOfSize:9.0f];
//
//    NSString *text = [[arrChatMsgs objectAtIndex:indexPath.row] objectForKey:@"message"];
//    CGFloat height = [text boundingRectWithSize:CGSizeMake(chatTableVw.frame.size.width, 800) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName: font} context:nil].size.height;
//    return height+90;
//    
////    static NSString *cellIdentifier=@"ChatMessageCustomeCell";
////    
////    ChatMessageCustomeCell *cell=[[ChatMessageCustomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
////    
////    NSLog(@"%f",cell.lblMsg.frame.size.height);
////    return 120.0f ;
//
//
//}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier=@"ChatMessageCustomeCell";
    static NSString *cellIdentifier1=@"ChatSenderMsgCustomeCell";
    
    ChatMessageCustomeCell *cell=[[ChatMessageCustomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    ChatSenderMsgCustomeCell *cell1=[[ChatSenderMsgCustomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier1];
    if (cell==nil||cell1==nil) {
        cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
           cell1=[tableView dequeueReusableCellWithIdentifier:cellIdentifier1];
    }
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:UD_BUSINESS_TYPE] isEqualToString:@"user"]) {
        
        
        cell1.selectionStyle=UITableViewCellSelectionStyleNone;
        cell1.bgVw.layer.cornerRadius=5.0f;
        
        cell1.imgProfileImg.layer.cornerRadius=5.0f;
        cell1.imgProfileImg.backgroundColor=[UIColor redColor];
        cell1.imgProfileImg.clipsToBounds=YES;
        
        
        if ([[[arrChatMsgs objectAtIndex:indexPath.row] objectForKey:@"message_sender"]isEqualToString:@"User"]) {
            
            
            cell1.lblTime.text = [NSDate prettyTimestampSinceDate:[NSDate dateWithTimeIntervalSince1970:[[[arrChatMsgs objectAtIndex:indexPath.row]objectForKey:@"created"]doubleValue]]];
            
            cell1.lblName.text=[NSString stringWithFormat:@"%@ %@",[[arrChatMsgs objectAtIndex:indexPath.row]objectForKey:@"sender_first_name"],[[arrChatMsgs objectAtIndex:indexPath.row]objectForKey:@"sender_last_name"]];
            
            NSData *nsdataFromBase64String = [[NSData alloc]
                                              initWithBase64EncodedString:[[arrChatMsgs objectAtIndex:indexPath.row] objectForKey:@"message"] options:0];
            
            // Decoded NSString from the NSData
            NSString *base64Decoded = [[NSString alloc]
                                       initWithData:nsdataFromBase64String encoding:NSUTF8StringEncoding];

            cell1.lblMsg.text=base64Decoded;
                        
            cell1.lblMsg.numberOfLines = 0;
            [cell1.lblMsg sizeToFit];
            
            NSString *imgURL=[[arrChatMsgs objectAtIndex:indexPath.row] objectForKey:@"sender_image"];
            if (imgURL.length>1) {
                
                [cell1.imgProfileImg setImageWithURL:[NSURL URLWithString:[[arrChatMsgs objectAtIndex:indexPath.row] objectForKey:@"sender_image"]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            }
            else
            {
                [cell1.imgProfileImg setImage:[UIImage imageNamed:@"DefaultProfileImg"]];
            }
            return cell1;
        }
        else
        {
            
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.bgVw.layer.cornerRadius=5.0f;
            
            cell.imgProfileImg.layer.cornerRadius=5.0f;
            cell.imgProfileImg.backgroundColor=[UIColor redColor];
            cell.imgProfileImg.clipsToBounds=YES;
            
            
            cell.lblTime.text = [NSDate prettyTimestampSinceDate:[NSDate dateWithTimeIntervalSince1970:[[[arrChatMsgs objectAtIndex:indexPath.row]objectForKey:@"created"]doubleValue]]];
            cell.lblName.text=[NSString stringWithFormat:@"%@",[[arrChatMsgs objectAtIndex:indexPath.row]objectForKey:@"sender_business_name"]];
            
            NSData *nsdataFromBase64String = [[NSData alloc]
                                              initWithBase64EncodedString:[[arrChatMsgs objectAtIndex:indexPath.row] objectForKey:@"message"] options:0];
            
            // Decoded NSString from the NSData
            NSString *base64Decoded = [[NSString alloc]
                                       initWithData:nsdataFromBase64String encoding:NSUTF8StringEncoding];
            
            
            cell.lblMsg.text=base64Decoded;
            
            
            cell.lblMsg.numberOfLines = 0;
            //        cell.lblMsg.text = [[tempDictionary objectForKey:@"data"] objectForKey:@"title"];
            [cell.lblMsg sizeToFit];
            
            NSString *imgURL=[[arrChatMsgs objectAtIndex:indexPath.row] objectForKey:@"sender_image"];
            if (imgURL.length>1) {
                
                [cell.imgProfileImg setImageWithURL:[NSURL URLWithString:[[arrChatMsgs objectAtIndex:indexPath.row] objectForKey:@"sender_image"]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                
            }
            else
            {
                [cell.imgProfileImg setImage:[UIImage imageNamed:@"DefaultProfileImg"]];
            }
            return cell;
        }
        
    }
    else
    {
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.bgVw.layer.cornerRadius=5.0f;
        
        cell.imgProfileImg.layer.cornerRadius=5.0f;
        cell.imgProfileImg.backgroundColor=[UIColor redColor];
        cell.imgProfileImg.clipsToBounds=YES;
        
        
        if ([[[arrChatMsgs objectAtIndex:indexPath.row] objectForKey:@"message_sender"]isEqualToString:@"User"]) {
            cell.lblTime.text = [NSDate prettyTimestampSinceDate:[NSDate dateWithTimeIntervalSince1970:[[[arrChatMsgs objectAtIndex:indexPath.row]objectForKey:@"created"]doubleValue]]];;
            cell.lblName.text=[NSString stringWithFormat:@"%@ %@",[[arrChatMsgs objectAtIndex:indexPath.row]objectForKey:@"sender_first_name"],[[arrChatMsgs objectAtIndex:indexPath.row]objectForKey:@"sender_last_name"]];
            
            NSData *nsdataFromBase64String = [[NSData alloc]
                                              initWithBase64EncodedString:[[arrChatMsgs objectAtIndex:indexPath.row] objectForKey:@"message"] options:0];
            
            // Decoded NSString from the NSData
            NSString *base64Decoded = [[NSString alloc]
                                       initWithData:nsdataFromBase64String encoding:NSUTF8StringEncoding];
            
            cell.lblMsg.text=base64Decoded;
            
            

            cell.lblMsg.numberOfLines = 0;
            [cell.lblMsg sizeToFit];
            
            NSString *imgURL=[[arrChatMsgs objectAtIndex:indexPath.row] objectForKey:@"sender_image"];
            if (imgURL.length>1) {
                
                [cell.imgProfileImg setImageWithURL:[NSURL URLWithString:[[arrChatMsgs objectAtIndex:indexPath.row] objectForKey:@"sender_image"]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            }
            else
            {
                [cell.imgProfileImg setImage:[UIImage imageNamed:@"DefaultProfileImg"]];
            }
            return cell;
        }
        else
        {
            
            cell1.selectionStyle=UITableViewCellSelectionStyleNone;
            cell1.bgVw.layer.cornerRadius=5.0f;
            
            cell1.imgProfileImg.layer.cornerRadius=5.0f;
            cell1.imgProfileImg.backgroundColor=[UIColor redColor];
            cell1.imgProfileImg.clipsToBounds=YES;
            
            
            cell1.lblTime.text = [NSDate prettyTimestampSinceDate:[NSDate dateWithTimeIntervalSince1970:[[[arrChatMsgs objectAtIndex:indexPath.row]objectForKey:@"created"]doubleValue]]];
            cell1.lblName.text=[NSString stringWithFormat:@"%@",[[arrChatMsgs objectAtIndex:indexPath.row]objectForKey:@"sender_business_name"]];
            
            NSData *nsdataFromBase64String = [[NSData alloc]
                                              initWithBase64EncodedString:[[arrChatMsgs objectAtIndex:indexPath.row] objectForKey:@"message"] options:0];
            
            // Decoded NSString from the NSData
            NSString *base64Decoded = [[NSString alloc]
                                       initWithData:nsdataFromBase64String encoding:NSUTF8StringEncoding];
            
            cell1.lblMsg.text=base64Decoded;
            
            
            cell1.lblMsg.numberOfLines = 0;
            //        cell.lblMsg.text = [[tempDictionary objectForKey:@"data"] objectForKey:@"title"];
            [cell1.lblMsg sizeToFit];
            
            NSString *imgURL=[[arrChatMsgs objectAtIndex:indexPath.row] objectForKey:@"sender_image"];
            if (imgURL.length>1) {
                
                [cell1.imgProfileImg setImageWithURL:[NSURL URLWithString:[[arrChatMsgs objectAtIndex:indexPath.row] objectForKey:@"sender_image"]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                
            }
            else
            {
                [cell1.imgProfileImg setImage:[UIImage imageNamed:@"DefaultProfileImg"]];
            }
            return cell1;
        }
        
    }
    return cell;
}




#pragma mark - IBAction Methods
- (IBAction)sentButtonClicked:(id)sender {
    if ([CommonFunctions trimSpaceInString:txtTxtViw.text].length>1) {
        if ([CommonFunctions reachabiltyCheck]) {
                [self keyboardWillBeHidden];
            [activityIndicator setHidden:NO];
            [activityIndicator startAnimating];
            [btnSend setHidden:YES];
            [self postUserMessage];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
        }
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please type message."];
    }
}

#pragma mark - WebServiceAPI


-(void)postUserMessage
{
    
    // Get NSString from NSData object in Base64
    NSData *nsdata = [[[CommonFunctions trimSpaceInString:txtTxtViw.text]  capitalizedString] dataUsingEncoding:NSUTF8StringEncoding];
    
    // Get NSString from NSData object in Base64
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];


    
    NSMutableDictionary *param;
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"user_id",[dataDict objectForKey:@"user_id"],@"business_id",base64Encoded,@"message",[[NSUserDefaults standardUserDefaults]objectForKey:UD_BUSINESS_TYPE],@"type", nil];
    
    NSString *url = [NSString stringWithFormat:@"writeMessage"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/userReview
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        [activityIndicator stopAnimating];
        [btnSend setHidden:NO];
        [activityIndicator setHidden:YES];

        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
           // arrReviewData=[responseDict objectForKey:@"data"];
          //  [reviewTbleView reloadData];
            [txtTxtViw resignFirstResponder];
            txtTxtViw.text=@"";
            [self keyboardWillBeHidden];
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self getUserMsgs];
            
        }
        else if([operation.response statusCode]  == 206 ){
            NSLog(@"impo response %@",operation.response);
           // lblMsgError.text=@"No review found";
            
        }
        
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self postUserMessage];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          
                                      }];
}


-(void)getUserMsgs
{
    NSMutableDictionary *param;
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"user_id",[dataDict objectForKey:@"user_id"],@"business_id", nil];
    
    NSString *url = [NSString stringWithFormat:@"allMessage"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/userReview
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        [activityIndicator stopAnimating];
        [btnSend setHidden:NO];
        [activityIndicator setHidden:YES];
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            // arrReviewData=[responseDict objectForKey:@"data"];
            //  [reviewTbleView reloadData];
            arrChatMsgs=[responseDict objectForKey:@"data"];
            [chatTableVw reloadData];
            [self adjustTblViewScrollPosition];
            
        }
        else if([operation.response statusCode]  == 206 ){
            NSLog(@"impo response %@",operation.response);
            // lblMsgError.text=@"No review found";
            
        }
        
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self getUserMsgs];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          
                                      }];
}

@end
