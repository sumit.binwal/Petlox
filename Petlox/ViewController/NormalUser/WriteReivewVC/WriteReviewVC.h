//
//  WriteReviewVC.h
//  Petlox
//
//  Created by Sumit Sharma on 06/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WriteReviewVC : UIViewController
@property(nonatomic,strong)NSMutableDictionary *dictBusinessDetail;
@property(nonatomic,strong)NSMutableDictionary *dictAllreadyReviewed;
@property(nonatomic,strong)NSString *strBusinessID;
@property (weak, nonatomic) IBOutlet UIButton *btnPost;
@property (weak, nonatomic) IBOutlet UIView *reviewView;
@end
