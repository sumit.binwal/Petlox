//
//  SignInVC.h
//  Petlox
//
//  Created by Sumit Sharma on 22/07/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignInVC : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *btnSignin;
@property (weak, nonatomic) IBOutlet UIButton *btnSignup;
@property (weak, nonatomic) IBOutlet UIView *signBtnsContainerView;
@property BOOL isFromBusiness;
@property (weak, nonatomic) IBOutlet UIView *forgotPasswordUnderline;
@property (weak, nonatomic) IBOutlet UIView *businessButtonUnderline;
@property (weak, nonatomic) IBOutlet UIButton *btnBusiness;
@end
