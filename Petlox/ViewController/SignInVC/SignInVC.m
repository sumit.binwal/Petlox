//
//  SignInVC.m
//  Petlox
//
//  Created by Sumit Sharma on 22/07/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "SignInVC.h"
#import "SignUpVC.h"
#import "BusinessClaimVC.h"
#import "ReservationVC.h"
#import "RightMenuVC.h"
#import "BusinessSignupFirstVC.h"



@interface SignInVC ()<UIGestureRecognizerDelegate,UITextFieldDelegate>
{
    IBOutlet UIView *alrtBox;
    IBOutlet UIView *forgetView;
    IBOutlet UIView *containerVw;
    IBOutlet UIView *emailVw;
    
    IBOutlet UIView *passwordVw;
    IBOutlet UIView *businessBtnVw;
    IBOutlet UITextField *txtForgotEmailAddress;
    IBOutlet UITextField *txtEmailAddress;
    IBOutlet UITextField *txtPassword;
    
    IBOutlet UIButton *btnForgetPassword;
    
    IBOutlet UIScrollView *scrllVw;
    IBOutlet UIScrollView *forgotPasswordScrollVw;
}
@end
@implementation SignInVC
@synthesize btnSignin, btnSignup;

#pragma mark-ViewCntroller Life Cycle Methods

- (void)viewDidLoad {
    
//[self.navigationItem setHidesBackButton:YES];
    
    wbServiceCount=1;
    [super viewDidLoad];
    [self setupView];    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    btnSignin.layer.cornerRadius=self.btnSignin.frame.size.width/2;
    btnSignup.layer.cornerRadius=self.btnSignup.frame.size.width/2;
    [self setUnderlineFrame:self.forgotPasswordUnderline For:btnForgetPassword];
    [self setUnderlineFrame:self.businessButtonUnderline For:self.btnBusiness];
}

-(void)setUnderlineFrame:(UIView *)underline For:(UIButton *)button{
    underline.frame = CGRectMake(button.titleLabel.frame.origin.x, underline.frame.origin.y, button.titleLabel.frame.size.width, 1);
}


#pragma mark-Setup Method
-(void)setupView
{
    //For Navigation Bar.
    if (self.isFromBusiness) {
        [businessBtnVw setHidden:NO];
    }
    else
    {
        [businessBtnVw setHidden:YES];
    }
    
    
    [btnSignin.titleLabel setFont:[UIFont fontWithName:btnSignin.titleLabel.font.fontName size:btnSignin.titleLabel.font.pointSize*SCREEN_XScale]];
    [btnSignup.titleLabel setFont:[UIFont fontWithName:btnSignup.titleLabel.font.fontName size:btnSignup.titleLabel.font.pointSize*SCREEN_XScale]];

    
    [CommonFunctions setNavigationBar:self.navigationController];
    
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init]
                                      forBarPosition:UIBarPositionAny
                                          barMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
    //TextField Placeholder Color Change
    
    [txtEmailAddress setValue:[UIColor colorWithRed:133.0f/255.0f green:133.0f/255.0f blue:133.0f/255.0f alpha:1]
                   forKeyPath:@"_placeholderLabel.textColor"];
    [txtPassword setValue:[UIColor colorWithRed:133.0f/255.0f green:133.0f/255.0f blue:133.0f/255.0f alpha:1]
               forKeyPath:@"_placeholderLabel.textColor"];
    [txtForgotEmailAddress setValue:[UIColor colorWithRed:66.0f/255.0f green:66.0f/255.0f blue:66.0f/255.0f alpha:1]
                         forKeyPath:@"_placeholderLabel.textColor"];
    
    emailVw.layer.cornerRadius=5.0f;
    passwordVw.layer.cornerRadius=5.0f;
    
    
    //Signin Button. Border Color and Radius Set
//    btnSignin.layer.borderWidth=1;
//    btnSignin.layer.borderColor=[[UIColor colorWithRed:140.0f/255.0f green:203.0f/255.0f blue:243.0f/255.0f alpha:1] CGColor];
    
    //UITapGesture Integration
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureClicked)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];
    
//    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
}

-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
//Validate Text Field Values
-(BOOL)validateTextField
{
    if (![CommonFunctions isValueNotEmpty:txtEmailAddress.text]) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter email address ."];
        return false;
    }
    else if (![CommonFunctions IsValidEmail:txtEmailAddress.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter valid email address."];
        return false;
    }
    else if (![CommonFunctions isValueNotEmpty:txtPassword.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter password ."];
        return false;
    }
    return true;
}


#pragma mark-IBAction Button

-(void)tapGestureClicked
{
    [self.view endEditing:YES];
    [self scrollToNormalView];
}

- (IBAction)forgetPasswordButtonClicked:(id)sender {
    
    [self tapGestureClicked];
//    self.navigationController.navigationBarHidden=YES;
    forgetView.frame=CGRectMake(0, self.view.bounds.origin.y-self.navigationController.navigationBar.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height+self.navigationController.navigationBar.frame.size.height);
    
    txtForgotEmailAddress.text=@"";
    [self.view addSubview:forgetView];
}
- (IBAction)forgotPasswordCancleBtnClicked:(id)sender {
    [forgetView removeFromSuperview ];
    btnSignin.hidden=NO;
}
- (IBAction)forgotPasswordSendButtonClicked:(id)sender {
    
    if ([CommonFunctions isValueNotEmpty:txtForgotEmailAddress.text]) {
        if ([CommonFunctions IsValidEmail:txtForgotEmailAddress.text]) {
            if ([CommonFunctions reachabiltyCheck]) {
                [CommonFunctions showActivityIndicatorWithText:@""];
                [self forgotPasswordAPI];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection ."];
            }
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please enter valid email address."];
            
        }
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter email address ."];
    }
}
- (IBAction)signInButtonClicked:(id)sender
{
    [self tapGestureClicked];

    if ([self validateTextField])
    {
        if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self userSignIn];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection"];
        }
    }
}
- (IBAction)registrationUserBtnClicked:(id)sender
{
    [self tapGestureClicked];
    if (_isFromBusiness) {
        BusinessSignupFirstVC *bsvc=[[BusinessSignupFirstVC alloc]init];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_CLAIM_ID];
        [self.navigationController pushViewController:bsvc animated:YES];
    }
    else
    {
        SignUpVC *suvc=[[SignUpVC alloc]init];
        [self.navigationController pushViewController:suvc animated:YES];
    }
    
}
- (IBAction)businessUserRegistrationBtnClicked:(id)sender
{
    [self tapGestureClicked];
    BusinessClaimVC *bcvc=[[BusinessClaimVC alloc]init];
    [self.navigationController pushViewController:bcvc animated:YES];
}

#pragma mark-Scroll View Method
-(void)scrollViewToCenterOfScreen:(UITextField *)textField
{
    [scrllVw setScrollEnabled:YES];
    float difference;
    if (scrllVw.contentSize.height == 600)
        difference = 50.0f;
    else
        difference = 50.0f;
    CGFloat viewCenterY = textField.center.y+containerVw.frame.origin.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat avaliableHeight = applicationFrame.size.height - 5.0f;
    CGFloat y = viewCenterY - avaliableHeight / 3.0f;
    
    if (y < 0)
        y = 0;
    
    [scrllVw setContentOffset:CGPointMake(0, y) animated:YES];
    [scrllVw setContentSize:CGSizeMake(320.0f, 830.0f)];
}
-(void)scrollForgotPasswordViewToCenterOfScreen:(UITextField *)textField
{
    [forgotPasswordScrollVw setScrollEnabled:YES];
    float difference;
    if (forgotPasswordScrollVw.contentSize.height == 600)
        difference = 50.0f;
    else
        difference = 50.0f;
    CGFloat viewCenterY = textField.center.y+containerVw.frame.origin.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat avaliableHeight = applicationFrame.size.height - 5.0f;
    CGFloat y = viewCenterY - avaliableHeight / 3.0f;
    
    if (y < 0)
        y = 0;
    
    [forgotPasswordScrollVw setContentOffset:CGPointMake(0, y) animated:YES];
    [forgotPasswordScrollVw setContentSize:CGSizeMake(320.0f, 700.0f)];
}
-(void)scrollToNormalView
{
    [scrllVw setContentOffset:CGPointZero];
    [scrllVw setScrollEnabled:NO];
    [forgotPasswordScrollVw setContentOffset:CGPointZero];
    [forgotPasswordScrollVw setScrollEnabled:NO];
}


#pragma mark-UIText Field Delegate Methods

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (txtForgotEmailAddress==textField) {
        [self scrollForgotPasswordViewToCenterOfScreen:textField];
        [txtForgotEmailAddress setTintColor:[UIColor colorWithRed:66.0f/255.0f green:66.0f/255.0f blue:66.0f/255.0f alpha:1.0f]];
    }
    else
    {
        [self scrollViewToCenterOfScreen:textField];
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSCharacterSet *whiteSpace=[NSCharacterSet whitespaceCharacterSet];
    if(textField==txtPassword)
    {
        if ([string rangeOfCharacterFromSet:whiteSpace].location != NSNotFound) {
            [CommonFunctions alertTitle:@"" withMessage:@"Space Not Allowed"];
            return NO;
        }
    }
    return YES;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==txtEmailAddress) {
        [txtPassword becomeFirstResponder];
    }
    else if (textField==txtPassword)
    {
        [txtPassword resignFirstResponder];
        [scrllVw setContentOffset:CGPointZero];
        [scrllVw setScrollEnabled:NO];
    }
    else if (txtForgotEmailAddress==textField)
    {
        [txtForgotEmailAddress resignFirstResponder];
        [self scrollToNormalView];
    }
    return YES;
}


#pragma mark - WebService API
//Login API
-(void)userSignIn
{
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CommonFunctions trimSpaceInString:txtEmailAddress.text],@"email",[CommonFunctions trimSpaceInString:txtPassword.text],@"password",pushDeviceToken,@"device_token",@"iphone",@"device_type",[NSString stringWithFormat:@"%f",CurrentLatitude],@"lat",[NSString stringWithFormat:@"%f",CurrentLongitude],@"lon", nil];
    NSString *url = [NSString stringWithFormat:@"login"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.173/petlox_svn/mobile/login
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"token"] forKey:UD_TOKEN_ID];
            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"type"] forKey:UD_BUSINESS_TYPE];
            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"first_name"] forKey:UD_FIRST_NAME];
            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"last_name"] forKey:UD_LAST_NAME];
            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"image"] forKey:UD_USER_IMG];
            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"is_status"] forKey:UD_STATUS_COMPLETE];
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:UD_BUSINESS_TYPE]isEqualToString:@"business"]) {
                
                [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"business_name"] forKey:UD_BUSINESS_NAME];
                [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"email"] forKey:UD_BUSINESS_EMAIL];
                [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"pghone"] forKey:UD_BUSINESS_PHONE];
                
                [[NSUserDefaults standardUserDefaults]synchronize];
                
               // APPDELEGATE.window=[[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
                
                IIViewDeckController* deckController = [APPDELEGATE generateBusinessControllerStack];
                
                [APPDELEGATE.window setRootViewController:deckController];
                [APPDELEGATE.window makeKeyAndVisible];

            }
            else
            {
                //APPDELEGATE.window=[[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
                IIViewDeckController* deckController = [APPDELEGATE generateControllerStack];
                [APPDELEGATE.window setRootViewController:deckController];
                [APPDELEGATE.window makeKeyAndVisible];
            }
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self userSignIn];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}

//Forgot Passwrd API
-(void)forgotPasswordAPI
{
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:txtForgotEmailAddress.text,@"email", nil];
    NSString *url = [NSString stringWithFormat:@"forgot_password"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.173/petlox_svn/mobile/forgot_password
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            [forgetView removeFromSuperview ];
            btnSignin.hidden=NO;
            txtForgotEmailAddress.text=@"";
            
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self forgotPasswordAPI];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}


@end
